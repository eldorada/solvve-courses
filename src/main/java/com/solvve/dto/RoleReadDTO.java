package com.solvve.dto;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class RoleReadDTO {
    private UUID id;
    private String description;
    private String nameRole;

    private Instant createdAt;
    private Instant updatedAt;

    private UUID filmId;
    private UUID actorId;
}
