package com.solvve.dto;

import com.solvve.domain.ViewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ViewReadDTO {
    private UUID id;

    private UUID loggedUserId;
    private UUID filmId;

    private Integer loggedUserMark;

    private ViewStatus status;
    private Instant startAt;
    private Instant finishAt;

    private Instant createdAt;
    private Instant updatedAt;

}
