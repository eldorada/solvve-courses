package com.solvve.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class RolePutDTO {
    private String description;
    private String nameRole;

    private UUID filmId;
    private UUID actorId;
}
