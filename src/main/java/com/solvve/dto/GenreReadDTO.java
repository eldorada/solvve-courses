package com.solvve.dto;

import com.solvve.domain.GenreType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class GenreReadDTO {

    private UUID id;
    private GenreType type;

    private Instant createdAt;
    private Instant updatedAt;

}
