package com.solvve.dto;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ReviewReadDTO {
    private UUID id;
    private String text;
    private Integer like;
    private Integer dislike;
    private Instant modifyDate;
    private ReviewStatus status;

    private Instant createdAt;
    private Instant updatedAt;

    private UUID filmId;

}
