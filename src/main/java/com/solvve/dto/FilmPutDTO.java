package com.solvve.dto;

import lombok.Data;

import java.time.Instant;

@Data
public class FilmPutDTO {
    private String title;
    private String originalTitle;
    private Instant releaseDate;
    private Boolean status;

}
