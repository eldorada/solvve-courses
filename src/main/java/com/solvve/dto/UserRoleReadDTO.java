package com.solvve.dto;

import com.solvve.domain.UserRoleType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class UserRoleReadDTO {

    private UUID id;
    private UserRoleType type;

    private Instant createdAt;
    private Instant updatedAt;

}
