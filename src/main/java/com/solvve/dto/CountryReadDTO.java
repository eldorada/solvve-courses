package com.solvve.dto;

import com.solvve.domain.CountryType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class CountryReadDTO {

    private UUID id;
    private CountryType type;

    private Instant createdAt;
    private Instant updatedAt;

}
