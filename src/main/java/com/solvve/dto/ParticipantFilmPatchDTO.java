package com.solvve.dto;

import com.solvve.domain.ParticipantFilmType;
import lombok.Data;

import java.util.UUID;

@Data
public class ParticipantFilmPatchDTO {
    private String description;
    private ParticipantFilmType type;

    private UUID filmId;
    private UUID personId;
}
