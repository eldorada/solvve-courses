package com.solvve.dto;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class FilmReadDTO {
    private UUID id;
    private String title;
    private String originalTitle;
    private Instant releaseDate;
    private Boolean status;
    private Double averageMark;

    private Instant createdAt;
    private Instant updatedAt;

}
