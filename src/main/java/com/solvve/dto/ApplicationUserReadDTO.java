package com.solvve.dto;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ApplicationUserReadDTO {

    private UUID id;
    private String name;
    private String login;
    private Double rating;

    private String email;
    private String encodedPassword;


    private Instant createdAt;
    private Instant updatedAt;

}
