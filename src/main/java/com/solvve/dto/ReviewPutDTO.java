package com.solvve.dto;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ReviewPutDTO {
    private String text;
    private Integer like;
    private Integer dislike;
    private Instant modifyDate;
    private ReviewStatus status;

    private UUID filmId;

}
