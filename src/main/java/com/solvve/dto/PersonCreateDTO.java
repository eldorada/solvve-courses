package com.solvve.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PersonCreateDTO {
    private String firstName;
    private String secondName;
    private String biography;
    private LocalDate dateOfBirth;

}
