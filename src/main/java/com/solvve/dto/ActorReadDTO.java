package com.solvve.dto;

import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ActorReadDTO {
    private UUID id;
    private Double averageMark;

    private UUID personId;

    private Instant createdAt;
    private Instant updatedAt;

}
