package com.solvve.dto;

import com.solvve.domain.ViewStatus;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.time.Instant;
import java.util.UUID;

@Data
public class ViewCreateDTO {

    @NotNull
    private UUID loggedUserId;

    @NotNull
    private UUID filmId;

    private Integer loggedUserMark;

    private ViewStatus status;

    @NotNull
    private Instant startAt;
    @NotNull
    private Instant finishAt;

}
