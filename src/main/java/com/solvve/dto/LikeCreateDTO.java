package com.solvve.dto;

import com.solvve.domain.LikedObjectType;
import lombok.Data;

import java.util.UUID;

@Data
public class LikeCreateDTO {
    private Boolean like;

    private UUID likedObjectId;

    private LikedObjectType type;

}
