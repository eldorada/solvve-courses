package com.solvve.dto;

import com.solvve.domain.SignalToManagerStatus;
import com.solvve.domain.SignalToManagerType;
import lombok.Data;

import java.util.UUID;

@Data
public class SignalToManagerFilter {

    private UUID loggedUserId;
    private UUID managerId;
    private UUID fixObjectId;

    private SignalToManagerStatus status;
    private SignalToManagerType type;

    private String remarkText;
}
