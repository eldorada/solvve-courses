package com.solvve.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorPutDTO {
    private Double averageMark;

    private UUID personId;

}
