package com.solvve.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.UUID;

@Data
@AllArgsConstructor
public class FilmInLeaderBoardReadDTO {
    private UUID id;
    private String title;
    private Double averageMark;
    private Long viewCount;

}
