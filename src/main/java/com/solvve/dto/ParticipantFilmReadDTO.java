package com.solvve.dto;

import com.solvve.domain.ParticipantFilmType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ParticipantFilmReadDTO {
    private UUID id;
    private String description;
    private ParticipantFilmType type;

    private Instant createdAt;
    private Instant updatedAt;

    private UUID filmId;
    private UUID personId;
}
