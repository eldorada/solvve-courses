package com.solvve.dto;

import com.solvve.domain.ViewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ViewReadExtendedDTO {
    private UUID id;

    private ApplicationUserReadDTO loggedUser;
    private FilmReadDTO film;

    private Integer loggedUserMark;

    private ViewStatus status;
    private Instant startAt;
    private Instant finishAt;

    private Instant createdAt;
    private Instant updatedAt;

}
