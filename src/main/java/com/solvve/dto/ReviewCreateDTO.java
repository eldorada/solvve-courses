package com.solvve.dto;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.time.Instant;

@Data
public class ReviewCreateDTO {

    private String text;
    private Integer like;
    private Integer dislike;
    private Instant modifyDate;
    private ReviewStatus status;

}
