package com.solvve.dto;

import com.solvve.domain.ParticipantFilmType;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class ParticipantFilmCreateDTO {
    private String description;
    private ParticipantFilmType type;

    @NotNull
    private UUID personId;

    @NotNull
    private UUID filmId;

}
