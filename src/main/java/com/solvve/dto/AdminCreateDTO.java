package com.solvve.dto;

import lombok.Data;

@Data
public class AdminCreateDTO {

    private String name;
    private String login;
    private Double rating;
    private String email;
    private String encodedPassword;

}
