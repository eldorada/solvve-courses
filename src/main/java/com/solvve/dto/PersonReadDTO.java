package com.solvve.dto;

import lombok.Data;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
public class PersonReadDTO {
    private UUID id;

    private String firstName;
    private String secondName;
    private String biography;
    private LocalDate dateOfBirth;

    private Instant createdAt;
    private Instant updatedAt;
}
