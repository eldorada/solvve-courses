package com.solvve.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class RolePatchDTO {
    private String description;
    private String nameRole;

    private UUID filmId;
    private UUID actorId;

}
