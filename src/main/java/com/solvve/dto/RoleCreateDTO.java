package com.solvve.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
public class RoleCreateDTO {
    private String description;
    private String nameRole;

    @NotNull
    private UUID actorId;

    @NotNull
    private UUID filmId;

}
