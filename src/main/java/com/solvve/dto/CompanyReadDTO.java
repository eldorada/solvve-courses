package com.solvve.dto;

import com.solvve.domain.CompanyType;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class CompanyReadDTO {

    private UUID id;
    private CompanyType type;

    private Instant createdAt;
    private Instant updatedAt;

}
