package com.solvve.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ActorPatchDTO {
    private Double averageMark;

    private UUID personId;

}
