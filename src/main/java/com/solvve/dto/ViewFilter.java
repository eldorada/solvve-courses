package com.solvve.dto;

import com.solvve.domain.ViewStatus;
import lombok.Data;

import java.time.Instant;
import java.util.UUID;

@Data
public class ViewFilter {
    private UUID loggedUserId;
    private UUID filmId;

    private ViewStatus status;
    private Instant startAtFrom;
    private Instant startAtTo;
}
