package com.solvve.event;

import com.solvve.domain.ViewStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class ViewStatusChangedEvent {
    private UUID viewId;
    private ViewStatus newStatus;
}
