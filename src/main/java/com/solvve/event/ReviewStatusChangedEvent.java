package com.solvve.event;

import com.solvve.domain.ReviewStatus;
import lombok.Data;

import java.util.UUID;

@Data
public class ReviewStatusChangedEvent {
    private UUID reviewId;
    private ReviewStatus newStatus;
}