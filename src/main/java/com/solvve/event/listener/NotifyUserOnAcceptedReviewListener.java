package com.solvve.event.listener;

import com.solvve.event.ReviewStatusChangedEvent;
import com.solvve.service.UserNotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotifyUserOnAcceptedReviewListener {

    @Autowired
    private UserNotificationService userNotificationService;

    @Async
    @EventListener(condition = "#event.newStatus == T(com.solvve.domain.ReviewStatus).ACCEPTED")
    public void onEvent(ReviewStatusChangedEvent event) {
        log.info("handling {}", event);
        userNotificationService.notifyOnReviewStatusChangedToAccepted(event.getReviewId());
    }
}
