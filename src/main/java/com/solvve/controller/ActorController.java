package com.solvve.controller;

import com.solvve.dto.ActorCreateDTO;
import com.solvve.dto.ActorPatchDTO;
import com.solvve.dto.ActorPutDTO;
import com.solvve.dto.ActorReadDTO;
import com.solvve.service.ActorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/actors")
public class ActorController {

    @Autowired
    private ActorService actorService;

    @GetMapping("/{id}")
    public ActorReadDTO getActor(@PathVariable UUID id) {
        return actorService.getActor(id);
    }

    @PostMapping("/persons/{personId}")
    public ActorReadDTO createPersonsUser(@PathVariable UUID personId, @RequestBody ActorCreateDTO createDTO) {
        return actorService.createPersonsActor(personId, createDTO);
    }

    @PatchMapping("/{id}")
    public ActorReadDTO patchActor(@PathVariable UUID id,
                                                       @RequestBody ActorPatchDTO patch) {
        return actorService.patchActor(id, patch);
    }

    @PutMapping("/{id}")
    public ActorReadDTO updateActor(@PathVariable UUID id, @RequestBody ActorPutDTO put) {
        return actorService.updateActor(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteActor(@PathVariable UUID id) {
        actorService.deleteActor(id);
    }

}
