package com.solvve.controller;

import com.solvve.controller.security.AdminOrCurrentModerator;
import com.solvve.controller.security.OnlyAdmin;
import com.solvve.dto.ModeratorCreateDTO;
import com.solvve.dto.ModeratorPatchDTO;
import com.solvve.dto.ModeratorPutDTO;
import com.solvve.dto.ModeratorReadDTO;
import com.solvve.service.ModeratorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/moderators")
public class ModeratorController {

    @Autowired
    private ModeratorService moderatorService;

    @AdminOrCurrentModerator
    @GetMapping("/{id}")
    public ModeratorReadDTO getModerator(@PathVariable UUID id) {
        return moderatorService.getModerator(id);
    }

    @OnlyAdmin
    @PostMapping
    public ModeratorReadDTO createUser(@RequestBody ModeratorCreateDTO createDTO) {
        return moderatorService.createModerator(createDTO);
    }

    @AdminOrCurrentModerator
    @PatchMapping("/{id}")
    public ModeratorReadDTO patchModerator(@PathVariable UUID id,
                                       @RequestBody ModeratorPatchDTO patch) {
        return moderatorService.patchModerator(id, patch);
    }

    @AdminOrCurrentModerator
    @PutMapping("/{id}")
    public ModeratorReadDTO updateModerator(@PathVariable UUID id, @RequestBody ModeratorPutDTO put) {
        return moderatorService.updateModerator(id, put);
    }

    @AdminOrCurrentModerator
    @DeleteMapping("/{id}")
    public void deleteModerator(@PathVariable UUID id) {
        moderatorService.deleteModerator(id);
    }

}
