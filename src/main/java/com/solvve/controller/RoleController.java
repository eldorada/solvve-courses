package com.solvve.controller;

import com.solvve.dto.RoleCreateDTO;
import com.solvve.dto.RolePatchDTO;
import com.solvve.dto.RolePutDTO;
import com.solvve.dto.RoleReadDTO;
import com.solvve.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = {"/api/v1"})
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping("/films/{filmId}/roles")
    public List<RoleReadDTO> getFilmsRoles(@PathVariable UUID filmId) {
        return roleService.getFilmsRoles(filmId);
    }

    @GetMapping("/films/{filmId}/roles/{id}")
    public RoleReadDTO getFilmsRoles(@PathVariable UUID filmId, @PathVariable UUID id) {
        return roleService.getFilmsRoles(filmId, id);
    }

    @PostMapping("/films/{filmId}/roles")
    public RoleReadDTO createFilmsRoles(@PathVariable UUID filmId, @RequestBody RoleCreateDTO createDTO) {
        return roleService.createFilmsRoles(filmId, createDTO);
    }

    @PatchMapping("/films/{filmId}/roles/{id}")
    public RoleReadDTO patchFilmsRoles(@PathVariable UUID filmId, @PathVariable UUID id,
                                       @RequestBody RolePatchDTO patch) {
        return roleService.patchFilmsRoles(filmId, id, patch);
    }

    @PutMapping("/films/{filmId}/roles/{id}")
    public RoleReadDTO updateFilmsRoles(@PathVariable UUID filmId, @PathVariable UUID id,
                                        @RequestBody RolePutDTO put) {
        return roleService.updateFilmsRoles(filmId, id, put);
    }

    @DeleteMapping("/films/{filmId}/roles/{id}")
    public void deleteFilmsRoles(@PathVariable UUID filmId, @PathVariable UUID id) {
        roleService.deleteFilmsRoles(filmId, id);
    }

}
