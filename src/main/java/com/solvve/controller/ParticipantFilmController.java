package com.solvve.controller;

import com.solvve.dto.ParticipantFilmCreateDTO;
import com.solvve.dto.ParticipantFilmPatchDTO;
import com.solvve.dto.ParticipantFilmPutDTO;
import com.solvve.dto.ParticipantFilmReadDTO;
import com.solvve.service.ParticipantFilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = {"/api/v1"})
public class ParticipantFilmController {

    @Autowired
    private ParticipantFilmService participantFilmService;

    @GetMapping("/films/{filmId}/participantFilms")
    public List<ParticipantFilmReadDTO> getFilmsParticipantFilms(@PathVariable UUID filmId) {
        return participantFilmService.getFilmsParticipantFilms(filmId);
    }

    @GetMapping("/films/{filmId}/participantFilms/{id}")
    public ParticipantFilmReadDTO getFilmsParticipantFilms(@PathVariable UUID filmId, @PathVariable UUID id) {
        return participantFilmService.getFilmsParticipantFilms(filmId, id);
    }

    @PostMapping("/films/{filmId}/participantFilms")
    public ParticipantFilmReadDTO createFilmsParticipantFilms(@PathVariable UUID filmId,
                                           @RequestBody ParticipantFilmCreateDTO createDTO) {
        return participantFilmService.createFilmsParticipantFilms(filmId, createDTO);
    }

    @PatchMapping("/films/{filmId}/participantFilms/{id}")
    public ParticipantFilmReadDTO patchFilmsParticipantFilms(@PathVariable UUID filmId, @PathVariable UUID id,
                                           @RequestBody ParticipantFilmPatchDTO patch) {
        return participantFilmService.patchFilmsParticipantFilms(filmId, id, patch);
    }

    @PutMapping("/films/{filmId}/participantFilms/{id}")
    public ParticipantFilmReadDTO updateFilmsParticipantFilms(@PathVariable UUID filmId, @PathVariable UUID id,
                                           @RequestBody ParticipantFilmPutDTO put) {
        return participantFilmService.updateFilmsParticipantFilms(filmId, id, put);
    }

    @DeleteMapping("/films/{filmId}/participantFilms/{id}")
    public void deleteFilmsParticipantFilms(@PathVariable UUID filmId, @PathVariable UUID id) {
        participantFilmService.deleteFilmsParticipantFilms(filmId, id);
    }

}
