package com.solvve.controller;

import com.solvve.controller.security.AdminOrCurrentManager;
import com.solvve.controller.security.OnlyAdmin;
import com.solvve.dto.*;
import com.solvve.service.ManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/managers")
public class ManagerController {

    @Autowired
    private ManagerService managerService;

    @AdminOrCurrentManager
    @GetMapping("/{id}/views")
    public List<ViewReadDTO> getManagerViews(@PathVariable UUID id) {
        return managerService.getManagerViews(id);
    }

    @AdminOrCurrentManager
    @GetMapping("/{id}")
    public ManagerReadDTO getManager(@PathVariable UUID id) {
        return managerService.getManager(id);
    }

    @OnlyAdmin
    @PostMapping
    public ManagerReadDTO createUser(@RequestBody ManagerCreateDTO createDTO) {
        return managerService.createManager(createDTO);
    }

    @AdminOrCurrentManager
    @PatchMapping("/{id}")
    public ManagerReadDTO patchManager(@PathVariable UUID id,
                                       @RequestBody ManagerPatchDTO patch) {
        return managerService.patchManager(id, patch);
    }

    @AdminOrCurrentManager
    @PutMapping("/{id}")
    public ManagerReadDTO updateManager(@PathVariable UUID id, @RequestBody ManagerPutDTO put) {
        return managerService.updateManager(id, put);
    }

    @AdminOrCurrentManager
    @DeleteMapping("/{id}")
    public void deleteManager(@PathVariable UUID id) {
        managerService.deleteManager(id);
    }

}
