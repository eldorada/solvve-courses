package com.solvve.controller;

import com.solvve.controller.security.AdminOrOther;
import com.solvve.dto.ApplicationUserCreateDTO;
import com.solvve.dto.ApplicationUserPatchDTO;
import com.solvve.dto.ApplicationUserPutDTO;
import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.service.ApplicationUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class ApplicationUserController {

    @Autowired
    private ApplicationUserService applicationUserService;

    @GetMapping("/{id}")
    public ApplicationUserReadDTO getApplicationUser(@PathVariable UUID id) {
        return applicationUserService.getApplicationUser(id);
    }

    @PostMapping
    public ApplicationUserReadDTO createUser(@RequestBody ApplicationUserCreateDTO createDTO) {
        return applicationUserService.createApplicationUser(createDTO);
    }

    @AdminOrOther
    @PatchMapping("/{id}")
    public ApplicationUserReadDTO patchApplicationUser(@PathVariable UUID id,
                                                       @RequestBody ApplicationUserPatchDTO patch) {
        return applicationUserService.patchApplicationUser(id, patch);
    }

    @AdminOrOther
    @PutMapping("/{id}")
    public ApplicationUserReadDTO updateApplicationUser(@PathVariable UUID id, @RequestBody ApplicationUserPutDTO put) {
        return applicationUserService.updateApplicationUser(id, put);
    }

    @AdminOrOther
    @DeleteMapping("/{id}")
    public void deleteApplicationUser(@PathVariable UUID id) {
        applicationUserService.deleteApplicationUser(id);
    }

}
