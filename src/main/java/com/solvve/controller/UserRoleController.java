package com.solvve.controller;

import com.solvve.controller.security.OnlyAdmin;
import com.solvve.dto.UserRoleReadDTO;
import com.solvve.service.UserRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class UserRoleController {

    @Autowired
    private UserRoleService userRoleService;

    @OnlyAdmin
    @GetMapping("/userRoles")
    public List<UserRoleReadDTO> getUserRole() {
        return userRoleService.getUserRoles();
    }

    @OnlyAdmin
    @GetMapping("/users/{userId}/userRoles")
    public List<UserRoleReadDTO> getApplicationUserUserRoles(@PathVariable UUID userId) {
        return userRoleService.getApplicationUsersUserRoles(userId);
    }

    @OnlyAdmin
    @PostMapping("/users/{userId}/userRoles/{id}")
    public List<UserRoleReadDTO> addUserRoleToFilm(@PathVariable UUID userId, @PathVariable UUID id) {
        return userRoleService.addUserRoleToApplicationUser(userId, id);
    }

    @OnlyAdmin
    @DeleteMapping("/users/{userId}/userRoles/{id}")
    public List<UserRoleReadDTO> removeUserRoleFromFilm(@PathVariable UUID userId, @PathVariable UUID id) {
        return userRoleService.removeUserRoleFromApplicationUser(userId, id);
    }

}
