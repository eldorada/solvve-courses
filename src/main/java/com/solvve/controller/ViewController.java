package com.solvve.controller;

import com.solvve.controller.documentation.ApiPageable;
import com.solvve.controller.validation.ControllerValidationUtil;
import com.solvve.dto.*;
import com.solvve.service.ViewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/views")
public class ViewController {

    @Autowired
    private ViewService viewService;

    @GetMapping("/{id}")
    public ViewReadExtendedDTO getView(@PathVariable UUID id) {
        return viewService.getView(id);
    }

    @ApiPageable
    @GetMapping
    public PageResult<ViewReadDTO> getViews(ViewFilter filter, @ApiIgnore Pageable pageable) {
        return viewService.getViews(filter, pageable);
    }

    @PostMapping
    public ViewReadDTO createView(@RequestBody @Valid ViewCreateDTO create) {
        ControllerValidationUtil.validateLessThan(create.getStartAt(), create.getFinishAt(),
                "startAt", "finishAt");
        return viewService.createView(create);
    }
}
