package com.solvve.controller;

import com.solvve.dto.CountryReadDTO;
import com.solvve.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public List<CountryReadDTO> getCountry() {
        return countryService.getCountries();
    }

    @GetMapping("/films/{filmId}/countries")
    public List<CountryReadDTO> getFilmsCountries(@PathVariable UUID filmId) {
        return countryService.getFilmsCountries(filmId);
    }

    @PostMapping("/films/{filmId}/countries/{id}")
    public List<CountryReadDTO> addCountryToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return countryService.addCountryToFilm(filmId, id);
    }

    @DeleteMapping("/films/{filmId}/countries/{id}")
    public List<CountryReadDTO> removeCountryFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return countryService.removeCountryFromFilm(filmId, id);
    }

}
