package com.solvve.controller;

import com.solvve.dto.SignalToManagerCreateDTO;
import com.solvve.dto.SignalToManagerReadDTO;
import com.solvve.service.SignalToManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class SignalToManagerController {

    @Autowired
    private SignalToManagerService signalToManagerService;

    @GetMapping("objects/{fixObjectId}/signalsToManager/{id}")
    public SignalToManagerReadDTO getSignalToManager(@PathVariable UUID fixObjectId, @PathVariable UUID id) {
        return signalToManagerService.getSignalToManager(fixObjectId, id);
    }

    @PostMapping("signalsToManager")
    public SignalToManagerReadDTO createSignalToManager(@RequestBody SignalToManagerCreateDTO createDTO) {
        return signalToManagerService.createSignalToManager(createDTO);
    }

    @PutMapping("objects/{fixObjectId}/signalsToManager/{id}")
    public void fixSignalToManager(@PathVariable UUID fixObjectId, @PathVariable UUID id) {
        signalToManagerService.fixSignalToManager(fixObjectId, id);
    }

    @DeleteMapping("objects/{fixObjectId}/signalsToManager/{id}")
    public void deleteSignalToManager(@PathVariable UUID fixObjectId, @PathVariable UUID id) {
        signalToManagerService.deleteSignalToManager(fixObjectId, id);
    }

}
