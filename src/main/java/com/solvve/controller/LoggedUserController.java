package com.solvve.controller;

import com.solvve.controller.security.AdminOrCurrentLoggedUser;
import com.solvve.controller.security.OnlyAdmin;
import com.solvve.dto.LoggedUserCreateDTO;
import com.solvve.dto.LoggedUserPatchDTO;
import com.solvve.dto.LoggedUserPutDTO;
import com.solvve.dto.LoggedUserReadDTO;
import com.solvve.service.LoggedUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/logged-users")
public class LoggedUserController {

    @Autowired
    private LoggedUserService loggedUserService;

    @AdminOrCurrentLoggedUser
    @GetMapping("/{id}")
    public LoggedUserReadDTO getLoggedUser(@PathVariable UUID id) {
        return loggedUserService.getLoggedUser(id);
    }

    @OnlyAdmin
    @PostMapping
    public LoggedUserReadDTO createUser(@RequestBody LoggedUserCreateDTO createDTO) {
        return loggedUserService.createLoggedUser(createDTO);
    }

    @AdminOrCurrentLoggedUser
    @PatchMapping("/{id}")
    public LoggedUserReadDTO patchLoggedUser(@PathVariable UUID id,
                                             @RequestBody LoggedUserPatchDTO patch) {
        return loggedUserService.patchLoggedUser(id, patch);
    }

    @AdminOrCurrentLoggedUser
    @PutMapping("/{id}")
    public LoggedUserReadDTO updateLoggedUser(@PathVariable UUID id, @RequestBody LoggedUserPutDTO put) {
        return loggedUserService.updateLoggedUser(id, put);
    }

    @AdminOrCurrentLoggedUser
    @DeleteMapping("/{id}")
    public void deleteLoggedUser(@PathVariable UUID id) {
        loggedUserService.deleteLoggedUser(id);
    }

}
