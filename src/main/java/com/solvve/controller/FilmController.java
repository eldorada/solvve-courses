package com.solvve.controller;

import com.solvve.dto.*;
import com.solvve.service.FilmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1/films")
public class FilmController {

    @Autowired
    private FilmService filmService;

    @GetMapping("/{id}")
    public FilmReadDTO getFilm(@PathVariable UUID id) {
        return filmService.getFilm(id);
    }

    @GetMapping("/leader-board")
    public List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard() {
        return filmService.getFilmsLeaderBoard();
    }

    @PostMapping
    public FilmReadDTO createFilm(@RequestBody FilmCreateDTO createDTO) {
        return filmService.createFilm(createDTO);
    }

    @PatchMapping("/{id}")
    public FilmReadDTO patchFilm(@PathVariable UUID id, @RequestBody FilmPatchDTO patch) {
        return filmService.patchFilm(id, patch);
    }

    @PutMapping("/{id}")
    public FilmReadDTO updateFilm(@PathVariable UUID id, @RequestBody FilmPutDTO put) {
        return filmService.updateFilm(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteFilm(@PathVariable UUID id) {
        filmService.deleteFilm(id);
    }

}
