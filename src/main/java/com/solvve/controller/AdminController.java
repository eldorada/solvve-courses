package com.solvve.controller;

import com.solvve.dto.AdminCreateDTO;
import com.solvve.dto.AdminPatchDTO;
import com.solvve.dto.AdminPutDTO;
import com.solvve.dto.AdminReadDTO;
import com.solvve.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/admins")
public class AdminController {

    @Autowired
    private AdminService adminService;

    @GetMapping("/{id}")
    public AdminReadDTO getAdmin(@PathVariable UUID id) {
        return adminService.getAdmin(id);
    }

    @PostMapping
    public AdminReadDTO createUser(@RequestBody AdminCreateDTO createDTO) {
        return adminService.createAdmin(createDTO);
    }

    @PatchMapping("/{id}")
    public AdminReadDTO patchAdmin(@PathVariable UUID id,
                                       @RequestBody AdminPatchDTO patch) {
        return adminService.patchAdmin(id, patch);
    }

    @PutMapping("/{id}")
    public AdminReadDTO updateAdmin(@PathVariable UUID id, @RequestBody AdminPutDTO put) {
        return adminService.updateAdmin(id, put);
    }

    @DeleteMapping("/{id}")
    public void deleteAdmin(@PathVariable UUID id) {
        adminService.deleteAdmin(id);
    }

}
