package com.solvve.controller;

import com.solvve.dto.ReviewCreateDTO;
import com.solvve.dto.ReviewPatchDTO;
import com.solvve.dto.ReviewPutDTO;
import com.solvve.dto.ReviewReadDTO;
import com.solvve.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping(value = {"/api/v1"})
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping("/reviews/{id}")
    public ReviewReadDTO getReview(@PathVariable UUID id) {
        return reviewService.getReview(id);
    }

    @GetMapping("/films/{filmId}/reviews")
    public List<ReviewReadDTO> getFilmsReviews(@PathVariable UUID filmId) {
        return reviewService.getFilmsReviews(filmId);
    }

    @GetMapping("/films/{filmId}/reviews/{id}")
    public ReviewReadDTO getFilmsReviews(@PathVariable UUID filmId, @PathVariable UUID id) {
        return reviewService.getFilmsReviews(filmId, id);
    }

    @PostMapping("/films/{filmId}/reviews")
    public ReviewReadDTO createFilmsReviews(@PathVariable UUID filmId, @RequestBody ReviewCreateDTO createDTO) {
        return reviewService.createFilmsReviews(filmId, createDTO);
    }

    @PatchMapping("/reviews/{id}")
    public ReviewReadDTO patchReview(@PathVariable UUID id, @RequestBody ReviewPatchDTO patch) {
        return reviewService.patchReview(id, patch);
    }

    @PatchMapping("/films/{filmId}/reviews/{id}")
    public ReviewReadDTO patchFilmsReviews(@PathVariable UUID filmId, @PathVariable UUID id,
                                           @RequestBody ReviewPatchDTO patch) {
        return reviewService.patchFilmsReviews(filmId, id, patch);
    }

    @PutMapping("/reviews/{id}")
    public ReviewReadDTO updateReview(@PathVariable UUID id, @RequestBody ReviewPutDTO put) {
        return reviewService.updateReview(id, put);
    }

    @PutMapping("/films/{filmId}/reviews/{id}")
    public ReviewReadDTO updateFilmsReviews(@PathVariable UUID filmId, @PathVariable UUID id,
                                            @RequestBody ReviewPutDTO put) {
        return reviewService.updateFilmsReviews(filmId, id, put);
    }

    @DeleteMapping("/reviews/{id}")
    public void deleteReview(@PathVariable UUID id) {
        reviewService.deleteReview(id);
    }

    @DeleteMapping("/films/{filmId}/reviews/{id}")
    public void deleteFilmsReviews(@PathVariable UUID filmId, @PathVariable UUID id) {
        reviewService.deleteFilmsReviews(filmId, id);
    }

}
