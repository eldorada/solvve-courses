package com.solvve.controller;

import com.solvve.dto.CompanyReadDTO;
import com.solvve.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class CompanyController {

    @Autowired
    private CompanyService companyService;

    @GetMapping("/companies")
    public List<CompanyReadDTO> getCompany() {
        return companyService.getCompanies();
    }

    @GetMapping("/films/{filmId}/companies")
    public List<CompanyReadDTO> getFilmsCompanies(@PathVariable UUID filmId) {
        return companyService.getFilmsCompanies(filmId);
    }

    @PostMapping("/films/{filmId}/companies/{id}")
    public List<CompanyReadDTO> addCompanyToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return companyService.addCompanyToFilm(filmId, id);
    }

    @DeleteMapping("/films/{filmId}/companies/{id}")
    public List<CompanyReadDTO> removeCompanyFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return companyService.removeCompanyFromFilm(filmId, id);
    }

}
