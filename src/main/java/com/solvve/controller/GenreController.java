package com.solvve.controller;

import com.solvve.dto.GenreReadDTO;
import com.solvve.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class GenreController {

    @Autowired
    private GenreService genreService;

    @GetMapping("/genres")
    public List<GenreReadDTO> getGenre() {
        return genreService.getGenres();
    }

    @GetMapping("/films/{filmId}/genres")
    public List<GenreReadDTO> getFilmsGenres(@PathVariable UUID filmId) {
        return genreService.getFilmsGenres(filmId);
    }

    @PostMapping("/films/{filmId}/genres/{id}")
    public List<GenreReadDTO> addGenreToFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return genreService.addGenreToFilm(filmId, id);
    }

    @DeleteMapping("/films/{filmId}/genres/{id}")
    public List<GenreReadDTO> removeGenreFromFilm(@PathVariable UUID filmId, @PathVariable UUID id) {
        return genreService.removeGenreFromFilm(filmId, id);
    }

}
