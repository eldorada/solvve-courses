package com.solvve.service;

import com.solvve.domain.LoggedUser;
import com.solvve.dto.LoggedUserCreateDTO;
import com.solvve.dto.LoggedUserPatchDTO;
import com.solvve.dto.LoggedUserPutDTO;
import com.solvve.dto.LoggedUserReadDTO;
import com.solvve.repository.LoggedUserRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LoggedUserService {

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public LoggedUserReadDTO getLoggedUser(UUID id) {
        LoggedUser loggedUser = repositoryHelper.getByIdRequired(LoggedUser.class, id);
        return translationService.translate(loggedUser, LoggedUserReadDTO.class);
    }

    public LoggedUserReadDTO createLoggedUser(LoggedUserCreateDTO create) {
        LoggedUser loggedUser = translationService.translate(create, LoggedUser.class);
        loggedUser = loggedUserRepository.save(loggedUser);
        return translationService.translate(loggedUser, LoggedUserReadDTO.class);
    }

    public LoggedUserReadDTO patchLoggedUser(UUID id, LoggedUserPatchDTO patch) {
        LoggedUser loggedUser = repositoryHelper.getByIdRequired(LoggedUser.class, id);

        translationService.map(patch, loggedUser);

        loggedUser = loggedUserRepository.save(loggedUser);
        return translationService.translate(loggedUser, LoggedUserReadDTO.class);

    }

    public LoggedUserReadDTO updateLoggedUser(UUID id, LoggedUserPutDTO put) {
        LoggedUser loggedUser = repositoryHelper.getByIdRequired(LoggedUser.class, id);

        translationService.map(put, loggedUser);

        loggedUser = loggedUserRepository.save(loggedUser);
        return translationService.translate(loggedUser, LoggedUserReadDTO.class);

    }

    public void deleteLoggedUser(UUID id) {
        loggedUserRepository.delete(repositoryHelper.getByIdRequired(LoggedUser.class, id));
    }

}
