package com.solvve.service;

import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.dto.*;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ViewService {

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public ViewReadExtendedDTO getView(UUID id) {
        View view = repositoryHelper.getByIdRequired(View.class, id);
        return translationService.translate(view, ViewReadExtendedDTO.class);
    }

    public PageResult<ViewReadDTO> getViews(ViewFilter filter, Pageable pageable) {
        Page<View> views = viewRepository.findByFilter(filter, pageable);
        return translationService.toPageResult(views, ViewReadDTO.class);
    }

    public ViewReadDTO createView(ViewCreateDTO create) {
        View view = translationService.translate(create, View.class);
        view.setStatus(ViewStatus.ONLINE);
        view = viewRepository.save(view);
        return translationService.translate(view, ViewReadDTO.class);
    }

}
