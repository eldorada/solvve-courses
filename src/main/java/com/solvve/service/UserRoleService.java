package com.solvve.service;

import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRole;
import com.solvve.dto.UserRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class UserRoleService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private TranslationService translationService;

    public List<UserRoleReadDTO> getUserRoles() {
        List<UserRole> userRoles = userRoleRepository.findAll();
        return translationService.translateList(userRoles, UserRoleReadDTO.class);
    }

    public List<UserRoleReadDTO> getApplicationUsersUserRoles(UUID applicationUserId) {
        List<UserRole> userRoles = userRoleRepository
                .findByApplicationUsers(repositoryHelper.getByIdRequired(ApplicationUser.class, applicationUserId));
        return translationService.translateList(userRoles, UserRoleReadDTO.class);
    }

    @Transactional
    public List<UserRoleReadDTO> addUserRoleToApplicationUser(UUID applicationUserId, UUID id) {
        ApplicationUser applicationUser = repositoryHelper.getByIdRequired(ApplicationUser.class, applicationUserId);
        UserRole userRole = repositoryHelper.getByIdRequired(UserRole.class, id);

        if (applicationUser.getUserRoles().stream().anyMatch(ur -> ur.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("ApplicationUser %s already has userRole %s",
                    applicationUserId, id));
        }

        applicationUser.getUserRoles().add(userRole);
        applicationUser = applicationUserRepository.save(applicationUser);

        return translationService.translateList(applicationUser.getUserRoles(), UserRoleReadDTO.class);
    }

    @Transactional
    public List<UserRoleReadDTO> removeUserRoleFromApplicationUser(UUID applicationUserId, UUID id) {
        ApplicationUser applicationUser = repositoryHelper.getByIdRequired(ApplicationUser.class, applicationUserId);

        boolean removed = applicationUser.getUserRoles().removeIf(ur -> ur.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("ApplicationUser " + applicationUserId + " has no userRole" + id);
        }

        applicationUser = applicationUserRepository.save(applicationUser);

        return translationService.translateList(applicationUser.getUserRoles(), UserRoleReadDTO.class);
    }
}
