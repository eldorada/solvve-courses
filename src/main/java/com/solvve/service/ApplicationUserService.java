package com.solvve.service;

import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.ApplicationUserCreateDTO;
import com.solvve.dto.ApplicationUserPatchDTO;
import com.solvve.dto.ApplicationUserPutDTO;
import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ApplicationUserService {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    @Autowired
    private UserRoleService userRoleService;

    public ApplicationUserReadDTO getApplicationUser(UUID id) {
        ApplicationUser applicationUser = repositoryHelper.getByIdRequired(ApplicationUser.class, id);
        return translationService.translate(applicationUser, ApplicationUserReadDTO.class);
    }

    public ApplicationUserReadDTO createApplicationUser(ApplicationUserCreateDTO create) {
        ApplicationUser applicationUser = translationService.translate(create, ApplicationUser.class);
        applicationUser = applicationUserRepository.save(applicationUser);

        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.LOGGED_USER);
        userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);
        return translationService.translate(applicationUser, ApplicationUserReadDTO.class);
    }

    public ApplicationUserReadDTO patchApplicationUser(UUID id, ApplicationUserPatchDTO patch) {
        ApplicationUser applicationUser = repositoryHelper.getByIdRequired(ApplicationUser.class, id);

        translationService.map(patch, applicationUser);

        applicationUser = applicationUserRepository.save(applicationUser);
        return translationService.translate(applicationUser, ApplicationUserReadDTO.class);

    }

    public ApplicationUserReadDTO updateApplicationUser(UUID id, ApplicationUserPutDTO put) {
        ApplicationUser applicationUser = repositoryHelper.getByIdRequired(ApplicationUser.class, id);

        translationService.map(put, applicationUser);

        applicationUser = applicationUserRepository.save(applicationUser);
        return translationService.translate(applicationUser, ApplicationUserReadDTO.class);

    }

    public void deleteApplicationUser(UUID id) {
        applicationUserRepository.delete(repositoryHelper.getByIdRequired(ApplicationUser.class, id));
    }

}
