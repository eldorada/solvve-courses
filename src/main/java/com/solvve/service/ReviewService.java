package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Review;
import com.solvve.dto.*;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ReviewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ReviewService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    public ReviewReadDTO getReview(UUID id) {
        Review review = repositoryHelper.getByIdRequired(Review.class, id);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public List<ReviewReadDTO> getFilmsReviews(UUID filmId) {
        List<Review> reviews = reviewRepository.findByFilm(repositoryHelper.getByIdRequired(Film.class, filmId));
        return translationService.translateList(reviews, ReviewReadDTO.class);
    }

    public ReviewReadDTO getFilmsReviews(UUID filmId, UUID id) {
        Review review = reviewRepository.findByFilmAndId(repositoryHelper.getByIdRequired(Film.class, filmId), id);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO createFilmsReviews(UUID filmId, ReviewCreateDTO create) {
        Review review = translationService.translate(create, Review.class);
        review.setFilm(repositoryHelper.getReferenceIfExist(Film.class, filmId));
        review = reviewRepository.save(review);

        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchReview(UUID id, ReviewPatchDTO patch) {
        Review review = repositoryHelper.getByIdRequired(Review.class, id);

        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO patchFilmsReviews(UUID filmId, UUID id, ReviewPatchDTO patch) {
        Review review = reviewRepository.findByFilmAndId(repositoryHelper.getByIdRequired(Film.class, filmId), id);

        translationService.map(patch, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateReview(UUID id, ReviewPutDTO put) {
        Review review = repositoryHelper.getByIdRequired(Review.class, id);

        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public ReviewReadDTO updateFilmsReviews(UUID filmId, UUID id, ReviewPutDTO put) {
        Review review = reviewRepository.findByFilmAndId(repositoryHelper.getByIdRequired(Film.class, filmId), id);

        translationService.map(put, review);

        review = reviewRepository.save(review);
        return translationService.translate(review, ReviewReadDTO.class);
    }

    public void deleteReview(UUID id) {
        reviewRepository.delete(repositoryHelper.getByIdRequired(Review.class, id));
    }

    public void deleteFilmsReviews(UUID filmId, UUID id) {
        reviewRepository.delete(reviewRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id));
    }

}
