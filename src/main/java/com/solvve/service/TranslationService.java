package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.*;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.bitbucket.brunneng.ot.Configuration;
import org.bitbucket.brunneng.ot.ObjectTranslator;
import org.bitbucket.brunneng.ot.exceptions.TranslationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
public class TranslationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    private ObjectTranslator objectTranslator;

    public TranslationService() {
        objectTranslator = new ObjectTranslator(createConfiguration());
    }

    private Configuration createConfiguration() {
        Configuration c = new Configuration();
        configureForAbstractEntity(c);
        configureForView(c);
        configureForReview(c);
        configureForSignalToManager(c);
        configureForApplicationUser(c);
        configureForFilm(c);
        configureForLike(c);
        configureForLoggedUser(c);
        configureForManager(c);
        configureForModerator(c);
        configureForAdmin(c);
        configureForParticipantFilm(c);
        configureForRole(c);
        configureForPerson(c);
        configureForActor(c);

        return  c;
    }

    private void configureForApplicationUser(Configuration c) {
        c.beanOfClass(ApplicationUserPatchDTO.class).translationTo(ApplicationUser.class).mapOnlyNotNullProperties();
    }

    private void configureForLoggedUser(Configuration c) {
        c.beanOfClass(LoggedUserPatchDTO.class).translationTo(LoggedUser.class).mapOnlyNotNullProperties();
    }

    private void configureForManager(Configuration c) {
        c.beanOfClass(ManagerPatchDTO.class).translationTo(Manager.class).mapOnlyNotNullProperties();
    }
    
    private void configureForModerator(Configuration c) {
        c.beanOfClass(ModeratorPatchDTO.class).translationTo(Moderator.class).mapOnlyNotNullProperties();
    }

    private void configureForAdmin(Configuration c) {
        c.beanOfClass(AdminPatchDTO.class).translationTo(Admin.class).mapOnlyNotNullProperties();
    }
    
    private void configureForFilm(Configuration c) {
        c.beanOfClass(FilmPatchDTO.class).translationTo(Film.class).mapOnlyNotNullProperties();
    }

    private void configureForPerson(Configuration c) {
        c.beanOfClass(PersonPatchDTO.class).translationTo(Person.class).mapOnlyNotNullProperties();
    }
    
    private void configureForLike(Configuration c) {
        c.beanOfClass(LikePatchDTO.class).translationTo(Like.class).mapOnlyNotNullProperties();
    }

    private void configureForView(Configuration c) {
        Configuration.Translation t = c.beanOfClass(View.class).translationTo(ViewReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        t.srcProperty("loggedUser.id").translatesTo("loggedUserId");

        Configuration.Translation fromCreateToEntity = c.beanOfClass(ViewCreateDTO.class).translationTo(View.class);
        fromCreateToEntity.srcProperty("filmId").translatesTo("film.id");
        fromCreateToEntity.srcProperty("loggedUserId").translatesTo("loggedUser.id");

    }

    private void configureForReview(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Review.class).translationTo(ReviewReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        c.beanOfClass(ReviewPatchDTO.class).translationTo(Review.class).mapOnlyNotNullProperties();

    }

    private void configureForActor(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Actor.class).translationTo(ActorReadDTO.class);
        t.srcProperty("person.id").translatesTo("personId");
        c.beanOfClass(ActorPatchDTO.class).translationTo(Actor.class).mapOnlyNotNullProperties();

    }

    private void configureForSignalToManager(Configuration c) {
        Configuration.Translation t = c.beanOfClass(SignalToManager.class).translationTo(SignalToManagerReadDTO.class);
        t.srcProperty("manager.id").translatesTo("managerId");
        t.srcProperty("loggedUser.id").translatesTo("loggedUserId");

        Configuration.Translation fromCreateToEntity = c.beanOfClass(SignalToManagerCreateDTO.class)
                .translationTo(SignalToManager.class);
        fromCreateToEntity.srcProperty("managerId").translatesTo("manager.id");
        fromCreateToEntity.srcProperty("loggedUserId").translatesTo("loggedUser.id");

    }

    private void configureForParticipantFilm(Configuration c) {
        Configuration.Translation t = c.beanOfClass(ParticipantFilm.class).translationTo(ParticipantFilmReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        t.srcProperty("person.id").translatesTo("personId");

        c.beanOfClass(ParticipantFilmPatchDTO.class).translationTo(ParticipantFilm.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(ParticipantFilmCreateDTO.class)
                .translationTo(ParticipantFilm.class);
        fromCreateToEntity.srcProperty("filmId").translatesTo("film.id");
        fromCreateToEntity.srcProperty("personId").translatesTo("person.id");

    }

    private void configureForRole(Configuration c) {
        Configuration.Translation t = c.beanOfClass(Role.class).translationTo(RoleReadDTO.class);
        t.srcProperty("film.id").translatesTo("filmId");
        t.srcProperty("actor.id").translatesTo("actorId");
        c.beanOfClass(RolePatchDTO.class).translationTo(Role.class).mapOnlyNotNullProperties();

        c.beanOfClass(ParticipantFilmPatchDTO.class).translationTo(ParticipantFilm.class).mapOnlyNotNullProperties();

        Configuration.Translation fromCreateToEntity = c.beanOfClass(RoleCreateDTO.class)
                .translationTo(Role.class);
        fromCreateToEntity.srcProperty("filmId").translatesTo("film.id");
        fromCreateToEntity.srcProperty("actorId").translatesTo("actor.id");

    }

    private void configureForAbstractEntity(Configuration c) {
        c.beanOfClass(AbstractEntity.class).setIdentifierProperty("id");
        c.beanOfClass(AbstractEntity.class).setBeanFinder(
                (beanClass, id) -> repositoryHelper.getReferenceIfExist(beanClass, (UUID) id));
        
    }

    public <T> T translate(Object srcObject, Class<T> targetClass) {
        try {
            return objectTranslator.translate(srcObject, targetClass);
        }
        catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <T> void map(Object srcObject, Object destObject) {
        try {
            objectTranslator.mapBean(srcObject, destObject);
        }
        catch (TranslationException e) {
            log.warn(e.getMessage());
            throw (RuntimeException) e.getCause();
        }
    }

    public <E, T> PageResult<T> toPageResult(Page<E> page, Class<T> dtoType) {
        PageResult<T> res = new PageResult<>();
        res.setPage(page.getNumber());
        res.setPageSize(page.getSize());
        res.setTotalPages(page.getTotalPages());
        res.setTotalElements(page.getTotalElements());
        res.setData(page.getContent().stream().map(e -> translate(e, dtoType)).collect(Collectors.toList()));
        return res;
    }

    public  <T> List<T> translateList(List<?> objects, Class<T> targetClass) {
        return objects.stream().map(o -> translate(o, targetClass)).collect(Collectors.toList());
    }
}
