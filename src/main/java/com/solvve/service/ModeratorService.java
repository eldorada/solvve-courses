package com.solvve.service;

import com.solvve.domain.Moderator;
import com.solvve.dto.ModeratorCreateDTO;
import com.solvve.dto.ModeratorPatchDTO;
import com.solvve.dto.ModeratorPutDTO;
import com.solvve.dto.ModeratorReadDTO;
import com.solvve.repository.ModeratorRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ModeratorService {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public ModeratorReadDTO getModerator(UUID id) {
        Moderator moderator = repositoryHelper.getByIdRequired(Moderator.class, id);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO createModerator(ModeratorCreateDTO create) {
        Moderator moderator = translationService.translate(create, Moderator.class);
        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);
    }

    public ModeratorReadDTO patchModerator(UUID id, ModeratorPatchDTO patch) {
        Moderator moderator = repositoryHelper.getByIdRequired(Moderator.class, id);

        translationService.map(patch, moderator);

        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);

    }

    public ModeratorReadDTO updateModerator(UUID id, ModeratorPutDTO put) {
        Moderator moderator = repositoryHelper.getByIdRequired(Moderator.class, id);

        translationService.map(put, moderator);

        moderator = moderatorRepository.save(moderator);
        return translationService.translate(moderator, ModeratorReadDTO.class);

    }

    public void deleteModerator(UUID id) {
        moderatorRepository.delete(repositoryHelper.getByIdRequired(Moderator.class, id));
    }

}
