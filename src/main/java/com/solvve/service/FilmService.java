package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ViewRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class FilmService {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public FilmReadDTO getFilm(UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, id);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard() {
        return filmRepository.getFilmsLeaderBoard();
    }

    public FilmReadDTO createFilm(FilmCreateDTO create) {
        Film film = translationService.translate(create, Film.class);

        film = filmRepository.save(film);
        return translationService.translate(film, FilmReadDTO.class);
    }

    public FilmReadDTO patchFilm(UUID id, FilmPatchDTO patch) {
        Film film = repositoryHelper.getByIdRequired(Film.class, id);

        translationService.map(patch, film);

        film = filmRepository.save(film);

        return translationService.translate(film, FilmReadDTO.class);

    }

    public FilmReadDTO updateFilm(UUID id, FilmPutDTO put) {
        Film film = repositoryHelper.getByIdRequired(Film.class, id);

        translationService.map(put, film);

        film = filmRepository.save(film);

        return translationService.translate(film, FilmReadDTO.class);

    }

    public void deleteFilm(UUID id) {
        filmRepository.delete(repositoryHelper.getByIdRequired(Film.class, id));
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void updateAverageMarkOfFilm(UUID filmId) {
        Double averageMark = viewRepository.calcAverageMarkOfFilm(filmId);
        Film film = filmRepository.findById(filmId).orElseThrow(
                () -> new EntityNotFoundException(Film.class, filmId));

        log.info("Seting new average mark of film: {}. old value: {}, new value: {}",
                filmId, film.getAverageMark(), averageMark);
        film.setAverageMark(averageMark);
        filmRepository.save(film);
    }
}
