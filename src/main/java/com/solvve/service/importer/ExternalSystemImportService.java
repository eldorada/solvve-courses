package com.solvve.service.importer;

import com.solvve.domain.*;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.repository.ExternalSystemImportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ExternalSystemImportService {

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    public void validateNotImported(Class<? extends AbstractEntity> entityToImport,
                                    String idInExternalSystem) throws ImportAlreadyPerformedException {
        ImportedEntityType importedEntityType = getImportedEntityType(entityToImport);
        ExternalSystemImport esi = externalSystemImportRepository.findByIdInExternalSystemAndEntityType(
                idInExternalSystem, importedEntityType);

        if (esi != null) {
            throw new ImportAlreadyPerformedException(esi);
        }
    }

    private ImportedEntityType getImportedEntityType(Class<? extends AbstractEntity> entityToImport) {
        if (Film.class.equals(entityToImport)) {
            return ImportedEntityType.FILM;
        } else if (Person.class.equals(entityToImport)) {
            return ImportedEntityType.PERSON;
        }

        throw new IllegalArgumentException("Importing of entities " + entityToImport + " is not supported");
    }

    public <T extends AbstractEntity> UUID createExternalSystemImport(T entity, String idInExternalSystem) {
        ImportedEntityType importedEntityType = getImportedEntityType(entity.getClass());
        ExternalSystemImport esi = new ExternalSystemImport();
        esi.setEntityType(importedEntityType);
        esi.setEntityId(entity.getId());
        esi.setIdInExternalSystem(idInExternalSystem);
        esi = externalSystemImportRepository.save(esi);
        return esi.getId();
    }
}
