package com.solvve.service.importer;

import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.MovieReadDTO;
import com.solvve.domain.Film;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.repository.FilmRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Slf4j
@Service
public class MovieImporterService {

    @Autowired
    private TheMovieDbClient movieDbClient;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Transactional
    public UUID importFIlm(String filmExternalId)
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        log.info("Importing movie with external id={}", filmExternalId);

        externalSystemImportService.validateNotImported(Film.class, filmExternalId);
        MovieReadDTO read = movieDbClient.getMovie(filmExternalId);
        Film existingFilm = filmRepository.findByTitle(read.getTitle());
        if (existingFilm != null) {
            throw new ImportedEntityAlreadyExistException(Film.class, existingFilm.getId(),
                    "FIlm with name =" + read.getTitle() + "already exist");
        }

        Film film = new Film();
        film.setTitle(read.getTitle());
        film.setOriginalTitle(read.getOriginalTitle());
        filmRepository.save(film);

        externalSystemImportService.createExternalSystemImport(film, filmExternalId);

        log.info("Importing movie with external id={} imported", filmExternalId);

        return film.getId();
    }
}
