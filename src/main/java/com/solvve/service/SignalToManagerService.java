package com.solvve.service;

import com.solvve.domain.*;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.repository.ReviewRepository;
import com.solvve.repository.SignalToManagerRepository;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Slf4j
@Service
public class SignalToManagerService {

    @Autowired
    private SignalToManagerRepository signalToManagerRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private TranslationService translationService;

    public SignalToManagerReadDTO getSignalToManager(UUID fixObjectId, UUID id) {
        SignalToManager signalToManager = signalToManagerRepository.findByFixObjectIdAndId(fixObjectId, id);
        return translationService.translate(signalToManager, SignalToManagerReadDTO.class);
    }

    public List<SignalToManagerReadDTO> getSignalsToManager(SignalToManagerFilter filter) {
        List<SignalToManager> signalsToManager = signalToManagerRepository.findByFilter(filter);
        return translationService.translateList(signalsToManager, SignalToManagerReadDTO.class);
    }

    public SignalToManagerReadDTO createSignalToManager(SignalToManagerCreateDTO create) {
        SignalToManager signalToManager = translationService.translate(create, SignalToManager.class);
        signalToManager.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        signalToManager = signalToManagerRepository.save(signalToManager);
        return translationService.translate(signalToManager, SignalToManagerReadDTO.class);
    }

    public void deleteSignalToManager(UUID fixObjectSignal, UUID id) {
        signalToManagerRepository.delete(signalToManagerRepository.findByFixObjectIdAndId(fixObjectSignal, id));
    }

    @Transactional
    public void fixSignalToManager(UUID fixObjectId, UUID signalToManagerId) {

        SignalToManager signalToManager = repositoryHelper.getByIdRequired(SignalToManager.class, signalToManagerId);

        if (signalToManager.getStatus() != SignalToManagerStatus.NEED_TO_FIX) {
            throw new EntityWrongStatusException(String.format("Entity %s with id=%s already was modify!!",
                    signalToManager.getType(), fixObjectId));
        } else {
            log.info("Set correction text to review: {}. old value: {}, new value: {}",
                    fixObjectId, signalToManager.getRemarkText(), signalToManager.getCorrectionText());

            switch (signalToManager.getType()) {
              case REVIEW:
                  replaceInReview(fixObjectId, signalToManager);
                  break;
              default:
                  throw new EntityNotFoundException(SignalToManager.class, signalToManagerId);
            }
        }

    }

    public void replaceInReview(UUID id, SignalToManager signalToManager) {
        Review review = repositoryHelper.getByIdRequired(Review.class, id);

        if (!review.getText().substring(signalToManager.getRemarkTextStart(),
                signalToManager.getRemarkTextEnd()).equals(signalToManager.getRemarkText())) {
            signalToManager.setStatus(SignalToManagerStatus.REJECTED);
        } else {
            String startPartStr = review.getText().substring(0, signalToManager.getRemarkTextStart());
            String replacePartStr = review.getText().substring(signalToManager.getRemarkTextStart(),
                    signalToManager.getRemarkTextEnd());
            String endPartStr = review.getText().substring(signalToManager.getRemarkTextEnd());
            review.setText(startPartStr
                    + replacePartStr.replace(signalToManager.getRemarkText(), signalToManager.getCorrectionText())
                    + endPartStr);
            signalToManager.setStatus(SignalToManagerStatus.FIXED);
            reviewRepository.save(review);
            //repositoryHelper.updateEntity(Review.class, review.getId());
        }

        signalToManagerRepository.save(signalToManager);

    }

    public void fixAllSignalsToManager(UUID fixObjectId, String remarkText) {

        SignalToManagerFilter filter = new SignalToManagerFilter();
        filter.setFixObjectId(fixObjectId);
        filter.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        filter.setRemarkText(remarkText);

        signalToManagerRepository.findByFilter(filter).forEach(signalToManager -> {
            try {
                signalToManager.setStatus(SignalToManagerStatus.FIXED);
                signalToManagerRepository.save(signalToManager);
            } catch (Exception e) {
                log.error("Failed to update review signal: {}", signalToManager.getId(), e);
            }
        });
    }
}
