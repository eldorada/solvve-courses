package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Country;
import com.solvve.dto.CountryReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class CountryService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TranslationService translationService;

    public List<CountryReadDTO> getCountries() {
        List<Country> countries = countryRepository.findAll();
        return translationService.translateList(countries, CountryReadDTO.class);
    }

    public List<CountryReadDTO> getFilmsCountries(UUID filmId) {
        List<Country> countries = countryRepository.findByFilms(repositoryHelper.getByIdRequired(Film.class, filmId));
        return translationService.translateList(countries, CountryReadDTO.class);
    }

    @Transactional
    public List<CountryReadDTO> addCountryToFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);
        Country country = repositoryHelper.getByIdRequired(Country.class, id);

        if (film.getCountries().stream().anyMatch(cntr -> cntr.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has country %s", filmId, id));
        }

        film.getCountries().add(country);
        film = filmRepository.save(film);

        return translationService.translateList(film.getCountries(), CountryReadDTO.class);
    }

    @Transactional
    public List<CountryReadDTO> removeCountryFromFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);

        boolean removed = film.getCountries().removeIf(cntr -> cntr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no country" + id);
        }

        film = filmRepository.save(film);

        return translationService.translateList(film.getCountries(), CountryReadDTO.class);
    }
}
