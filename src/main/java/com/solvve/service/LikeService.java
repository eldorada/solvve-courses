package com.solvve.service;

import com.solvve.domain.Like;
import com.solvve.dto.LikeCreateDTO;
import com.solvve.dto.LikePatchDTO;
import com.solvve.dto.LikePutDTO;
import com.solvve.dto.LikeReadDTO;
import com.solvve.repository.LikeRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class LikeService {

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public LikeReadDTO getLike(UUID id) {
        Like like = repositoryHelper.getByIdRequired(Like.class, id);
        return translationService.translate(like, LikeReadDTO.class);
    }

    public LikeReadDTO createLike(LikeCreateDTO create) {
        Like like = translationService.translate(create, Like.class);

        like = likeRepository.save(like);
        return translationService.translate(like, LikeReadDTO.class);
    }

    public LikeReadDTO patchLike(UUID id, LikePatchDTO patch) {
        Like like = repositoryHelper.getByIdRequired(Like.class, id);

        translationService.map(patch, like);

        like = likeRepository.save(like);

        return translationService.translate(like, LikeReadDTO.class);

    }

    public LikeReadDTO updateLike(UUID id, LikePutDTO put) {
        Like like = repositoryHelper.getByIdRequired(Like.class, id);

        translationService.map(put, like);

        like = likeRepository.save(like);

        return translationService.translate(like, LikeReadDTO.class);

    }

    public void deleteLike(UUID id) {
        likeRepository.delete(repositoryHelper.getByIdRequired(Like.class, id));
    }

}
