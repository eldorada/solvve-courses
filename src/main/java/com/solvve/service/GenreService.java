package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Genre;
import com.solvve.dto.GenreReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.GenreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class GenreService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TranslationService translationService;

    public List<GenreReadDTO> getGenres() {
        List<Genre> genres = genreRepository.findAll();
        return translationService.translateList(genres, GenreReadDTO.class);
    }

    public List<GenreReadDTO> getFilmsGenres(UUID filmId) {
        List<Genre> genres = genreRepository.findByFilms(repositoryHelper.getByIdRequired(Film.class, filmId));
        return translationService.translateList(genres, GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> addGenreToFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);
        Genre genre = repositoryHelper.getByIdRequired(Genre.class, id);

        if (film.getGenres().stream().anyMatch(gnr -> gnr.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has genre %s", filmId, id));
        }

        film.getGenres().add(genre);
        film = filmRepository.save(film);

        return translationService.translateList(film.getGenres(), GenreReadDTO.class);
    }

    @Transactional
    public List<GenreReadDTO> removeGenreFromFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);

        boolean removed = film.getGenres().removeIf(gnr -> gnr.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no genre" + id);
        }

        film = filmRepository.save(film);

        return translationService.translateList(film.getGenres(), GenreReadDTO.class);
    }
}
