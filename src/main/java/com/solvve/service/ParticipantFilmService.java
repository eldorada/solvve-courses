package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.ParticipantFilm;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ParticipantFilmRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ParticipantFilmService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ParticipantFilmRepository participantFilmRepository;

    @Autowired
    private TranslationService translationService;

    public List<ParticipantFilmReadDTO> getFilmsParticipantFilms(UUID filmId) {
        List<ParticipantFilm> participantFilms = participantFilmRepository.findByFilm(repositoryHelper
                .getByIdRequired(Film.class, filmId));
        return translationService.translateList(participantFilms, ParticipantFilmReadDTO.class);
    }

    public ParticipantFilmReadDTO getFilmsParticipantFilms(UUID filmId, UUID id) {
        ParticipantFilm participantFilm = participantFilmRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        if (participantFilm  == null) {
            throw new EntityNotFoundException(ParticipantFilm.class, id);
        }

        return translationService.translate(participantFilm, ParticipantFilmReadDTO.class);
    }

    public ParticipantFilmReadDTO createFilmsParticipantFilms(UUID filmId, ParticipantFilmCreateDTO create) {
        ParticipantFilm participantFilm = translationService.translate(create, ParticipantFilm.class);
        participantFilm.setFilm(repositoryHelper.getReferenceIfExist(Film.class, filmId));
        participantFilm = participantFilmRepository.save(participantFilm);

        return translationService.translate(participantFilm, ParticipantFilmReadDTO.class);
    }

    public ParticipantFilmReadDTO patchFilmsParticipantFilms(UUID filmId, UUID id, ParticipantFilmPatchDTO patch) {
        ParticipantFilm participantFilm = participantFilmRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        translationService.map(patch, participantFilm);

        participantFilm = participantFilmRepository.save(participantFilm);
        return translationService.translate(participantFilm, ParticipantFilmReadDTO.class);
    }

    public ParticipantFilmReadDTO updateFilmsParticipantFilms(UUID filmId, UUID id, ParticipantFilmPutDTO put) {
        ParticipantFilm participantFilm = participantFilmRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        translationService.map(put, participantFilm);

        participantFilm = participantFilmRepository.save(participantFilm);
        return translationService.translate(participantFilm, ParticipantFilmReadDTO.class);
    }

    public void deleteFilmsParticipantFilms(UUID filmId, UUID id) {
        ParticipantFilm participantFilm = participantFilmRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        if (participantFilm  == null) {
            throw new EntityNotFoundException(ParticipantFilm.class, id);
        }
        participantFilmRepository.delete(participantFilm);
    }

}
