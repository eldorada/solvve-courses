package com.solvve.service;

import com.solvve.domain.Admin;
import com.solvve.dto.AdminCreateDTO;
import com.solvve.dto.AdminPatchDTO;
import com.solvve.dto.AdminPutDTO;
import com.solvve.dto.AdminReadDTO;
import com.solvve.repository.AdminRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class AdminService {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public AdminReadDTO getAdmin(UUID id) {
        Admin admin = repositoryHelper.getByIdRequired(Admin.class, id);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO createAdmin(AdminCreateDTO create) {
        Admin admin = translationService.translate(create, Admin.class);
        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);
    }

    public AdminReadDTO patchAdmin(UUID id, AdminPatchDTO patch) {
        Admin admin = repositoryHelper.getByIdRequired(Admin.class, id);

        translationService.map(patch, admin);

        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);

    }

    public AdminReadDTO updateAdmin(UUID id, AdminPutDTO put) {
        Admin admin = repositoryHelper.getByIdRequired(Admin.class, id);

        translationService.map(put, admin);

        admin = adminRepository.save(admin);
        return translationService.translate(admin, AdminReadDTO.class);

    }

    public void deleteAdmin(UUID id) {
        adminRepository.delete(repositoryHelper.getByIdRequired(Admin.class, id));
    }

}
