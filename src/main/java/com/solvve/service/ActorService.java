package com.solvve.service;

import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.dto.ActorCreateDTO;
import com.solvve.dto.ActorPatchDTO;
import com.solvve.dto.ActorPutDTO;
import com.solvve.dto.ActorReadDTO;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.RepositoryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class ActorService {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public ActorReadDTO getActor(UUID id) {
        Actor actor = repositoryHelper.getByIdRequired(Actor.class, id);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public ActorReadDTO createPersonsActor(UUID personId, ActorCreateDTO create) {
        Actor actor = translationService.translate(create, Actor.class);
        actor.setPerson(repositoryHelper.getReferenceIfExist(Person.class, personId));
        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);
    }

    public ActorReadDTO patchActor(UUID id, ActorPatchDTO patch) {
        Actor actor = repositoryHelper.getByIdRequired(Actor.class, id);

        translationService.map(patch, actor);

        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);

    }

    public ActorReadDTO updateActor(UUID id, ActorPutDTO put) {
        Actor actor = repositoryHelper.getByIdRequired(Actor.class, id);

        translationService.map(put, actor);

        actor = actorRepository.save(actor);
        return translationService.translate(actor, ActorReadDTO.class);

    }

    public void deleteActor(UUID id) {
        actorRepository.delete(repositoryHelper.getByIdRequired(Actor.class, id));
    }

}
