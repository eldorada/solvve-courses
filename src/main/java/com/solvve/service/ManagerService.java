package com.solvve.service;

import com.solvve.domain.ApplicationUser;
import com.solvve.domain.Manager;
import com.solvve.domain.View;
import com.solvve.dto.*;
import com.solvve.repository.ManagerRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.ViewRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ManagerService {

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private TranslationService translationService;

    public List<ViewReadDTO> getManagerViews(UUID id) {
        List<View> views = null;
        if (repositoryHelper.getByIdRequired(ApplicationUser.class, id) != null) {
            views = viewRepository.findViewForManager(id);
        }
        return translationService.translateList(views, ViewReadDTO.class);
    }

    public ManagerReadDTO getManager(UUID id) {
        Manager manager = repositoryHelper.getByIdRequired(Manager.class, id);
        return translationService.translate(manager, ManagerReadDTO.class);
    }

    public ManagerReadDTO createManager(ManagerCreateDTO create) {
        Manager manager = translationService.translate(create, Manager.class);
        manager = managerRepository.save(manager);
        return translationService.translate(manager, ManagerReadDTO.class);
    }

    public ManagerReadDTO patchManager(UUID id, ManagerPatchDTO patch) {
        Manager manager = repositoryHelper.getByIdRequired(Manager.class, id);

        translationService.map(patch, manager);

        manager = managerRepository.save(manager);
        return translationService.translate(manager, ManagerReadDTO.class);

    }

    public ManagerReadDTO updateManager(UUID id, ManagerPutDTO put) {
        Manager manager = repositoryHelper.getByIdRequired(Manager.class, id);

        translationService.map(put, manager);

        manager = managerRepository.save(manager);
        return translationService.translate(manager, ManagerReadDTO.class);

    }

    public void deleteManager(UUID id) {
        managerRepository.delete(repositoryHelper.getByIdRequired(Manager.class, id));
    }

}
