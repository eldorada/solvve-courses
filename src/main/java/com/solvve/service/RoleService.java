package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Role;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class RoleService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private TranslationService translationService;

    public List<RoleReadDTO> getFilmsRoles(UUID filmId) {
        List<Role> roles = roleRepository.findByFilm(repositoryHelper
                .getByIdRequired(Film.class, filmId));
        return translationService.translateList(roles, RoleReadDTO.class);
    }

    public RoleReadDTO getFilmsRoles(UUID filmId, UUID id) {
        Role role = roleRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        if (role  == null) {
            throw new EntityNotFoundException(Role.class, id);
        }

        return translationService.translate(role, RoleReadDTO.class);
    }

    public RoleReadDTO createFilmsRoles(UUID filmId, RoleCreateDTO create) {
        Role role = translationService.translate(create, Role.class);
        role.setFilm(repositoryHelper.getReferenceIfExist(Film.class, filmId));
        role = roleRepository.save(role);

        return translationService.translate(role, RoleReadDTO.class);
    }

    public RoleReadDTO patchFilmsRoles(UUID filmId, UUID id, RolePatchDTO patch) {
        Role role = roleRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        translationService.map(patch, role);

        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public RoleReadDTO updateFilmsRoles(UUID filmId, UUID id, RolePutDTO put) {
        Role role = roleRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        translationService.map(put, role);

        role = roleRepository.save(role);
        return translationService.translate(role, RoleReadDTO.class);
    }

    public void deleteFilmsRoles(UUID filmId, UUID id) {
        Role role = roleRepository.findByFilmAndId(repositoryHelper
                .getByIdRequired(Film.class, filmId), id);

        if (role  == null) {
            throw new EntityNotFoundException(Role.class, id);
        }
        roleRepository.delete(role);
    }

}
