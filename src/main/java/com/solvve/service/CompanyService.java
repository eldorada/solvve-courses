package com.solvve.service;

import com.solvve.domain.Film;
import com.solvve.domain.Company;
import com.solvve.dto.CompanyReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.RepositoryHelper;
import com.solvve.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Service
public class CompanyService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private TranslationService translationService;

    public List<CompanyReadDTO> getCompanies() {
        List<Company> companies = companyRepository.findAll();
        return translationService.translateList(companies, CompanyReadDTO.class);
    }

    public List<CompanyReadDTO> getFilmsCompanies(UUID filmId) {
        List<Company> companies = companyRepository.findByFilms(repositoryHelper.getByIdRequired(Film.class, filmId));
        return translationService.translateList(companies, CompanyReadDTO.class);
    }

    @Transactional
    public List<CompanyReadDTO> addCompanyToFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);
        Company company = repositoryHelper.getByIdRequired(Company.class, id);

        if (film.getCompanies().stream().anyMatch(cmpn -> cmpn.getId().equals(id))) {
            throw new LinkDuplicatedException(String.format("Film %s already has company %s", filmId, id));
        }

        film.getCompanies().add(company);
        film = filmRepository.save(film);

        return translationService.translateList(film.getCompanies(), CompanyReadDTO.class);
    }

    @Transactional
    public List<CompanyReadDTO> removeCompanyFromFilm(UUID filmId, UUID id) {
        Film film = repositoryHelper.getByIdRequired(Film.class, filmId);

        boolean removed = film.getCompanies().removeIf(cmpn -> cmpn.getId().equals(id));
        if (!removed) {
            throw new EntityNotFoundException("Film " + filmId + " has no company" + id);
        }

        film = filmRepository.save(film);

        return translationService.translateList(film.getCompanies(), CompanyReadDTO.class);
    }
}
