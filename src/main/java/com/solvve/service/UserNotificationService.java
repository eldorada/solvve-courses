package com.solvve.service;

import com.solvve.event.ReviewStatusChangedEvent;
import com.solvve.event.ViewStatusChangedEvent;
import com.solvve.repository.RepositoryHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Slf4j
@Service
public class UserNotificationService {

    @Autowired
    private RepositoryHelper repositoryHelper;

    public void notifyOnViewStatusChangedToOffline(UUID id) {
        log.info("View with ID {} now has status: {}", id,
                repositoryHelper.getByIdRequired(ViewStatusChangedEvent.class, id).getNewStatus());
    }

    public void notifyOnReviewStatusChangedToAccepted(UUID id) {
        log.info("Review with ID {} now has status: {}", id,
                repositoryHelper.getByIdRequired(ReviewStatusChangedEvent.class, id).getNewStatus());
    }
}
