package com.solvve.repository;

import com.solvve.domain.LoggedUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface LoggedUserRepository extends CrudRepository<LoggedUser, UUID> {

}
