package com.solvve.repository;

import com.solvve.domain.SignalToManager;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface SignalToManagerRepository extends CrudRepository<SignalToManager, UUID>,
        SignalToManagerRepositoryCustom {
    SignalToManager findByFixObjectIdAndId(UUID fixObjectId, UUID id);

}
