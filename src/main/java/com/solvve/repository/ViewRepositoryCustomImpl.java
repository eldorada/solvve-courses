package com.solvve.repository;

import com.solvve.domain.View;
import com.solvve.dto.ViewFilter;
import org.bitbucket.brunneng.qb.JpaQueryBuilder;
import org.bitbucket.brunneng.qb.SpringQueryBuilderUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public class ViewRepositoryCustomImpl implements ViewRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Page<View> findByFilter(ViewFilter filter, Pageable pageable) {
        JpaQueryBuilder qb = new JpaQueryBuilder(entityManager);
        qb.append("select v from View v where 1=1");
        qb.append(" and v.loggedUser.id = :v", filter.getLoggedUserId());
        qb.append(" and v.film.id = :v", filter.getFilmId());
        qb.append(" and v.status in :v", filter.getStatus());
        qb.append(" and v.startAt >= :v", filter.getStartAtFrom());
        qb.append(" and v.startAt < :v", filter.getStartAtTo());
        
        return SpringQueryBuilderUtils.loadPage(qb, pageable, "id");
    }
}
