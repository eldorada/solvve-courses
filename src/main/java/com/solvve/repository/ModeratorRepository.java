package com.solvve.repository;

import com.solvve.domain.Moderator;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ModeratorRepository extends CrudRepository<Moderator, UUID> {

}
