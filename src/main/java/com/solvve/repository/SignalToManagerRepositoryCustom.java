package com.solvve.repository;

import com.solvve.domain.SignalToManager;
import com.solvve.dto.SignalToManagerFilter;

import java.util.List;

public interface SignalToManagerRepositoryCustom {
    List<SignalToManager> findByFilter(SignalToManagerFilter filter);
}
