package com.solvve.repository;

import com.solvve.domain.ApplicationUser;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ApplicationUserRepository extends CrudRepository<ApplicationUser, UUID> {

    ApplicationUser findByEmail(String email);

    boolean existsByIdAndEmail(UUID id, String email);

}
