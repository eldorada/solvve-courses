package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.Genre;
import com.solvve.domain.GenreType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface GenreRepository extends CrudRepository<Genre, UUID> {

    @Query("select g from Genre g")
    List<Genre> findAll();

    List<Genre> findByFilms(Film films);

    @Query("select gnr.id from Genre gnr where gnr.type = :genreType")
    UUID findGenreIdByType(GenreType genreType);

}
