package com.solvve.repository;

import com.solvve.domain.SignalToManager;
import com.solvve.dto.SignalToManagerFilter;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

public class SignalToManagerRepositoryCustomImpl implements SignalToManagerRepositoryCustom {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<SignalToManager> findByFilter(SignalToManagerFilter filter) {
        StringBuilder sb = new StringBuilder();
        sb.append("select stm from SignalToManager stm where 1=1");
        if (filter.getLoggedUserId() != null) {
            sb.append(" and stm.loggedUser.id = :loggedUserId");

        }
        if (filter.getManagerId() != null) {
            sb.append(" and stm.manager.id = :managerId");

        }
        if (filter.getFixObjectId() != null) {
            sb.append(" and stm.fixObjectId = :fixObjectId");

        }
        if (filter.getStatus() != null) {
            sb.append(" and stm.status in (:status)");

        }
        if (filter.getType() != null) {
            sb.append(" and stm.type in (:type)");

        }
        if (filter.getRemarkText() != null) {
            sb.append(" and ((stm.remarkText LIKE :remarkTextBig) "
                    + "or (:remarkText LIKE CONCAT('%', stm.remarkText, '%')))"); //has a bug

        }

        TypedQuery<SignalToManager> query = entityManager.createQuery(sb.toString(), SignalToManager.class);

        if (filter.getLoggedUserId() != null) {
            query.setParameter("loggedUserId", filter.getLoggedUserId());
        }
        if (filter.getManagerId() != null) {
            query.setParameter("managerId", filter.getManagerId());
        }
        if (filter.getStatus() != null) {
            query.setParameter("status", filter.getStatus());
        }
        if (filter.getType() != null) {
            query.setParameter("type", filter.getType());
        }
        if (filter.getFixObjectId() != null) {
            query.setParameter("fixObjectId", filter.getFixObjectId());
        }
        if (filter.getRemarkText() != null) {
            query.setParameter("remarkTextBig", "%" + filter.getRemarkText() + "%");
            query.setParameter("remarkText", filter.getRemarkText());
        }

        return query.getResultList();
    }
}
