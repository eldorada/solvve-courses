package com.solvve.repository;

import com.solvve.domain.ExternalSystemImport;
import com.solvve.domain.ImportedEntityType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ExternalSystemImportRepository extends CrudRepository<ExternalSystemImport, UUID> {
    ExternalSystemImport findByIdInExternalSystemAndEntityType(String idInExternalSystem,
                                                               ImportedEntityType entityType);
}
