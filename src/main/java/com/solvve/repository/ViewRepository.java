package com.solvve.repository;

import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Repository
public interface ViewRepository extends CrudRepository<View, UUID>, ViewRepositoryCustom {
    List<View> findByFilmIdAndStatusOrderByStartAtAsc(UUID filmId, ViewStatus viewStatus);

    @Query("select v from View v Where v.film.id = :filmId and v.status = :viewStatus"
            + " and v.startAt >= :startFrom and v.startAt < :startTo order by v.startAt asc")
    List<View> findViewForFilmInGivenInterval(
            UUID filmId, ViewStatus viewStatus, Instant startFrom, Instant startTo);

    @Query("select avg(v.loggedUserMark) from View v where v.film.id = :filmId")
    Double calcAverageMarkOfFilm(UUID filmId);

    @Query("select v from View v Where v.loggedUser.id = :id")
    List<View> findViewForManager(UUID id);

}
