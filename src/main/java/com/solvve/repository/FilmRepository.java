package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.dto.FilmInLeaderBoardReadDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;
import java.util.stream.Stream;

@Repository
public interface FilmRepository extends CrudRepository<Film, UUID> {

    @Query("select f.id from Film f")
    Stream<UUID> getIdsOfFilms();

    @Query("select new com.solvve.dto.FilmInLeaderBoardReadDTO(f.id, f.title, f.averageMark, "
            + "(select count(v) from View v where v.film.id = f.id and v.loggedUserMark is not null)) "
            + "from Film f order by f.averageMark desc")
    List<FilmInLeaderBoardReadDTO> getFilmsLeaderBoard();

    Film findByTitle(String title);
}