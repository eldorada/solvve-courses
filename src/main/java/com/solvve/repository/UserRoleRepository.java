package com.solvve.repository;

import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRole;
import com.solvve.domain.UserRoleType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface UserRoleRepository extends CrudRepository<UserRole, UUID> {

    @Query("select ur from UserRole ur")
    List<UserRole> findAll();

    @Query("select ur.id from UserRole ur where ur.type = :userRoleType")
    UUID findUserRoleIdByType(UserRoleType userRoleType);

    List<UserRole> findByApplicationUsers(ApplicationUser applicationUsers);
}
