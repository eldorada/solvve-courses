package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.Country;
import com.solvve.domain.CountryType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CountryRepository extends CrudRepository<Country, UUID> {

    @Query("select g from Country g")
    List<Country> findAll();

    List<Country> findByFilms(Film films);

    @Query("select gnr.id from Country gnr where gnr.type = :countryType")
    UUID findCountryIdByType(CountryType countryType);

}
