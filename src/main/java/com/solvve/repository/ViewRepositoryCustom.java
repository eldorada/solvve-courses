package com.solvve.repository;

import com.solvve.domain.View;
import com.solvve.dto.ViewFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ViewRepositoryCustom {
    Page<View> findByFilter(ViewFilter filter, Pageable pageable);
}
