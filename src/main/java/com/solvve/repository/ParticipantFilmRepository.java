package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.ParticipantFilm;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ParticipantFilmRepository extends CrudRepository<ParticipantFilm, UUID> {
    List<ParticipantFilm> findByFilm(Film film);

    ParticipantFilm findByFilmAndId(Film film, UUID id);

}
