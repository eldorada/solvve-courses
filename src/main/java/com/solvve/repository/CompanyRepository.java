package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.Company;
import com.solvve.domain.CompanyType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface CompanyRepository extends CrudRepository<Company, UUID> {

    @Query("select g from Company g")
    List<Company> findAll();

    List<Company> findByFilms(Film films);

    @Query("select gnr.id from Company gnr where gnr.type = :countryType")
    UUID findCompanyIdByType(CompanyType countryType);

}
