package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.Review;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface ReviewRepository extends CrudRepository<Review, UUID> {
    List<Review> findByFilm(Film film);

    Review findByFilmAndId(Film film, UUID id);

}
