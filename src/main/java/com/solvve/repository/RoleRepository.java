package com.solvve.repository;

import com.solvve.domain.Film;
import com.solvve.domain.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID> {
    List<Role> findByFilm(Film film);

    Role findByFilmAndId(Film film, UUID id);

}
