package com.solvve.domain;

public enum ReviewStatus {
    ON_CHECK,
    ACCEPTED,
    CANCELED,
    CHANGED

}
