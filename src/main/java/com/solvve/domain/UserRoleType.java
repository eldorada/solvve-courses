package com.solvve.domain;

public enum UserRoleType {
    ADMIN,
    MODERATOR,
    MANAGER,
    LOGGED_USER
}
