package com.solvve.domain;

public enum CompanyType {
    PARAMOUNT_PICTURES,
    DISNEY,
    TWENTY_CENTURY_FOX,
    BRAZZERS,
    OTHER
}
