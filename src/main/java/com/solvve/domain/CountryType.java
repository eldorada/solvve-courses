package com.solvve.domain;

public enum CountryType {
    UKRAINE,
    RUSSIA,
    USA,
    INDIA,
    CHINA,
    ROK,
    CANADA,
    FRANCE,
    ENGLAND,
    OTHER
}
