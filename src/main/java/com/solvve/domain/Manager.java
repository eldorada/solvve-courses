package com.solvve.domain;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue("MANAGER")
public class Manager extends ApplicationUser {

}
