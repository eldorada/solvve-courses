package com.solvve.domain;

public enum ParticipantFilmType {
    FILMMAKER,
    PRODUCER,
    SCREENWRITER,
    ACTOR,
    DIRECTOR
}
