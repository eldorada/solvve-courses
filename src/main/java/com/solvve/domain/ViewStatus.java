package com.solvve.domain;

public enum ViewStatus {
    ONLINE,
    OFFLINE
}
