package com.solvve.domain;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Data
public class Person extends AbstractEntity {

    private String firstName;
    private String secondName;
    private String biography;
    private LocalDate dateOfBirth;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

    @OneToMany(mappedBy = "person")
    private List<ParticipantFilm> participantFilms = new ArrayList<>();

    @OneToOne(mappedBy = "person")
    private Actor actors;

}
