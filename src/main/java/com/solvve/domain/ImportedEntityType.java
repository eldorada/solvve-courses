package com.solvve.domain;

public enum ImportedEntityType {
    FILM,
    PERSON
}
