package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.Instant;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class View extends AbstractEntity {

    @NotNull
    @ManyToOne
    private ApplicationUser loggedUser;

    @NotNull
    @ManyToOne
    private Film film;

    @Positive
    private Integer loggedUserMark;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ViewStatus status;

    private Instant startAt;
    private Instant finishAt;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

}
