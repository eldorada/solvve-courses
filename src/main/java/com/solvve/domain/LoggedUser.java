package com.solvve.domain;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue("LOGGED_USER")
public class LoggedUser extends ApplicationUser {

}
