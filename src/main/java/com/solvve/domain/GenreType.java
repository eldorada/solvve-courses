package com.solvve.domain;

public enum GenreType {
    ACTION_MOVIE,
    DETECTIVE,
    DRAMA,
    HISTORICAL,
    COMEDY,
    MELODRAMA,
    ADVENTURE_MOVIE,
    STORY,
    TRAGEDY,
    THRILLER,
    SCIFI_MOVIE

}
