package com.solvve.domain;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.Instant;
import java.util.UUID;

@Entity
@EntityListeners(AuditingEntityListener.class)
@Getter
@Setter
public class SignalToManager {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne
    private LoggedUser loggedUser;

    @ManyToOne
    private Manager manager;

    private UUID fixObjectId;

    @Enumerated(EnumType.STRING)
    private SignalToManagerStatus status;

    @Enumerated(EnumType.STRING)
    private SignalToManagerType type;

    private String remarkText;
    private String correctionText;
    private Boolean correction;

    private Integer remarkTextStart;
    private Integer remarkTextEnd;

    @CreatedDate
    private Instant createdAt;

    @LastModifiedDate
    private Instant updatedAt;

}
