package com.solvve.domain;

public enum SignalToManagerStatus {
    FIXED,
    REJECTED,
    NEED_TO_FIX

}
