package com.solvve.domain;

import lombok.Data;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Data
@Entity
@DiscriminatorValue("MODERATOR")
public class Moderator extends ApplicationUser {

}
