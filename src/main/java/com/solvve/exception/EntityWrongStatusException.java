package com.solvve.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
public class EntityWrongStatusException extends RuntimeException {
    public EntityWrongStatusException(String message) {
        super(message);
    }

}
