package com.solvve.security;

import com.solvve.domain.ApplicationUser;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.UUID;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserDetailsImpl extends org.springframework.security.core.userdetails.User {

    private UUID id;

    public UserDetailsImpl(ApplicationUser applicationUser) {
        super(applicationUser.getEmail(), applicationUser.getEncodedPassword(),
                applicationUser.getUserRoles().stream().map(r -> new SimpleGrantedAuthority(r.getType().toString()))
                        .collect(Collectors.toList()));

        id = applicationUser.getId();
    }
}