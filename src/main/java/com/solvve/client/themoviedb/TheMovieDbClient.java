package com.solvve.client.themoviedb;

import com.solvve.client.themoviedb.dto.CrewReadDTO;
import com.solvve.client.themoviedb.dto.MovieCrewDTO;
import com.solvve.client.themoviedb.dto.MovieReadDTO;
import com.solvve.client.themoviedb.dto.MoviesPageDTO;
import com.solvve.client.themoviedb.dto.PersonReadDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "api.themoviedb.org", url = "${themoviedb.api.url}", configuration = TheMovieDbClientConfig.class)
public interface TheMovieDbClient {

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId, @RequestParam String language);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}")
    MovieReadDTO getMovie(@PathVariable("movieId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/{movieId}/credits")
    MovieCrewDTO getMovieCrew(@PathVariable("movieId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/credit/{creditId}")
    CrewReadDTO getCrew(@PathVariable("creditId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/person/{personId}/movie_credits")
    PersonReadDTO getPerson(@PathVariable("personId") String movieId);

    @RequestMapping(method = RequestMethod.GET, value = "/movie/top_rated")
    MoviesPageDTO getTopRatedMovies();

}
