package com.solvve.client.themoviedb.dto;

import lombok.Data;

import java.util.List;

@Data
public class MovieCrewDTO {
    private List<CrewReadShortDTO> results;
}
