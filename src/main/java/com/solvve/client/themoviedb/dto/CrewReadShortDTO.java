package com.solvve.client.themoviedb.dto;

import lombok.Data;

@Data
public class CrewReadShortDTO {
    private String crewId;

}
