package com.solvve.client.themoviedb.dto;

import lombok.Data;

import java.util.List;

@Data
public class CrewReadDTO {
    private String id;
    private List<String> cast;
    private List<String> crew;

}
