package com.solvve.client.themoviedb.dto;

import lombok.Data;

@Data
public class MovieReadDTO {

    private String id;

    private String originalTitle;

    private String title;
}
