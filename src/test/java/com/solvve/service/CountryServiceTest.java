package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Country;
import com.solvve.domain.CountryType;
import com.solvve.dto.CountryReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.CountryRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class CountryServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CountryRepository countryRepository;

    @Autowired
    private CountryService countryService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testAddCountryToFilm() {
        Film film = createFilm();
        UUID countryId = countryRepository.findCountryIdByType(CountryType.USA);

        List<CountryReadDTO> res = countryService.addCountryToFilm(film.getId(), countryId);

        CountryReadDTO expectedRead = new CountryReadDTO();
        expectedRead.setId(countryId);
        expectedRead.setType(CountryType.USA);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getCountries())
                    .extracting(Country::getId).containsExactlyInAnyOrder(countryId);
        });
    }

    @Test
    public void testDuplicatedCountry() {
        Film film = createFilm();
        UUID countryId = countryRepository.findCountryIdByType(CountryType.USA);

        countryService.addCountryToFilm(film.getId(), countryId);

        Assertions.assertThatThrownBy(() -> {
            countryService.addCountryToFilm(film.getId(), countryId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCountryId() {
        Film film = createFilm();
        UUID wrongCountryId = UUID.randomUUID();
        countryService.addCountryToFilm(film.getId(), wrongCountryId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {
        UUID wrongFilmId = UUID.randomUUID();
        UUID countryId = countryRepository.findCountryIdByType(CountryType.UKRAINE);
        countryService.addCountryToFilm(wrongFilmId, countryId);
    }

    @Test
    public void testRemoveCountryFromFilm() {
        Film film = createFilm();
        UUID countryId = countryRepository.findCountryIdByType(CountryType.USA);
        addCountryToFilm(film, countryId);

        List<CountryReadDTO> remainingCountries = countryService.removeCountryFromFilm(film.getId(), countryId);
        Assert.assertTrue(remainingCountries.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assert.assertTrue(filmAfterRemove.getCountries().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCountry() {
        Film film = createFilm();
        UUID countryId = countryRepository.findCountryIdByType(CountryType.USA);

        countryService.removeCountryFromFilm(film.getId(), countryId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedRole() {
        Film film = createFilm();

        countryService.removeCountryFromFilm(film.getId(), UUID.randomUUID());
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        return filmRepository.save(film);
    }

    private void addCountryToFilm(Film film, UUID countryId) {
        countryService.addCountryToFilm(film.getId(), countryId);
    }

}
