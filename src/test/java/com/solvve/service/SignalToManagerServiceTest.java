package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.*;
import com.solvve.repository.*;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

public class SignalToManagerServiceTest extends BaseTest {

    @Autowired
    private SignalToManagerRepository signalToManagerRepository;

    @Autowired
    private SignalToManagerService signalToManagerService;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @Test
    public void testGetSignalToManager() {
        Review review = createReview(createFilm());
        SignalToManager signalToManager = createSignalToManager(createLoggedUser(), createManager(), review);

        SignalToManagerReadDTO readDTO = signalToManagerService.getSignalToManager(review.getId(),
                signalToManager.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(signalToManager,
                "loggedUserId", "managerId");
    }

    @Test
    public void testCreateSignalToManager() {
        Film film = createFilm();
        Review review = createReview(film);
        LoggedUser loggedUser = createLoggedUser();
        Manager manager = createManager();
        SignalToManagerCreateDTO create = new SignalToManagerCreateDTO();
        create.setLoggedUserId(loggedUser.getId());
        create.setManagerId(manager.getId());
        create.setFixObjectId(review.getId());
        create.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        create.setType(SignalToManagerType.REVIEW);
        create.setCorrection(true);
        create.setCorrectionText("Vasy");
        create.setRemarkText("Vas");
        create.setRemarkTextStart(10);
        create.setRemarkTextEnd(15);

        SignalToManagerReadDTO read = signalToManagerService.createSignalToManager(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        SignalToManager signalToManager = signalToManagerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(signalToManager,
                "loggedUserId", "managerId");

    }

    @Test
    public void testFixSignalToManager() {
        Review review = createReview(createFilm());
        SignalToManager signalToManager = createSignalToManager(createLoggedUser(), createManager(), review);

        signalToManagerService.fixSignalToManager(review.getId(), signalToManager.getId());
        SignalToManager signalToManagerNew = signalToManagerRepository.findById(signalToManager.getId()).get();
        Assert.assertEquals(signalToManagerNew.getStatus(), SignalToManagerStatus.FIXED);
        Review reviewNew = reviewRepository.findById(review.getId()).get();
        Assertions.assertThat(reviewNew.getText()).isEqualTo("It's Nice, Vasy!");
    }

    @Test
    public void testFixSignalToManagerWithModeratedText() {
        Review review = createReview(createFilm());
        review.setText("It's Nice, Petre!");
        reviewRepository.save(review);
        SignalToManager signalToManager = createSignalToManager(createLoggedUser(), createManager(), review);

        signalToManagerService.fixSignalToManager(review.getId(), signalToManager.getId());
        SignalToManager signalToManagerNew = signalToManagerRepository.findById(signalToManager.getId()).get();
        Assert.assertEquals(signalToManagerNew.getStatus(), SignalToManagerStatus.REJECTED);

    }

    @Test
    public void testDeleteSignalToManagers() {
        Review review = createReview(createFilm());
        SignalToManager signalToManager = createSignalToManager(createLoggedUser(), createManager(), review);

        signalToManagerService.deleteSignalToManager(review.getId(), signalToManager.getId());
        Assert.assertFalse(signalToManagerRepository.existsById(signalToManager.getId()));
    }

    @Test
    public void testGetSignalsToManagerByRemarkText() {
        Review review = createReview(createFilm());
        SignalToManager s1 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas");
        SignalToManager s2 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas!");
        SignalToManager s3 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Petr");
        SignalToManager s4 = createSignalToManager(createLoggedUser(), createManager(),
                review, " Vas");

        SignalToManagerFilter filter = new SignalToManagerFilter();
        filter.setRemarkText("Vas");
        Assertions.assertThat(signalToManagerService.getSignalsToManager(filter)).extracting("id")
                .containsExactlyInAnyOrder(s1.getId(), s2.getId(), s4.getId());
    }

    @Test
    public void testGetSignalsToManagerByAll() {
        Review review = createReview(createFilm());
        SignalToManager s1 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas");
        createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas!");
        createSignalToManager(createLoggedUser(), createManager(),
                review, "Petr");
        createSignalToManager(createLoggedUser(), createManager(),
                review, " Vas");

        SignalToManagerFilter filter = new SignalToManagerFilter();
        filter.setRemarkText("Vas");
        filter.setLoggedUserId(s1.getLoggedUser().getId());
        filter.setManagerId(s1.getManager().getId());
        filter.setFixObjectId(s1.getFixObjectId());
        filter.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        filter.setType(SignalToManagerType.REVIEW);
        Assertions.assertThat(signalToManagerService.getSignalsToManager(filter)).extracting("id")
                .containsExactlyInAnyOrder(s1.getId());
    }

    @Test
    public void testFixAllSignalsToManager() {
        Review review = createReview(createFilm());
        SignalToManager s1 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas", 11, 14);
        SignalToManager s2 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Vas!", 11, 15);
        SignalToManager s3 = createSignalToManager(createLoggedUser(), createManager(),
                review, "Petr", 12, 14);
        SignalToManager s4 = createSignalToManager(createLoggedUser(), createManager(),
                review, " Vas", 10, 15);

        signalToManagerService.fixAllSignalsToManager(review.getId(), "Vas");

        s1 = signalToManagerRepository.findById(s1.getId()).get();
        s2 = signalToManagerRepository.findById(s2.getId()).get();
        s4 = signalToManagerRepository.findById(s4.getId()).get();

        Assert.assertEquals(SignalToManagerStatus.FIXED, s1.getStatus());
        Assert.assertEquals(SignalToManagerStatus.FIXED, s2.getStatus());
        Assert.assertEquals(SignalToManagerStatus.FIXED, s4.getStatus());

    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

    private Review createReview(Film film) {
        Review review = new Review();
        review.setText("It's Nice, Vas!");
        review.setDislike(1);
        review.setLike(134);
        review.setStatus(ReviewStatus.ON_CHECK);
        review.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        review.setFilm(film);
        return reviewRepository.save(review);
    }

    private LoggedUser createLoggedUser() {
        LoggedUser user = new LoggedUser();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return loggedUserRepository.save(user);
    }

    private Manager createManager() {
        Manager user = new Manager();
        user.setName("Pyt");
        user.setLogin("Petya 1963");
        user.setRating(10.0);
        return managerRepository.save(user);
    }

    private SignalToManager createSignalToManager(LoggedUser loggedUser,
                                                  Manager manager, Review review) {
        SignalToManager signalToManager = new SignalToManager();
        signalToManager.setLoggedUser(loggedUser);
        signalToManager.setManager(manager);
        signalToManager.setFixObjectId(review.getId());
        signalToManager.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        signalToManager.setType(SignalToManagerType.REVIEW);
        signalToManager.setCorrection(true);
        signalToManager.setCorrectionText("Vasy");
        signalToManager.setRemarkText("Vas");
        signalToManager.setRemarkTextStart(11);
        signalToManager.setRemarkTextEnd(14);
        return signalToManagerRepository.save(signalToManager);
    }

    private SignalToManager createSignalToManager(LoggedUser loggedUser, Manager manager,
                                                  Review review, String remarkText) {
        SignalToManager signalToManager = new SignalToManager();
        signalToManager.setLoggedUser(loggedUser);
        signalToManager.setManager(manager);
        signalToManager.setFixObjectId(review.getId());
        signalToManager.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        signalToManager.setType(SignalToManagerType.REVIEW);
        signalToManager.setCorrection(true);
        signalToManager.setCorrectionText("Vasy");
        signalToManager.setRemarkText(remarkText);
        signalToManager.setRemarkTextStart(11);
        signalToManager.setRemarkTextEnd(14);
        return signalToManagerRepository.save(signalToManager);
    }

    private SignalToManager createSignalToManager(LoggedUser loggedUser, Manager manager,
                                                  Review review, String remarkText, Integer Start, Integer End) {
        SignalToManager signalToManager = new SignalToManager();
        signalToManager.setLoggedUser(loggedUser);
        signalToManager.setManager(manager);
        signalToManager.setFixObjectId(review.getId());
        signalToManager.setStatus(SignalToManagerStatus.NEED_TO_FIX);
        signalToManager.setType(SignalToManagerType.REVIEW);
        signalToManager.setCorrection(true);
        signalToManager.setCorrectionText("Vasy");
        signalToManager.setRemarkText(remarkText);
        signalToManager.setRemarkTextStart(Start);
        signalToManager.setRemarkTextEnd(End);
        return signalToManagerRepository.save(signalToManager);
    }
}

