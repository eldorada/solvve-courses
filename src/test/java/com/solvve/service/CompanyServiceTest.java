package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Company;
import com.solvve.domain.CompanyType;
import com.solvve.dto.CompanyReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.CompanyRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class CompanyServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private CompanyRepository companyRepository;

    @Autowired
    private CompanyService companyService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testAddCompanyToFilm() {
        Film film = createFilm();
        UUID companyId = companyRepository.findCompanyIdByType(CompanyType.DISNEY);

        List<CompanyReadDTO> res = companyService.addCompanyToFilm(film.getId(), companyId);

        CompanyReadDTO expectedRead = new CompanyReadDTO();
        expectedRead.setId(companyId);
        expectedRead.setType(CompanyType.DISNEY);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getCompanies())
                    .extracting(Company::getId).containsExactlyInAnyOrder(companyId);
        });
    }

    @Test
    public void testDuplicatedCompany() {
        Film film = createFilm();
        UUID companyId = companyRepository.findCompanyIdByType(CompanyType.DISNEY);

        companyService.addCompanyToFilm(film.getId(), companyId);

        Assertions.assertThatThrownBy(() -> {
            companyService.addCompanyToFilm(film.getId(), companyId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongCompanyId() {
        Film film = createFilm();
        UUID wrongCompanyId = UUID.randomUUID();
        companyService.addCompanyToFilm(film.getId(), wrongCompanyId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {
        UUID wrongFilmId = UUID.randomUUID();
        UUID companyId = companyRepository.findCompanyIdByType(CompanyType.OTHER);
        companyService.addCompanyToFilm(wrongFilmId, companyId);
    }

    @Test
    public void testRemoveCompanyFromFilm() {
        Film film = createFilm();
        UUID companyId = companyRepository.findCompanyIdByType(CompanyType.DISNEY);
        addCompanyToFilm(film, companyId);

        List<CompanyReadDTO> remainingCompanies = companyService.removeCompanyFromFilm(film.getId(), companyId);
        Assert.assertTrue(remainingCompanies.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assert.assertTrue(filmAfterRemove.getCompanies().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedCompany() {
        Film film = createFilm();
        UUID companyId = companyRepository.findCompanyIdByType(CompanyType.DISNEY);
        companyService.removeCompanyFromFilm(film.getId(), companyId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedRole() {
        Film film = createFilm();
        companyService.removeCompanyFromFilm(film.getId(), UUID.randomUUID());
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        return filmRepository.save(film);
    }

    private void addCompanyToFilm(Film film, UUID companyId) {
        companyService.addCompanyToFilm(film.getId(), companyId);
    }

}
