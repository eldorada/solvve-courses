package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Admin;
import com.solvve.dto.AdminCreateDTO;
import com.solvve.dto.AdminPatchDTO;
import com.solvve.dto.AdminPutDTO;
import com.solvve.dto.AdminReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.AdminRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class AdminServiceTest extends BaseTest {

    @Autowired
    private AdminRepository adminRepository;

    @Autowired
    private AdminService adminService;

    @Test
    public void testGetUser() {
        Admin user = createUser();

        AdminReadDTO readDTO = adminService.getAdmin(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        adminService.getAdmin(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        AdminCreateDTO create = new AdminCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);
        AdminReadDTO read = adminService.createAdmin(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Admin user = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        Admin user = createUser();

        AdminPatchDTO patch = new AdminPatchDTO();
        patch.setName("Petya");
        patch.setLogin("Petya1991");
        patch.setRating(1.2);
        AdminReadDTO read = adminService.patchAdmin(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateUser() {
        Admin user = createUser();

        AdminPutDTO put = new AdminPutDTO();
        put.setName("Petya");
        put.setLogin("Petya1991");
        put.setRating(1.2);
        AdminReadDTO read = adminService.updateAdmin(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = adminRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        Admin user = createUser();

        AdminPatchDTO patch = new AdminPatchDTO();
        AdminReadDTO read = adminService.patchAdmin(user.getId(), patch);

        Assert.assertNotNull(read.getLogin());
        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getRating());

        Admin userAfterUpdate = adminRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getLogin());
        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getRating());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(userAfterUpdate,
                "userRoles");
    }

    @Test
    public void testDeleteUser() {
        Admin user = createUser();

        adminService.deleteAdmin(user.getId());
        Assert.assertFalse(adminRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        adminService.deleteAdmin(UUID.randomUUID());
    }

    private Admin createUser() {
        Admin user = new Admin();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return adminRepository.save(user);
    }

}
