package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Moderator;
import com.solvve.dto.ModeratorCreateDTO;
import com.solvve.dto.ModeratorPatchDTO;
import com.solvve.dto.ModeratorPutDTO;
import com.solvve.dto.ModeratorReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ModeratorRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ModeratorServiceTest extends BaseTest {

    @Autowired
    private ModeratorRepository moderatorRepository;

    @Autowired
    private ModeratorService moderatorService;

    @Test
    public void testGetUser() {
        Moderator user = createUser();

        ModeratorReadDTO readDTO = moderatorService.getModerator(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        moderatorService.getModerator(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        ModeratorCreateDTO create = new ModeratorCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);
        ModeratorReadDTO read = moderatorService.createModerator(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Moderator user = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        Moderator user = createUser();

        ModeratorPatchDTO patch = new ModeratorPatchDTO();
        patch.setName("Petya");
        patch.setLogin("Petya1991");
        patch.setRating(1.2);
        ModeratorReadDTO read = moderatorService.patchModerator(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateUser() {
        Moderator user = createUser();

        ModeratorPutDTO put = new ModeratorPutDTO();
        put.setName("Petya");
        put.setLogin("Petya1991");
        put.setRating(1.2);
        ModeratorReadDTO read = moderatorService.updateModerator(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = moderatorRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        Moderator user = createUser();

        ModeratorPatchDTO patch = new ModeratorPatchDTO();
        ModeratorReadDTO read = moderatorService.patchModerator(user.getId(), patch);

        Assert.assertNotNull(read.getLogin());
        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getRating());

        Moderator userAfterUpdate = moderatorRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getLogin());
        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getRating());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(userAfterUpdate,
                "userRoles");
    }

    @Test
    public void testDeleteUser() {
        Moderator user = createUser();

        moderatorService.deleteModerator(user.getId());
        Assert.assertFalse(moderatorRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        moderatorService.deleteModerator(UUID.randomUUID());
    }

    private Moderator createUser() {
        Moderator user = new Moderator();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return moderatorRepository.save(user);
    }

}
