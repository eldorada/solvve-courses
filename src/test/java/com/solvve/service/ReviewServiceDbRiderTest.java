package com.solvve.service;

import com.github.database.rider.core.api.dataset.DataSet;
import com.github.database.rider.core.api.dataset.ExpectedDataSet;
import com.github.database.rider.spring.api.DBRider;
import com.solvve.base.BaseTest;
import com.solvve.domain.Review;
import com.solvve.dto.ReviewReadDTO;
import org.assertj.core.api.Assertions;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.UUID;

@DBRider
public class ReviewServiceDbRiderTest extends BaseTest {

    @Autowired
    private ReviewService reviewService;

    @Test
    @DataSet(value = "/datasets/testUpdateReview.xml")
    public void testGetFilmsReview() {
        UUID f1 = UUID.fromString("fd5a01f0-f296-4781-bbe7-f423da1c9c77");
        UUID r1 = UUID.fromString("7b6a679f-d6e2-47eb-8178-e47ef40cfc6e");
        UUID r2 = UUID.fromString("19957d34-36a9-4833-8f63-51a5a1678e4f");

        Assertions.assertThat(reviewService.getFilmsReviews(f1)).extracting("id")
                .containsExactlyInAnyOrder(r1, r2);
    }

    @Ignore
    @Test
    @DataSet(value = "/datasets/testUpdateReview.xml")
    @ExpectedDataSet(value = "/datasets/testUpdateReview_result.xml")
    public void testGetFilmsReviewWithResult() {
        UUID f1 = UUID.fromString("fd5a01f0-f296-4781-bbe7-f423da1c9c77");

       List<ReviewReadDTO> r = reviewService.getFilmsReviews(f1);

    }

}

