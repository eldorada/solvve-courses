package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Genre;
import com.solvve.domain.GenreType;
import com.solvve.dto.GenreReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.GenreRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

public class GenreServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private GenreRepository genreRepository;

    @Autowired
    private GenreService genreService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testAddGenreToFilm() {
        Film film = createFilm();
        UUID genreId = genreRepository.findGenreIdByType(GenreType.DRAMA);

        List<GenreReadDTO> res = genreService.addGenreToFilm(film.getId(), genreId);

        GenreReadDTO expectedRead = new GenreReadDTO();
        expectedRead.setId(genreId);
        expectedRead.setType(GenreType.DRAMA);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterSave = filmRepository.findById(film.getId()).get();
            Assertions.assertThat(filmAfterSave.getGenres())
                    .extracting(Genre::getId).containsExactlyInAnyOrder(genreId);
        });
    }

    @Test
    public void testDuplicatedGenre() {
        Film film = createFilm();
        UUID genreId = genreRepository.findGenreIdByType(GenreType.DRAMA);

        genreService.addGenreToFilm(film.getId(), genreId);

        Assertions.assertThatThrownBy(() -> {
            genreService.addGenreToFilm(film.getId(), genreId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongGenreId() {
        Film film = createFilm();
        UUID wrongGenreId = UUID.randomUUID();
        genreService.addGenreToFilm(film.getId(), wrongGenreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongFilmId() {
        UUID wrongFilmId = UUID.randomUUID();
        UUID genreId = genreRepository.findGenreIdByType(GenreType.COMEDY);
        genreService.addGenreToFilm(wrongFilmId, genreId);
    }

    @Test
    public void testRemoveGenreFromFilm() {
        Film film = createFilm();
        UUID genreId = genreRepository.findGenreIdByType(GenreType.DRAMA);
        addGenreToFilm(film, genreId);

        List<GenreReadDTO> remainingGenres = genreService.removeGenreFromFilm(film.getId(), genreId);
        Assert.assertTrue(remainingGenres.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            Film filmAfterRemove = filmRepository.findById(film.getId()).get();
            Assert.assertTrue(filmAfterRemove.getGenres().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedGenre() {
        Film film = createFilm();
        UUID genreId = genreRepository.findGenreIdByType(GenreType.DRAMA);

        genreService.removeGenreFromFilm(film.getId(), genreId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedRole() {
        Film film = createFilm();

        genreService.removeGenreFromFilm(film.getId(), UUID.randomUUID());
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

    private void addGenreToFilm(Film film, UUID genreId) {
        genreService.addGenreToFilm(film.getId(), genreId);
    }

}
