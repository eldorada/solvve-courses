package com.solvve.service.importer;

import com.solvve.base.BaseTest;
import com.solvve.domain.*;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.repository.ExternalSystemImportRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ExternalSystemImportServiceTest extends BaseTest {

    @Autowired
    private ExternalSystemImportService externalSystemImportService;

    @Autowired
    private ExternalSystemImportRepository externalSystemImportRepository;

    @Test
    public void testValidateNotImportedFromExternalSystem() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Film.class, "some-id");
    }

    @Test
    public void testValidateNotImportedFromExternalSystemForPerson() throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(Person.class, "some-id");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValidateFromExternalSystemForOtherEntity()
            throws ImportAlreadyPerformedException {
        externalSystemImportService.validateNotImported(ApplicationUser.class, "some-id");
    }

    @Test
    public void testExceptionWhenAlreadyImported() {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.FILM);
        esi = externalSystemImportRepository.save(esi);
        String idExternalSystem = esi.getIdInExternalSystem();

        ImportAlreadyPerformedException ex = Assertions.catchThrowableOfType(
                () -> externalSystemImportService.validateNotImported(Film.class, idExternalSystem),
                ImportAlreadyPerformedException.class);
        Assertions.assertThat(ex.getExternalSystemImport()).isEqualToComparingFieldByField(esi);
    }

    @Test
    public void testExceptionWhenAlreadyImportedButDifferentEntityType() throws ImportAlreadyPerformedException {
        ExternalSystemImport esi = generateFlatEntityWithoutId(ExternalSystemImport.class);
        esi.setEntityType(ImportedEntityType.PERSON);
        esi = externalSystemImportRepository.save(esi);

        externalSystemImportService.validateNotImported(Film.class, esi.getIdInExternalSystem());
    }

    @Test
    public void testCreateExternalSystemImport() {
        Film film = generateFlatObject(Film.class);
        String idInExternalSystem = "id1";
        UUID importId = externalSystemImportService.createExternalSystemImport(film, idInExternalSystem);
        Assert.assertNotNull(importId);
        ExternalSystemImport esi = externalSystemImportRepository.findById(importId).get();
        Assert.assertEquals(idInExternalSystem, esi.getIdInExternalSystem());
        Assert.assertEquals(ImportedEntityType.FILM, esi.getEntityType());
        Assert.assertEquals(film.getId(), esi.getEntityId());

    }
}

