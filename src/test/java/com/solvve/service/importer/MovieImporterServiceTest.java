package com.solvve.service.importer;

import com.solvve.base.BaseTest;
import com.solvve.client.themoviedb.TheMovieDbClient;
import com.solvve.client.themoviedb.dto.MovieReadDTO;
import com.solvve.domain.Film;
import com.solvve.exception.ImportAlreadyPerformedException;
import com.solvve.exception.ImportedEntityAlreadyExistException;
import com.solvve.repository.FilmRepository;
import org.assertj.core.api.Assertions;
import org.dbunit.Assertion;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.UUID;

public class MovieImporterServiceTest extends BaseTest {

    @MockBean
    private TheMovieDbClient movieDbClient;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private MovieImporterService movieImporterService;

    @Test
    public void setMovieImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        UUID filmId = movieImporterService.importFIlm(movieExternalId);
        Film film = filmRepository.findById(filmId).get();

        Assert.assertEquals(read.getTitle(), film.getTitle());
        Assert.assertEquals(read.getOriginalTitle(), film.getOriginalTitle());
    }

    @Test
    public void testNoCallToClientOnDuplicateImport()
            throws ImportedEntityAlreadyExistException, ImportAlreadyPerformedException {

        String movieExternalId = "id1";

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        movieImporterService.importFIlm(movieExternalId);
        Mockito.verify(movieDbClient).getMovie(movieExternalId);
        Mockito.reset(movieDbClient);

        Assertions.assertThatThrownBy(() -> movieImporterService.importFIlm(movieExternalId))
                .isInstanceOf(ImportAlreadyPerformedException.class);

        Mockito.verifyNoInteractions(movieDbClient);
    }

    @Test
    public void setMovieImportAlreadyExists() {
        String movieExternalId = "id1";

        Film existingFilm = generateFlatEntityWithoutId(Film.class);
        existingFilm = filmRepository.save(existingFilm);

        MovieReadDTO read = generateObject(MovieReadDTO.class);
        read.setTitle(existingFilm.getTitle());
        read.setOriginalTitle(existingFilm.getOriginalTitle());
        Mockito.when(movieDbClient.getMovie(movieExternalId)).thenReturn(read);

        ImportedEntityAlreadyExistException ex = Assertions.catchThrowableOfType(
                () -> movieImporterService.importFIlm(movieExternalId), ImportedEntityAlreadyExistException.class);
        Assert.assertEquals(Film.class, ex.getEntityClass());
        Assert.assertEquals(existingFilm.getId(), ex.getEntityId());
    }
}
