package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Film;
import com.solvve.domain.Person;
import com.solvve.domain.Role;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.PersonRepository;
import com.solvve.repository.RoleRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class RoleServiceTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private RoleService roleService;

    @Test
    public void testGetFilmsRole() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f1 = createFilm();
        Film f2 = createFilm();
        Role r1 = createRole(f1, a);
        Role r2 = createRole(f1, a);
        createRole(f2, a);

        Assertions.assertThat(roleService.getFilmsRoles(f1.getId()))
                .extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId());
    }

    @Test
    public void testGetFilmsRoleById() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f1 = createFilm();
        Film f2 = createFilm();
        Role r1 = createRole(f1, a);
        createRole(f1, a);
        createRole(f2, a);

        RoleReadDTO readDTO = roleService.getFilmsRoles(f1.getId(), r1.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(r1, "filmId", "actorId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetRoleWrongId() {
        Film f1 = createFilm();
        roleService.getFilmsRoles(f1.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateFilmsRoles() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film film = createFilm();
        RoleCreateDTO create = new RoleCreateDTO();
        create.setDescription("It's Bad!");
        create.setNameRole("policeman");
        create.setActorId(a.getId());

        RoleReadDTO read = roleService.createFilmsRoles(film.getId(), create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read, "filmId");
        Assert.assertNotNull(read.getId());

        Role role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(role, "filmId", "actorId");

    }

    @Test
    public void testPatchFilmsRoles() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film film = createFilm();
        Role role = createRole(film, a);

        RolePatchDTO patch = new RolePatchDTO();
        patch.setDescription("It's Bad!");
        patch.setNameRole("policeman");
        patch.setFilmId(film.getId());
        patch.setActorId(a.getId());

        RoleReadDTO read = roleService.patchFilmsRoles(film.getId(),
                role.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(role, "filmId", "actorId");
    }

    @Test
    public void testPutFilmsRoles() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film film = createFilm();
        Role role = createRole(film, a);

        RolePutDTO put = new RolePutDTO();
        put.setDescription("It's Bad!");
        put.setNameRole("policeman");
        put.setActorId(a.getId());
        put.setFilmId(film.getId());
        RoleReadDTO read = roleService.updateFilmsRoles(film.getId(),
                role.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        role = roleRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(role, "filmId", "actorId");
    }

    @Test
    public void testPatchRoleEmptyPatch() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film film = createFilm();
        Role role = createRole(film, a);

        RolePatchDTO patch = new RolePatchDTO();

        RoleReadDTO read = roleService.patchFilmsRoles(film.getId(), role.getId(), patch);

        Assert.assertNotNull(read.getDescription());
        Assert.assertNotNull(read.getNameRole());

        Role roleAfterUpdate = roleRepository.findById(read.getId()).get();

        Assert.assertNotNull(roleAfterUpdate.getDescription());
        Assert.assertNotNull(roleAfterUpdate.getNameRole());
        Assert.assertNotNull(roleAfterUpdate.getFilm());

        Assertions.assertThat(role).isEqualToIgnoringGivenFields(roleAfterUpdate,
                "film", "actor");
    }

    @Test
    public void testDeleteFilmsRoles() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film film = createFilm();
        Role role = createRole(film, a);

        roleService.deleteFilmsRoles(film.getId(), role.getId());
        Assert.assertFalse(roleRepository.existsById(role.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteRoleNotFound() {
        Film film = createFilm();
        roleService.deleteFilmsRoles(film.getId(),UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteFilmNotFound() {
        roleService.deleteFilmsRoles(UUID.randomUUID(),UUID.randomUUID());
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);

        return filmRepository.save(film);
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);
    }
    private Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actorRepository.save(actor);
    }

    private Role createRole(Film film, Actor actor) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setActor(actor);
        role.setFilm(film);
        return roleRepository.save(role);
    }


}

