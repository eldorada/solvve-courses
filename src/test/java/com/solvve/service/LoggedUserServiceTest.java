package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.LoggedUser;
import com.solvve.dto.LoggedUserCreateDTO;
import com.solvve.dto.LoggedUserPatchDTO;
import com.solvve.dto.LoggedUserPutDTO;
import com.solvve.dto.LoggedUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.LoggedUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class LoggedUserServiceTest extends BaseTest {

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private LoggedUserService loggedUserService;

    @Test
    public void testGetUser() {
        LoggedUser user = createUser();

        LoggedUserReadDTO readDTO = loggedUserService.getLoggedUser(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        loggedUserService.getLoggedUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        LoggedUserCreateDTO create = new LoggedUserCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);
        LoggedUserReadDTO read = loggedUserService.createLoggedUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        LoggedUser user = loggedUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        LoggedUser user = createUser();

        LoggedUserPatchDTO patch = new LoggedUserPatchDTO();
        patch.setName("Petya");
        patch.setLogin("Petya1991");
        patch.setRating(1.2);
        LoggedUserReadDTO read = loggedUserService.patchLoggedUser(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = loggedUserRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateUser() {
        LoggedUser user = createUser();

        LoggedUserPutDTO put = new LoggedUserPutDTO();
        put.setName("Petya");
        put.setLogin("Petya1991");
        put.setRating(1.2);
        LoggedUserReadDTO read = loggedUserService.updateLoggedUser(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = loggedUserRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        LoggedUser user = createUser();

        LoggedUserPatchDTO patch = new LoggedUserPatchDTO();
        LoggedUserReadDTO read = loggedUserService.patchLoggedUser(user.getId(), patch);

        Assert.assertNotNull(read.getLogin());
        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getRating());

        LoggedUser userAfterUpdate = loggedUserRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getLogin());
        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getRating());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles");
    }

    @Test
    public void testDeleteUser() {
        LoggedUser user = createUser();

        loggedUserService.deleteLoggedUser(user.getId());
        Assert.assertFalse(loggedUserRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        loggedUserService.deleteLoggedUser(UUID.randomUUID());
    }

    private LoggedUser createUser() {
        LoggedUser user = new LoggedUser();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return loggedUserRepository.save(user);
    }

}

