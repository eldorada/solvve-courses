package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.dto.ApplicationUserCreateDTO;
import com.solvve.dto.ApplicationUserPatchDTO;
import com.solvve.dto.ApplicationUserPutDTO;
import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ApplicationUserRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ApplicationUserServiceTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private ApplicationUserService applicationUserService;

    @Test
    public void testGetUser() {
        ApplicationUser user = createUser();

        ApplicationUserReadDTO readDTO = applicationUserService.getApplicationUser(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        applicationUserService.getApplicationUser(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        ApplicationUserCreateDTO create = new ApplicationUserCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);
        ApplicationUserReadDTO read = applicationUserService.createApplicationUser(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        ApplicationUser user = applicationUserRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        ApplicationUser user = createUser();

        ApplicationUserPatchDTO patch = new ApplicationUserPatchDTO();
        patch.setName("Petya");
        patch.setLogin("Petya1991");
        patch.setRating(1.2);
        patch.setEmail("abv@gmail.com");
        patch.setEncodedPassword("123");
        ApplicationUserReadDTO read = applicationUserService.patchApplicationUser(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = applicationUserRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateUser() {
        ApplicationUser user = createUser();

        ApplicationUserPutDTO put = new ApplicationUserPutDTO();
        put.setName("Petya");
        put.setLogin("Petya1991");
        put.setRating(1.2);
        put.setEmail("Petya1991@gamail.com");
        put.setEncodedPassword("132");

        ApplicationUserReadDTO read = applicationUserService.updateApplicationUser(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = applicationUserRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles","createdAt", "updatedAt");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        ApplicationUser user = createUser();

        ApplicationUserPatchDTO patch = new ApplicationUserPatchDTO();
        ApplicationUserReadDTO read = applicationUserService.patchApplicationUser(user.getId(), patch);

        Assert.assertNotNull(read.getLogin());
        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getRating());

        ApplicationUser userAfterUpdate = applicationUserRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getLogin());
        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getRating());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(userAfterUpdate,
                "userRoles");
    }

    @Test
    public void testDeleteUser() {
        ApplicationUser user = createUser();

        applicationUserService.deleteApplicationUser(user.getId());
        Assert.assertFalse(applicationUserRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        applicationUserService.deleteApplicationUser(UUID.randomUUID());
    }

    private ApplicationUser createUser() {
        ApplicationUser user = generateFlatEntityWithoutId(ApplicationUser.class);
        return applicationUserRepository.save(user);
    }

}

