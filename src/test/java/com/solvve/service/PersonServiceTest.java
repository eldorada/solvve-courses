package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.PersonCreateDTO;
import com.solvve.dto.PersonPatchDTO;
import com.solvve.dto.PersonPutDTO;
import com.solvve.dto.PersonReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.PersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.UUID;

public class PersonServiceTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PersonService personService;

    @Test
    public void testGetPerson() {
        Person person = createPerson();

        PersonReadDTO readDTO = personService.getPerson(person.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(person);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetPersonWrongId() {
        personService.getPerson(UUID.randomUUID());
    }

    @Test
    public void testCreatePerson() {

        PersonCreateDTO create = new PersonCreateDTO();
        create.setFirstName("Lola");
        create.setSecondName("Ivanova");
        create.setDateOfBirth(LocalDate.parse("2019-12-31"));
        create.setBiography("Small");

        PersonReadDTO read = personService.createPerson(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Person person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(person);
    }

    @Test
    public void testPatchPerson() {
        Person person = createPerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        patch.setFirstName("Lola");
        patch.setSecondName("Ivanova");
        patch.setDateOfBirth(LocalDate.parse("2019-12-31"));
        patch.setBiography("Small");
        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(person).isEqualToIgnoringGivenFields(read,
                "participantFilms", "actors");
    }

    @Test
    public void testPutPerson() {
        Person person = createPerson();

        PersonPutDTO put = new PersonPutDTO();
        put.setFirstName("Lola");
        put.setSecondName("Ivanova");
        put.setDateOfBirth(LocalDate.parse("2019-12-31"));
        put.setBiography("Small");

        PersonReadDTO read = personService.updatePerson(person.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        person = personRepository.findById(read.getId()).get();
        Assertions.assertThat(person).isEqualToIgnoringGivenFields(read,
                "participantFilms", "actors");
    }

    @Test
    public void testPatchPersonEmptyPatch() {
        Person person = createPerson();

        PersonPatchDTO patch = new PersonPatchDTO();
        PersonReadDTO read = personService.patchPerson(person.getId(), patch);

        Assert.assertNotNull(read.getFirstName());
        Assert.assertNotNull(read.getSecondName());
        Assert.assertNotNull(read.getDateOfBirth());
        Assert.assertNotNull(read.getBiography());

        Person personAfterUpdate = personRepository.findById(read.getId()).get();

        Assert.assertNotNull(personAfterUpdate.getFirstName());
        Assert.assertNotNull(personAfterUpdate.getSecondName());
        Assert.assertNotNull(personAfterUpdate.getDateOfBirth());
        Assert.assertNotNull(personAfterUpdate.getBiography());

        Assertions.assertThat(person).isEqualToIgnoringGivenFields(personAfterUpdate,
                "participantFilms", "actors");
    }

    @Test
    public void testDeletePerson() {
        Person person = createPerson();

        personService.deletePerson(person.getId());
        Assert.assertFalse(personRepository.existsById(person.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeletePersonNotFound() {
        personService.deletePerson(UUID.randomUUID());
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);
    }

}

