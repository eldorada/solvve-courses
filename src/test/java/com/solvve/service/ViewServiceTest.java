package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.*;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ManagerRepository;
import com.solvve.repository.ViewRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.UUID;

public class ViewServiceTest extends BaseTest {

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ViewService viewService;

    @Autowired
    private ManagerService managerService;

    @Test
    public void testGetViewExtended() {
        ApplicationUser loggedUser = createUser();
        Film film = createFilm();
        View view = createView(loggedUser, film);

        ViewReadExtendedDTO read = viewService.getView(view.getId());
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(view,
                "loggedUser", "film");
        Assertions.assertThat(read.getLoggedUser()).isEqualToIgnoringGivenFields(loggedUser);
        Assertions.assertThat(read.getFilm()).isEqualToIgnoringGivenFields(film);
    }

    @Test
    public void testGetViewsWithEmptyFilter() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1);
        View v2 = createView(u1, f2);
        View v3 = createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId(), v3.getId());
    }

    @Test
    public void testGetViewsByApplicationUser() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1);
        View v2 = createView(u1, f2);
        createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        filter.setLoggedUserId(u1.getId());
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByFilm() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1);
        View v2 = createView(u1, f2);
        View v1 = createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        filter.setFilmId(f2.getId());
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetManagerViews() {
        Manager m1 = createManager();
        Manager m2 = createManager();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(m1, f1);
        View v2 = createView(m1, f2);
        createView(m2, f2);

        Assertions.assertThat(managerService.getManagerViews(m1.getId()))
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByStatus() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        View v1 = createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        View v2 = createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setStatus(ViewStatus.ONLINE);
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByStartAtInterval() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        View v2 = createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setStartAtFrom(createInstant(9));
        filter.setStartAtTo(createInstant(13));

        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByAllFilters() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setLoggedUserId(u1.getId());
        filter.setFilmId(f1.getId());
        filter.setStatus(ViewStatus.OFFLINE);
        filter.setStartAtFrom(createInstant(9));
        filter.setStartAtTo(createInstant(13));

        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData())
                .extracting("id").containsExactlyInAnyOrder(v1.getId());
    }

    @Test
    public void testCreateView() {
        ApplicationUser u = createUser();
        Film f = createFilm();
        ViewCreateDTO create = new ViewCreateDTO();
        create.setStatus(ViewStatus.ONLINE);
        create.setStartAt(createInstant(4));
        create.setFinishAt(createInstant(5));
        create.setLoggedUserId(u.getId());
        create.setFilmId(f.getId());

        ViewReadDTO read = viewService.createView(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());
        Assert.assertEquals(ViewStatus.ONLINE, read.getStatus());

        View savedView = viewRepository.findById(read.getId()).get();
        Assertions.assertThat(savedView).isEqualToIgnoringGivenFields(read,
                "loggedUser", "film");

    }

    @Test(expected = EntityNotFoundException.class)
    public void testCreateViewWithWrongFilm() {
        ApplicationUser u = createUser();

        ViewCreateDTO create = new ViewCreateDTO();
        create.setStatus(ViewStatus.ONLINE);
        create.setStartAt(createInstant(4));
        create.setFinishAt(createInstant(5));
        create.setLoggedUserId(u.getId());
        create.setFilmId(UUID.randomUUID());

        viewService.createView(create);

    }

    @Test
    public void testGetViewWithEmptyFilterWithPagingAndSorting() {
        ApplicationUser u1 = createUser();
        ApplicationUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        View v2 = createView(u1, f2, ViewStatus.OFFLINE, createInstant(13));
        createView(u2, f2, ViewStatus.OFFLINE, createInstant(14));
        createView(u2, f2, ViewStatus.OFFLINE, createInstant(15));

        ViewFilter filter = new ViewFilter();
        PageRequest pageRequest = PageRequest.of(1, 2, Sort.by(Sort.Direction.DESC, "startAt"));
        Assertions.assertThat(viewService.getViews(filter, pageRequest).getData()).extracting("id")
                .isEqualTo(Arrays.asList(v2.getId(), v1.getId()));

    }

    private View createView(ApplicationUser loggedUser, Film film) {
        View view = generateFlatEntityWithoutId(View.class);
        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private View createView(ApplicationUser loggedUser, Film film, ViewStatus status, Instant startAt) {
        View view = generateFlatEntityWithoutId(View.class);
        view.setStatus(status);
        view.setStartAt(startAt);
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));

        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        return filmRepository.save(film);
    }

    private ApplicationUser createUser() {
        ApplicationUser user = generateFlatEntityWithoutId(ApplicationUser.class);
        return applicationUserRepository.save(user);
    }

    private Manager createManager() {
        Manager manager = generateFlatEntityWithoutId(Manager.class);
        return managerRepository.save(manager);
    }

    private Instant createInstant(int hour) {
        return LocalDateTime.of(2019, 12, 23, hour, 0).toInstant(ZoneOffset.UTC);
    }

}

