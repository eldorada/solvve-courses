package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.dto.ActorCreateDTO;
import com.solvve.dto.ActorPatchDTO;
import com.solvve.dto.ActorPutDTO;
import com.solvve.dto.ActorReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ActorRepository;
import com.solvve.repository.PersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ActorServiceTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ActorService actorService;

    @Test
    public void testGetActor() {
        Person person = createPerson();
        Actor actor = createActor(person);

        ActorReadDTO readDTO = actorService.getActor(actor.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(actor, "personId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetActorWrongId() {
        actorService.getActor(UUID.randomUUID());
    }

    @Test
    public void testCreateActor() {
        Person person = createPerson();

        ActorCreateDTO create = new ActorCreateDTO();
        create.setAverageMark(1.0);

        ActorReadDTO read = actorService.createPersonsActor(person.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Actor actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actor, "personId");
    }

    @Test
    public void testPatchActor() {
        Person p = createPerson();
        Actor actor = createActor(p);

        ActorPatchDTO patch = new ActorPatchDTO();
        patch.setAverageMark(2.5);
        patch.setPersonId(p.getId());

        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(actor, "personId");
    }

    @Test
    public void testUpdateActor() {
        Person person = createPerson();
        Actor actor = createActor(person);

        ActorPutDTO put = new ActorPutDTO();
        put.setAverageMark(1.2);
        put.setPersonId(person.getId());
        ActorReadDTO read = actorService.updateActor(actor.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        actor = actorRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(read, "personId");

    }

    @Test
    public void testPatchActorEmptyPatch() {
        Person person = createPerson();
        Actor actor = createActor(person);

        ActorPatchDTO patch = new ActorPatchDTO();
        ActorReadDTO read = actorService.patchActor(actor.getId(), patch);

        Assert.assertNotNull(read.getAverageMark());

        Actor actorAfterUpdate = actorRepository.findById(read.getId()).get();

        Assert.assertNotNull(actorAfterUpdate.getAverageMark());

        Assertions.assertThat(actor).isEqualToIgnoringGivenFields(actor, "personId");
    }

    @Test
    public void testDeleteActor() {
        Person person = createPerson();
        Actor actor = createActor(person);

        actorService.deleteActor(actor.getId());
        Assert.assertFalse(actorRepository.existsById(actor.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteActorNotFound() {
        actorService.deleteActor(UUID.randomUUID());
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);    
    }
    
    private Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actorRepository.save(actor);
    }

}

