package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Like;
import com.solvve.domain.LikedObjectType;
import com.solvve.dto.LikeCreateDTO;
import com.solvve.dto.LikePatchDTO;
import com.solvve.dto.LikePutDTO;
import com.solvve.dto.LikeReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.LikeRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

public class LikeServiceTest extends BaseTest {

    @Autowired
    private LikeRepository likeRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private LikeService likeService;

    @Test
    public void testGetLike() {
        Film film = createFilm();
        Like like = createLike(film);

        LikeReadDTO readDTO = likeService.getLike(like.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(like);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetLikeWrongId() {
        likeService.getLike(UUID.randomUUID());
    }

    @Test
    public void testCreateLike() {
        Film film = createFilm();
        LikeCreateDTO create = new LikeCreateDTO();
        create.setLike(true);
        create.setType(LikedObjectType.FILM);
        create.setLikedObjectId(film.getId());

        LikeReadDTO read = likeService.createLike(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Like like = likeRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(like);
    }

    @Test
    public void testPatchLike() {
        Film film = createFilm();
        Like like = createLike(film);

        LikePatchDTO patch = new LikePatchDTO();
        patch.setLike(false);
        patch.setType(LikedObjectType.FILM);
        patch.setLikedObjectId(film.getId());

        LikeReadDTO read = likeService.patchLike(like.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        like = likeRepository.findById(read.getId()).get();
        Assertions.assertThat(like).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPutLike() {
        Film film = createFilm();
        Like like = createLike(film);

        LikePutDTO put = new LikePutDTO();
        put.setLike(false);
        put.setType(LikedObjectType.FILM);
        put.setLikedObjectId(film.getId());

        LikeReadDTO read = likeService.updateLike(like.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        like = likeRepository.findById(read.getId()).get();
        Assertions.assertThat(like).isEqualToIgnoringGivenFields(read,
                "likedObjectId", "type");
    }

    @Test
    public void testPatchLikeEmptyPatch() {
        Film film = createFilm();
        Like like = createLike(film);

        LikePatchDTO patch = new LikePatchDTO();
        LikeReadDTO read = likeService.patchLike(like.getId(), patch);

        Assert.assertNotNull(read.getLike());
        Assert.assertNotNull(read.getType());
        Assert.assertNotNull(read.getLikedObjectId());

        Like likeAfterUpdate = likeRepository.findById(read.getId()).get();

        Assert.assertNotNull(likeAfterUpdate.getLike());
        Assert.assertNotNull(likeAfterUpdate.getType());
        Assert.assertNotNull(likeAfterUpdate.getLikedObjectId());

        Assertions.assertThat(like).isEqualToComparingFieldByField(likeAfterUpdate);
    }

    @Test
    public void testDeleteLike() {
        Film film = createFilm();
        Like like = createLike(film);

        likeService.deleteLike(like.getId());
        Assert.assertFalse(likeRepository.existsById(like.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteLikeNotFound() {
        likeService.deleteLike(UUID.randomUUID());
    }

    private Like createLike(Film film) {
        Like like = new Like();
        like.setLike(true);
        like.setType(LikedObjectType.FILM);
        like.setLikedObjectId(film.getId());
        return likeRepository.save(like);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

}

