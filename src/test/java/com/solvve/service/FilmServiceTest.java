package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.LoggedUser;
import com.solvve.domain.Film;
import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.dto.FilmCreateDTO;
import com.solvve.dto.FilmPatchDTO;
import com.solvve.dto.FilmPutDTO;
import com.solvve.dto.FilmReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.LoggedUserRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ViewRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

public class FilmServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private FilmService filmService;

    @Test
    public void testGetFilm() {
        Film film = createFilm();

        FilmReadDTO readDTO = filmService.getFilm(film.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(film);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetFilmWrongId() {
        filmService.getFilm(UUID.randomUUID());
    }

    @Test
    public void testCreateFilm() {

        FilmCreateDTO create = new FilmCreateDTO();
        create.setTitle("Lola");
        create.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        create.setStatus(true);

        FilmReadDTO read = filmService.createFilm(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Film film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(film);
    }

    @Test
    public void testPatchFilm() {
        Film film = createFilm();

        FilmPatchDTO patch = new FilmPatchDTO();
        patch.setTitle("Lola");
        patch.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        patch.setStatus(true);
        patch.setOriginalTitle("Run Lola");
        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(film).isEqualToIgnoringGivenFields(read,
                "reviews", "genres", "participantFilms", "roles", "countries", "companies");
    }

    @Test
    public void testPutFilm() {
        Film film = createFilm();

        FilmPutDTO put = new FilmPutDTO();
        put.setTitle("Boy");
        put.setReleaseDate(Instant.parse("2020-11-30T23:20:08Z"));
        put.setStatus(false);
        FilmReadDTO read = filmService.updateFilm(film.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        film = filmRepository.findById(read.getId()).get();
        Assertions.assertThat(film).isEqualToIgnoringGivenFields(read,
                "reviews", "genres", "participantFilms", "roles", "countries", "companies");
    }

    @Test
    public void testPatchFilmEmptyPatch() {
        Film film = createFilm();

        FilmPatchDTO patch = new FilmPatchDTO();
        FilmReadDTO read = filmService.patchFilm(film.getId(), patch);

        Assert.assertNotNull(read.getTitle());
        Assert.assertNotNull(read.getReleaseDate());
        Assert.assertNotNull(read.getStatus());

        Film filmAfterUpdate = filmRepository.findById(read.getId()).get();

        Assert.assertNotNull(filmAfterUpdate.getTitle());
        Assert.assertNotNull(filmAfterUpdate.getReleaseDate());
        Assert.assertNotNull(filmAfterUpdate.getStatus());

        Assertions.assertThat(film).isEqualToIgnoringGivenFields(filmAfterUpdate,
                "reviews", "genres", "participantFilms", "roles", "countries", "companies");
    }

    @Test
    public void testDeleteFilm() {
        Film film = createFilm();

        filmService.deleteFilm(film.getId());
        Assert.assertFalse(filmRepository.existsById(film.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteFilmNotFound() {
        filmService.deleteFilm(UUID.randomUUID());
    }

    @Test
    public void testUpdateAverageMarkOfFilm() {
        LoggedUser u1 = createUser();
        Film f1 = createFilm();

        createView(u1, f1, 3);
        createView(u1, f1, 5);

        filmService.updateAverageMarkOfFilm(f1.getId());
        f1 = filmRepository.findById(f1.getId()).get();
        Assert.assertEquals(4.0, f1.getAverageMark(), Double.MIN_NORMAL);

    }

    private View createView(LoggedUser loggedUser, Film film,
                            Integer loggedUserMark) {
        View view = generateFlatEntityWithoutId(View.class);
        //view.setStatus(ViewStatus.ONLINE);
        view.setLoggedUserMark(loggedUserMark);
        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private LoggedUser createUser() {
        LoggedUser user = generateFlatEntityWithoutId(LoggedUser.class);
        return loggedUserRepository.save(user);
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);
        return filmRepository.save(film);
    }
}

