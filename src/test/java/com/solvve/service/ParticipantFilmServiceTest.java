package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.ParticipantFilm;
import com.solvve.domain.ParticipantFilmType;
import com.solvve.domain.Person;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ParticipantFilmRepository;
import com.solvve.repository.PersonRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ParticipantFilmServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private ParticipantFilmRepository participantFilmRepository;

    @Autowired
    private ParticipantFilmService participantFilmService;

    @Test
    public void testGetFilmsParticipantFilm() {
        Person p = createPerson();
        Film f1 = createFilm();
        Film f2 = createFilm();
        ParticipantFilm r1 = createParticipantFilm(f1, p);
        ParticipantFilm r2 = createParticipantFilm(f1, p);
        createParticipantFilm(f2, p);

        Assertions.assertThat(participantFilmService.getFilmsParticipantFilms(f1.getId()))
                .extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId());
    }

    @Test
    public void testGetFilmsParticipantFilmById() {
        Person p = createPerson();
        Film f1 = createFilm();
        Film f2 = createFilm();
        ParticipantFilm r1 = createParticipantFilm(f1, p);
        createParticipantFilm(f1, p);
        createParticipantFilm(f2, p);

        ParticipantFilmReadDTO readDTO = participantFilmService.getFilmsParticipantFilms(f1.getId(), r1.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(r1, "filmId", "personId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetParticipantFilmWrongId() {
        Film f1 = createFilm();

        participantFilmService.getFilmsParticipantFilms(f1.getId(), UUID.randomUUID());
    }

    @Test
    public void testCreateFilmsParticipantFilms() {
        Person person = createPerson();
        Film film = createFilm();
        ParticipantFilmCreateDTO create = new ParticipantFilmCreateDTO();
        create.setDescription("It's Bad!");
        create.setType(ParticipantFilmType.ACTOR);
        create.setPersonId(person.getId());

        ParticipantFilmReadDTO read = participantFilmService.createFilmsParticipantFilms(film.getId(), create);
        Assertions.assertThat(create).isEqualToIgnoringGivenFields(read,"filmId");
        Assert.assertNotNull(read.getId());

        ParticipantFilm participantFilm = participantFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(participantFilm,
                "filmId", "personId");

    }

   @Test
    public void testPatchFilmsParticipantFilms() {
        Person p = createPerson();
        Film f = createFilm();
        ParticipantFilm participantFilm = createParticipantFilm(f, p);

        ParticipantFilmPatchDTO patch = new ParticipantFilmPatchDTO();
       patch.setDescription("It's Bad!");
       patch.setType(ParticipantFilmType.ACTOR);
       patch.setFilmId(f.getId());
       patch.setPersonId(p.getId());

       ParticipantFilmReadDTO read = participantFilmService.patchFilmsParticipantFilms(f.getId(),
                participantFilm.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        participantFilm = participantFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(participantFilm,
                "filmId", "personId");
    }

    @Test
    public void testPutFilmsParticipantFilms() {
        Person p = createPerson();
        Film f = createFilm();
        ParticipantFilm participantFilm = createParticipantFilm(f, p);

        ParticipantFilmPutDTO put = new ParticipantFilmPutDTO();
        put.setDescription("It's Bad!");
        put.setType(ParticipantFilmType.ACTOR);
        put.setFilmId(f.getId());
        put.setPersonId(p.getId());
        ParticipantFilmReadDTO read = participantFilmService.updateFilmsParticipantFilms(f.getId(),
                participantFilm.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        participantFilm = participantFilmRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(participantFilm,
                "filmId", "personId");
    }

    @Test
    public void testPatchParticipantFilmEmptyPatch() {
        Person p = createPerson();
        Film f = createFilm();
        ParticipantFilm participantFilm = createParticipantFilm(f, p);

        ParticipantFilmPatchDTO patch = new ParticipantFilmPatchDTO();

        ParticipantFilmReadDTO read = participantFilmService.patchFilmsParticipantFilms(f.getId(),
                participantFilm.getId(), patch);

        Assert.assertNotNull(read.getDescription());
        Assert.assertNotNull(read.getType());

        ParticipantFilm participantFilmAfterUpdate = participantFilmRepository.findById(read.getId()).get();

        Assert.assertNotNull(participantFilmAfterUpdate.getDescription());
        Assert.assertNotNull(participantFilmAfterUpdate.getType());
        Assert.assertNotNull(participantFilmAfterUpdate.getFilm());
        Assert.assertNotNull(participantFilmAfterUpdate.getPerson());

        Assertions.assertThat(participantFilm).isEqualToIgnoringGivenFields(participantFilmAfterUpdate,
                "film", "person");
    }

    @Test
    public void testDeleteFilmsParticipantFilms() {
        Person p = createPerson();
        Film f = createFilm();
        ParticipantFilm participantFilm = createParticipantFilm(f, p);

        participantFilmService.deleteFilmsParticipantFilms(f.getId(), participantFilm.getId());
        Assert.assertFalse(participantFilmRepository.existsById(participantFilm.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteParticipantFilmNotFound() {
        Film film = createFilm();
        participantFilmService.deleteFilmsParticipantFilms(film.getId(),UUID.randomUUID());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteFilmNotFound() {
        participantFilmService.deleteFilmsParticipantFilms(UUID.randomUUID(),UUID.randomUUID());
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);

        return filmRepository.save(film);
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);

        return personRepository.save(person);
    }

    private ParticipantFilm createParticipantFilm(Film film, Person person) {
        ParticipantFilm participantFilm = generateFlatEntityWithoutId(ParticipantFilm.class);

        participantFilm.setFilm(film);
        participantFilm.setPerson(person);

        return participantFilmRepository.save(participantFilm);
    }

    
}

