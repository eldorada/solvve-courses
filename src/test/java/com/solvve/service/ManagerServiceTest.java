package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Manager;
import com.solvve.dto.ManagerCreateDTO;
import com.solvve.dto.ManagerPatchDTO;
import com.solvve.dto.ManagerPutDTO;
import com.solvve.dto.ManagerReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.ManagerRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

public class ManagerServiceTest extends BaseTest {

    @Autowired
    private ManagerRepository managerRepository;

    @Autowired
    private ManagerService managerService;

    @Test
    public void testGetUser() {
        Manager user = createUser();

        ManagerReadDTO readDTO = managerService.getManager(user.getId());
        Assertions.assertThat(readDTO).isEqualToComparingFieldByField(user);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetUserWrongId() {
        managerService.getManager(UUID.randomUUID());
    }

    @Test
    public void testCreateUser() {

        ManagerCreateDTO create = new ManagerCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);
        ManagerReadDTO read = managerService.createManager(create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Manager user = managerRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToComparingFieldByField(user);
    }

    @Test
    public void testPatchUser() {
        Manager user = createUser();

        ManagerPatchDTO patch = new ManagerPatchDTO();
        patch.setName("Petya");
        patch.setLogin("Petya1991");
        patch.setRating(1.2);
        ManagerReadDTO read = managerService.patchManager(user.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        user = managerRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testUpdateUser() {
        Manager user = createUser();

        ManagerPutDTO put = new ManagerPutDTO();
        put.setName("Petya");
        put.setLogin("Petya1991");
        put.setRating(1.2);
        ManagerReadDTO read = managerService.updateManager(user.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        user = managerRepository.findById(read.getId()).get();
        Assertions.assertThat(user).isEqualToIgnoringGivenFields(read,
                "userRoles", "createdAt", "updatedAt");
    }

    @Test
    public void testPatchUserEmptyPatch() {
        Manager user = createUser();

        ManagerPatchDTO patch = new ManagerPatchDTO();
        ManagerReadDTO read = managerService.patchManager(user.getId(), patch);

        Assert.assertNotNull(read.getLogin());
        Assert.assertNotNull(read.getName());
        Assert.assertNotNull(read.getRating());

        Manager userAfterUpdate = managerRepository.findById(read.getId()).get();

        Assert.assertNotNull(userAfterUpdate.getLogin());
        Assert.assertNotNull(userAfterUpdate.getName());
        Assert.assertNotNull(userAfterUpdate.getRating());

        Assertions.assertThat(user).isEqualToIgnoringGivenFields(userAfterUpdate,
                "userRoles");
    }

    @Test
    public void testDeleteUser() {
        Manager user = createUser();

        managerService.deleteManager(user.getId());
        Assert.assertFalse(managerRepository.existsById(user.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteUserNotFound() {
        managerService.deleteManager(UUID.randomUUID());
    }

    private Manager createUser() {
        Manager user = new Manager();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return managerRepository.save(user);
    }

}
