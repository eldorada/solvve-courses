package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRole;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.UserRoleReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.exception.LinkDuplicatedException;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.UserRoleRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.UUID;

public class UserRoleServiceTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testAddUserRoleToApplicationUser() {
        ApplicationUser applicationUser = createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);

        List<UserRoleReadDTO> res = userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);

        UserRoleReadDTO expectedRead = new UserRoleReadDTO();
        expectedRead.setId(userRoleId);
        expectedRead.setType(UserRoleType.MANAGER);
        Assertions.assertThat(res).containsExactlyInAnyOrder(expectedRead);

        transactionTemplate.executeWithoutResult(status -> {
            ApplicationUser applicationUserAfterSave = applicationUserRepository
                    .findById(applicationUser.getId()).get();
            Assertions.assertThat(applicationUserAfterSave.getUserRoles())
                    .extracting(UserRole::getId).containsExactlyInAnyOrder(userRoleId);
        });
    }

    @Test
    public void testDuplicatedUserRole() {
        ApplicationUser applicationUser = createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);

        userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);

        Assertions.assertThatThrownBy(() -> {
            userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);
        }).isInstanceOf(LinkDuplicatedException.class);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongUserRoleId() {
        ApplicationUser applicationUser = createUser();
        UUID wrongUserRoleId = UUID.randomUUID();
        userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), wrongUserRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testWrongApplicationUserId() {
        UUID wrongApplicationUserId = UUID.randomUUID();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);
        userRoleService.addUserRoleToApplicationUser(wrongApplicationUserId, userRoleId);
    }

    @Test
    public void testRemoveUserRoleFromApplicationUser() {
        ApplicationUser applicationUser = createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);
        addUserRoleToApplicationUser(applicationUser, userRoleId);

        List<UserRoleReadDTO> remainingUserRoles = userRoleService
                .removeUserRoleFromApplicationUser(applicationUser.getId(), userRoleId);
        Assert.assertTrue(remainingUserRoles.isEmpty());

        transactionTemplate.executeWithoutResult(status -> {
            ApplicationUser applicationUserAfterRemove = applicationUserRepository
                    .findById(applicationUser.getId()).get();
            Assert.assertTrue(applicationUserAfterRemove.getUserRoles().isEmpty());
        });
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotAddedUserRole() {
        ApplicationUser applicationUser = createUser();
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);

        userRoleService.removeUserRoleFromApplicationUser(applicationUser.getId(), userRoleId);
    }

    @Test(expected = EntityNotFoundException.class)
    public void testRemoveNotExistedRole() {
        ApplicationUser applicationUser = createUser();

        userRoleService.removeUserRoleFromApplicationUser(applicationUser.getId(), UUID.randomUUID());
    }

    private ApplicationUser createUser() {
        ApplicationUser user = generateFlatEntityWithoutId(ApplicationUser.class);
        return applicationUserRepository.save(user);
    }

    private void addUserRoleToApplicationUser(ApplicationUser applicationUser, UUID userRoleId) {
        userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);
    }

}
