package com.solvve.service;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Review;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ReviewRepository;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

public class ReviewServiceTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private ReviewService reviewService;

    @Test
    public void testGetReview() {

        Review review = createReview(createFilm());

        ReviewReadDTO readDTO = reviewService.getReview(review.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(review, "filmId");
    }

    @Test
    public void testGetFilmsReview() {

        Film f1 = createFilm();
        Film f2 = createFilm();
        Review r1 = createReview(f1);
        Review r2 = createReview(f1);
        createReview(f2);

        Assertions.assertThat(reviewService.getFilmsReviews(f1.getId())).extracting("id")
                .containsExactlyInAnyOrder(r1.getId(), r2.getId());
    }

    @Test
    public void testGetFilmsReviewById() {

        Film f1 = createFilm();
        Film f2 = createFilm();
        Review r1 = createReview(f1);
        createReview(f1);
        createReview(f2);

        ReviewReadDTO readDTO = reviewService.getFilmsReviews(f1.getId(), r1.getId());
        Assertions.assertThat(readDTO).isEqualToIgnoringGivenFields(r1, "filmId");
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReviewWrongId() {
        reviewService.getReview(UUID.randomUUID());
    }

    @Test
    public void testCreateFilmsReviews() {
        Film film = createFilm();
        ReviewCreateDTO create = new ReviewCreateDTO();
        create.setText("It's Bad!");
        create.setDislike(123);
        create.setLike(13);
        create.setStatus(ReviewStatus.ACCEPTED);
        create.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));

        ReviewReadDTO read = reviewService.createFilmsReviews(film.getId(), create);
        Assertions.assertThat(create).isEqualToComparingFieldByField(read);
        Assert.assertNotNull(read.getId());

        Review review = reviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "filmId");

    }

    @Test
    public void testPatchReview() {
        Film film = createFilm();
        Review review = createReview(film);

        ReviewPatchDTO patch = new ReviewPatchDTO();
        patch.setText("It's Bad!");
        patch.setDislike(123);
        patch.setLike(13);
        patch.setStatus(ReviewStatus.ACCEPTED);
        patch.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        patch.setFilmId(film.getId());

        ReviewReadDTO read = reviewService.patchReview(review.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        review = reviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "filmId");
    }

    @Test
    public void testPatchFilmsReviews() {
        Film film = createFilm();
        Review review = createReview(film);

        ReviewPatchDTO patch = new ReviewPatchDTO();
        patch.setText("It's Bad!");
        patch.setDislike(123);
        patch.setLike(13);
        patch.setStatus(ReviewStatus.ACCEPTED);
        patch.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        patch.setFilmId(film.getId());

        ReviewReadDTO read = reviewService.patchFilmsReviews(film.getId(), review.getId(), patch);

        Assertions.assertThat(patch).isEqualToComparingFieldByField(read);

        review = reviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "filmId");
    }

    @Test
    public void testPutReview() {
        Film film = createFilm();
        Review review = createReview(film);

        ReviewPutDTO put = new ReviewPutDTO();
        put.setText("It's Bad!");
        put.setDislike(123);
        put.setLike(13);
        put.setStatus(ReviewStatus.ACCEPTED);
        put.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        put.setFilmId(film.getId());
        ReviewReadDTO read = reviewService.updateReview(review.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        review = reviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "filmId");
    }

    @Test
    public void testPutFilmsReviews() {
        Film film = createFilm();
        Review review = createReview(film);

        ReviewPutDTO put = new ReviewPutDTO();
        put.setText("It's Bad!");
        put.setDislike(123);
        put.setLike(13);
        put.setStatus(ReviewStatus.ACCEPTED);
        put.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        put.setFilmId(film.getId());
        ReviewReadDTO read = reviewService.updateFilmsReviews(film.getId(), review.getId(), put);

        Assertions.assertThat(put).isEqualToComparingFieldByField(read);

        review = reviewRepository.findById(read.getId()).get();
        Assertions.assertThat(read).isEqualToIgnoringGivenFields(review, "filmId");
    }

    @Test
    public void testPatchReviewEmptyPatch() {
        Film film = createFilm();
        Review review = createReview(film);

        ReviewPatchDTO patch = new ReviewPatchDTO();

        ReviewReadDTO read = reviewService.patchReview(review.getId(), patch);

        Assert.assertNotNull(read.getText());
        Assert.assertNotNull(read.getLike());
        Assert.assertNotNull(read.getDislike());
        Assert.assertNotNull(read.getModifyDate());
        Assert.assertNotNull(read.getStatus());

        Review reviewAfterUpdate = reviewRepository.findById(read.getId()).get();

        Assert.assertNotNull(reviewAfterUpdate.getText());
        Assert.assertNotNull(reviewAfterUpdate.getLike());
        Assert.assertNotNull(reviewAfterUpdate.getDislike());
        Assert.assertNotNull(reviewAfterUpdate.getModifyDate());
        Assert.assertNotNull(reviewAfterUpdate.getStatus());
        Assert.assertNotNull(reviewAfterUpdate.getFilm());

        Assertions.assertThat(review).isEqualToIgnoringGivenFields(reviewAfterUpdate, "film");
    }

    @Test
    public void testDeleteReview() {
        Review review = createReview(createFilm());

        reviewService.deleteReview(review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
    }

    @Test
    public void testDeleteFilmsReviews() {
        Film film = createFilm();
        Review review = createReview(film);

        reviewService.deleteFilmsReviews(film.getId(), review.getId());
        Assert.assertFalse(reviewRepository.existsById(review.getId()));
    }

    @Test(expected = EntityNotFoundException.class)
    public void testDeleteReviewNotFound() {
        reviewService.deleteReview(UUID.randomUUID());
    }

    private Review createReview(Film film) {
        Review review = new Review();
        review.setText("It's Nice!");
        review.setDislike(1);
        review.setLike(134);
        review.setStatus(ReviewStatus.ON_CHECK);
        review.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        review.setFilm(film);
        return reviewRepository.save(review);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

}

