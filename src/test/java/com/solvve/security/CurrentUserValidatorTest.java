package com.solvve.security;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.repository.ApplicationUserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;

public class CurrentUserValidatorTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private CurrentUserValidator currentUserValidator;

    @MockBean
    private AuthenticationResolver authenticationResolver;

    @Test
    public void testIsCurrentUser() {
        ApplicationUser user = generateFlatEntityWithoutId(ApplicationUser.class);
        user = applicationUserRepository.save(user);

        Authentication authentication = new TestingAuthenticationToken(user.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertTrue(currentUserValidator.isCurrentUser(user.getId()));
    }

    @Test
    public void testIsDifferentUser() {
        ApplicationUser user1 = generateFlatEntityWithoutId(ApplicationUser.class);
        user1 = applicationUserRepository.save(user1);
        ApplicationUser user2 = generateFlatEntityWithoutId(ApplicationUser.class);
        user2 = applicationUserRepository.save(user2);

        Authentication authentication = new TestingAuthenticationToken(user1.getEmail(), null);
        Mockito.when(authenticationResolver.getCurrentAuthentication()).thenReturn(authentication);

        Assert.assertFalse(currentUserValidator.isCurrentUser(user2.getId()));
    }

}
