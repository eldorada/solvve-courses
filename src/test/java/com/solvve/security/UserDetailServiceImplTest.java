package com.solvve.security;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.UserRoleRepository;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.IterableUtil;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.ArrayList;

public class UserDetailServiceImplTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private UserDetailsServiceImpl userDetailsService;

    @Test
    public void testLoadUserByUsername() {
        ApplicationUser applicationUser = transactionTemplate.execute(status -> {
            ApplicationUser u = generateFlatEntityWithoutId(ApplicationUser.class);
            u.setUserRoles(new ArrayList<>(IterableUtil.toCollection(userRoleRepository.findAll())));
            return applicationUserRepository.save(u);
        });

        UserDetails userDetails = userDetailsService.loadUserByUsername(applicationUser.getEmail());
        Assert.assertEquals(applicationUser.getEmail(), userDetails.getUsername());
        Assert.assertEquals(applicationUser.getEncodedPassword(), userDetails.getPassword());
        Assert.assertFalse(userDetails.getAuthorities().isEmpty());
        Assertions.assertThat(userDetails.getAuthorities())
                .extracting("authority").containsExactlyInAnyOrder(
                        applicationUser.getUserRoles().stream().map(ur -> ur.getType().toString()).toArray());

    }

    @Test(expected = UsernameNotFoundException.class)
    public void testUserNotFound() {
        userDetailsService.loadUserByUsername("Wrong name");
    }
}
