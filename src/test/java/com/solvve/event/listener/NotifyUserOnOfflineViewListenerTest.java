package com.solvve.event.listener;

import com.solvve.domain.ReviewStatus;
import com.solvve.domain.ViewStatus;
import com.solvve.event.ReviewStatusChangedEvent;
import com.solvve.event.ViewStatusChangedEvent;
import com.solvve.service.UserNotificationService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;

import static org.mockito.ArgumentMatchers.any;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
public class NotifyUserOnOfflineViewListenerTest {

    @MockBean
    private UserNotificationService userNotificationService;

    @SpyBean
    private NotifyUserOnOfflineViewListener notifyUserOnOfflineViewListener;

    @SpyBean
    private NotifyUserOnAcceptedReviewListener notifyUserOnAcceptedReviewListener;

    @Autowired
    private ApplicationEventPublisher applicationEventPublisher;

    @Test
    public void testOnEventForView() {
        ViewStatusChangedEvent event = new ViewStatusChangedEvent();
        event.setViewId(UUID.randomUUID());
        event.setNewStatus(ViewStatus.OFFLINE);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserOnOfflineViewListener,Mockito.timeout(500)).onEvent(event);
        Mockito.verify(userNotificationService, Mockito.timeout(500))
                .notifyOnViewStatusChangedToOffline(event.getViewId());
    }

    @Test
    public void testOnEventNotOfflineForView() {
        for (ViewStatus viewStatus : ViewStatus.values()) {
            if (viewStatus == ViewStatus.OFFLINE) {
                continue;
            }

            ViewStatusChangedEvent event = new ViewStatusChangedEvent();
            event.setViewId(UUID.randomUUID());
            event.setNewStatus(viewStatus);
            applicationEventPublisher.publishEvent(event);

            Mockito.verify(notifyUserOnOfflineViewListener, Mockito.never()).onEvent(any());
            Mockito.verify(userNotificationService,Mockito.never()).notifyOnViewStatusChangedToOffline(any());
        }
    }

    @Test
    public void testOnEventForViewAsync() throws InterruptedException {
        ViewStatusChangedEvent event = new ViewStatusChangedEvent();
        event.setViewId(UUID.randomUUID());
        event.setNewStatus(ViewStatus.OFFLINE);

        List<Integer> checklist = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            checklist.add(2);
            latch.countDown();
            return null;
        }).when(userNotificationService).notifyOnViewStatusChangedToOffline(event.getViewId());

        applicationEventPublisher.publishEvent(event);
        checklist.add(1);

        latch.await();
        Mockito.verify(notifyUserOnOfflineViewListener).onEvent(event);
        Mockito.verify(userNotificationService).notifyOnViewStatusChangedToOffline(event.getViewId());
        Assert.assertEquals(Arrays.asList(1, 2), checklist);
    }

    @Test
    public void testOnEventForReview() {
        ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setNewStatus(ReviewStatus.ACCEPTED);
        applicationEventPublisher.publishEvent(event);

        Mockito.verify(notifyUserOnAcceptedReviewListener,Mockito.timeout(500)).onEvent(event);
        Mockito.verify(userNotificationService, Mockito.timeout(500))
                .notifyOnReviewStatusChangedToAccepted(event.getReviewId());
    }

    @Test
    public void testOnEventNotOfflineForReview() {
        for (ReviewStatus viewStatus : ReviewStatus.values()) {
            if (viewStatus == ReviewStatus.ACCEPTED) {
                continue;
            }

            ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
            event.setReviewId(UUID.randomUUID());
            event.setNewStatus(viewStatus);
            applicationEventPublisher.publishEvent(event);

            Mockito.verify(notifyUserOnAcceptedReviewListener, Mockito.never()).onEvent(any());
            Mockito.verify(userNotificationService,Mockito.never()).notifyOnReviewStatusChangedToAccepted(any());
        }
    }

    @Test
    public void testOnEventForReviewAsync() throws InterruptedException {
        ReviewStatusChangedEvent event = new ReviewStatusChangedEvent();
        event.setReviewId(UUID.randomUUID());
        event.setNewStatus(ReviewStatus.ACCEPTED);

        List<Integer> checklist = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        Mockito.doAnswer(invocationOnMock -> {
            Thread.sleep(500);
            checklist.add(2);
            latch.countDown();
            return null;
        }).when(userNotificationService).notifyOnReviewStatusChangedToAccepted(event.getReviewId());

        applicationEventPublisher.publishEvent(event);
        checklist.add(1);

        latch.await();
        Mockito.verify(notifyUserOnAcceptedReviewListener).onEvent(event);
        Mockito.verify(userNotificationService).notifyOnReviewStatusChangedToAccepted(event.getReviewId());
        Assert.assertEquals(Arrays.asList(1, 2), checklist);
    }
}
