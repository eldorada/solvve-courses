package com.solvve.base;

import com.solvve.domain.AbstractEntity;
import org.bitbucket.brunneng.br.Configuration;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Sql(statements = {
        "delete from external_system_import",
        "delete from view",
        "delete from review",
        "delete from role",
        "delete from participant_film",
        "delete from signal_to_manager",
        "delete from like",
        "delete from actor",
        "delete from person",
        "delete from application_user",
        "delete from film"},
        executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public abstract class BaseTest {

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    protected <T> T generateObject(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        return entity;
    }

    protected <T> T generateFlatObject(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        return entity;
    }

    protected <T extends AbstractEntity> T generateEntityWithoutId(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    protected <T extends AbstractEntity> T generateFlatEntityWithoutId(Class<T> entityClass) {
        T entity = flatGenerator.generateRandomObject(entityClass);
        entity.setId(null);
        return entity;
    }

    private RandomObjectGenerator flatGenerator; {
        Configuration c = new Configuration();
        c.setFlatMode(true);
        flatGenerator = new RandomObjectGenerator(c);
    }
}
