package com.solvve.job;

import com.solvve.base.BaseTest;
import com.solvve.domain.LoggedUser;
import com.solvve.domain.Film;
import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.repository.LoggedUserRepository;
import com.solvve.repository.FilmRepository;
import com.solvve.repository.ViewRepository;
import com.solvve.service.FilmService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.time.Instant;
import java.util.UUID;

public class UpdateAverageMarkOfFilmsJobTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private UpdateAverageMarkOfFilmsJob updateAverageMarkOfFilmsJob;

    @SpyBean
    private FilmService filmService;

    @Test
    public void testUpdateAverageMarkOfFilms() {
        LoggedUser u1 = createUser();
        Film f1 = createFilm();

        createView(u1, f1, 3);
        createView(u1, f1, 5);
        updateAverageMarkOfFilmsJob.updateAverageMarkOfFilms();

        f1 = filmRepository.findById(f1.getId()).get();
        Assert.assertEquals(4.0, f1.getAverageMark(), Double.MIN_NORMAL);

    }

    @Test
    public void testFilmsUpdatedIndependently() {
        LoggedUser u1 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1, 3);
        createView(u1, f2, 5);

        UUID[] failedId = new UUID[1];
        Mockito.doAnswer(invocationOnMock -> {
            if (failedId[0] == null) {
                failedId[0] = invocationOnMock.getArgument(0);
                throw new RuntimeException();
            }
            return invocationOnMock.callRealMethod();
        }).when(filmService).updateAverageMarkOfFilm(Mockito.any());

        updateAverageMarkOfFilmsJob.updateAverageMarkOfFilms();

        for (Film f : filmRepository.findAll()) {
            if (f.getId().equals(failedId[0])) {
                Assert.assertNull(f.getAverageMark());
            } else {
                Assert.assertNotNull(f.getAverageMark());
            }
        }
    }

    private View createView(LoggedUser loggedUser, Film film,
                            Integer loggedUserMark) {
        View view = new View();
        view.setStatus(ViewStatus.ONLINE);
        view.setStartAt(Instant.parse("2019-12-31T23:20:08Z"));
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        view.setLoggedUserMark(loggedUserMark);
        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private LoggedUser createUser() {
        LoggedUser user = new LoggedUser();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return loggedUserRepository.save(user);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }
}