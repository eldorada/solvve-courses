package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.UserRoleReadDTO;
import com.solvve.service.UserRoleService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(UserRoleController.class)
public class UserRoleControllerTest extends BaseControllerTest{

    @MockBean
    private UserRoleService userRoleService;

    @Test
    public void testAddUserRoleToApplicationUser() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        UserRoleReadDTO read = new UserRoleReadDTO();
        read.setId(userRoleId);
        read.setType(UserRoleType.MANAGER);
        List<UserRoleReadDTO> expectedUserRoles = List.of(read);
        Mockito.when(userRoleService.addUserRoleToApplicationUser(userId, userRoleId)).thenReturn(expectedUserRoles);

        String resultJson = mvc.perform(post("/api/v1/users/{userId}/userRoles/{id}", userId, userRoleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUserRoles = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedUserRoles, actualUserRoles);
    }

    @Test
    public void testRemoveUserRoleFromApplicationUser() throws Exception {
        UUID userId = UUID.randomUUID();
        UUID userRoleId = UUID.randomUUID();

        UserRoleReadDTO read = new UserRoleReadDTO();
        read.setId(userRoleId);
        read.setType(UserRoleType.MANAGER);
        List<UserRoleReadDTO> expectedUserRoles = List.of(read);
        Mockito.when(userRoleService.removeUserRoleFromApplicationUser(userId, userRoleId))
                .thenReturn(expectedUserRoles);

        String resultJson = mvc.perform(delete("/api/v1/users/{userId}/userRoles/{id}", userId, userRoleId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<UserRoleReadDTO> actualUserRoles = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedUserRoles, actualUserRoles);
    }
}
