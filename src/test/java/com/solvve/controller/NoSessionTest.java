package com.solvve.controller;

import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.service.ApplicationUserService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(controllers = ApplicationUserController.class)
public class NoSessionTest extends BaseControllerTest{

    @MockBean
    private ApplicationUserService applicationUserService;

    @Test
    public void testNoSession() throws Exception {
        UUID wrongId = UUID.randomUUID();

        Mockito.when(applicationUserService.getApplicationUser(wrongId)).thenReturn(new ApplicationUserReadDTO());

        MvcResult mvcResult = mvc.perform(get("/api/v1/users/{id}", wrongId))
                .andExpect(status().isOk())
                .andReturn();
        Assert.assertNull(mvcResult.getRequest().getSession(false));
    }
}
