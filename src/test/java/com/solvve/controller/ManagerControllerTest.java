package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Manager;
import com.solvve.dto.*;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ManagerController.class)
public class ManagerControllerTest extends BaseControllerTest{

    @MockBean
    private ManagerService managerService;

    @Test
    public void testGetManagerViews() throws Exception {
        UUID managerId = UUID.randomUUID();

        List<ViewReadDTO> expectedViews = Arrays.asList(generateObject(ViewReadDTO.class));

        Mockito.when(managerService.getManagerViews(managerId)).thenReturn(expectedViews);

        String resultJson = mvc.perform(get("/api/v1/managers/{id}/views", managerId.toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ViewReadDTO> actualViews = objectMapper.readValue(resultJson, new TypeReference<List<ViewReadDTO>>() {
        });
        Assert.assertEquals(expectedViews, actualViews);
    }


    @Test
    public void testGetUser() throws Exception {
        ManagerReadDTO manager = createManagerRead();

        Mockito.when(managerService.getManager(manager.getId())).thenReturn(manager);

        String resultJson = mvc.perform(get("/api/v1/managers/{id}", manager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ManagerReadDTO actualUser = objectMapper.readValue(resultJson, ManagerReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(manager);

        Mockito.verify(managerService).getManager(manager.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(Manager.class, wrongId);
        Mockito.when(managerService.getManager(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/managers/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/managers/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        ManagerCreateDTO create = new ManagerCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);

        ManagerReadDTO read = createManagerRead();

        Mockito.when(managerService.createManager(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/managers")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ManagerReadDTO actualUser = objectMapper.readValue(resultJson, ManagerReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchManager() throws Exception {
        ManagerPatchDTO patchDTO = new ManagerPatchDTO();
        patchDTO.setName("Vasya");
        patchDTO.setLogin("Vasya1993");
        patchDTO.setRating(7.1);

        ManagerReadDTO read = createManagerRead();

        Mockito.when(managerService.patchManager(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/managers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ManagerReadDTO actualUser = objectMapper.readValue(resultJson, ManagerReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateManager() throws Exception {
        ManagerPutDTO putDTO = new ManagerPutDTO();
        putDTO.setName("Vasya");
        putDTO.setLogin("Vasya1993");
        putDTO.setRating(7.1);

        ManagerReadDTO read = createManagerRead();

        Mockito.when(managerService.updateManager(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/managers/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ManagerReadDTO actualUser = objectMapper.readValue(resultJson, ManagerReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteManager() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/managers/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(managerService).deleteManager(id);
    }

    private ManagerReadDTO createManagerRead() {
        ManagerReadDTO read = generateObject(ManagerReadDTO.class);
        return read;
    }

}
