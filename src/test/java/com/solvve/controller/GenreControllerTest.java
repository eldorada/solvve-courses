package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.GenreType;
import com.solvve.dto.GenreReadDTO;
import com.solvve.service.GenreService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(GenreController.class)
public class GenreControllerTest extends BaseControllerTest{

    @MockBean
    private GenreService genreService;

    @Test
    public void testAddGenreToFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = new GenreReadDTO();
        read.setId(genreId);
        read.setType(GenreType.DRAMA);
        List<GenreReadDTO> expectedGenres = List.of(read);
        Mockito.when(genreService.addGenreToFilm(filmId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(post("/api/v1/films/{filmId}/genres/{id}", filmId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedGenres, actualGenres);
    }

    @Test
    public void testRemoveGenreFromFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID genreId = UUID.randomUUID();

        GenreReadDTO read = new GenreReadDTO();
        read.setId(genreId);
        read.setType(GenreType.DRAMA);
        List<GenreReadDTO> expectedGenres = List.of(read);
        Mockito.when(genreService.removeGenreFromFilm(filmId, genreId)).thenReturn(expectedGenres);

        String resultJson = mvc.perform(delete("/api/v1/films/{filmId}/genres/{id}", filmId, genreId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<GenreReadDTO> actualGenres = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedGenres, actualGenres);
    }
}
