package com.solvve.controller;

import com.solvve.domain.Moderator;
import com.solvve.dto.ModeratorCreateDTO;
import com.solvve.dto.ModeratorPatchDTO;
import com.solvve.dto.ModeratorPutDTO;
import com.solvve.dto.ModeratorReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ModeratorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ModeratorController.class)
public class ModeratorControllerTest extends BaseControllerTest{

    @MockBean
    private ModeratorService moderatorService;

    @Test
    public void testGetUser() throws Exception {
        ModeratorReadDTO moderator = createModeratorRead();

        Mockito.when(moderatorService.getModerator(moderator.getId())).thenReturn(moderator);

        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", moderator.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualUser = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(moderator);

        Mockito.verify(moderatorService).getModerator(moderator.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(Moderator.class, wrongId);
        Mockito.when(moderatorService.getModerator(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/moderators/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/moderators/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        ModeratorCreateDTO create = new ModeratorCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);

        ModeratorReadDTO read = createModeratorRead();

        Mockito.when(moderatorService.createModerator(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/moderators")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualUser = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchModerator() throws Exception {
        ModeratorPatchDTO patchDTO = new ModeratorPatchDTO();
        patchDTO.setName("Vasya");
        patchDTO.setLogin("Vasya1993");
        patchDTO.setRating(7.1);

        ModeratorReadDTO read = createModeratorRead();

        Mockito.when(moderatorService.patchModerator(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/moderators/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualUser = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateModerator() throws Exception {
        ModeratorPutDTO putDTO = new ModeratorPutDTO();
        putDTO.setName("Vasya");
        putDTO.setLogin("Vasya1993");
        putDTO.setRating(7.1);

        ModeratorReadDTO read = createModeratorRead();

        Mockito.when(moderatorService.updateModerator(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/moderators/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ModeratorReadDTO actualUser = objectMapper.readValue(resultJson, ModeratorReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteModerator() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/moderators/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(moderatorService).deleteModerator(id);
    }

    private ModeratorReadDTO createModeratorRead() {
        ModeratorReadDTO read = generateObject(ModeratorReadDTO.class);
        return read;
    }

}
