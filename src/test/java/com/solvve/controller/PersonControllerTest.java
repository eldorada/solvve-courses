package com.solvve.controller;

import com.solvve.dto.PersonCreateDTO;
import com.solvve.dto.PersonPatchDTO;
import com.solvve.dto.PersonPutDTO;
import com.solvve.dto.PersonReadDTO;
import com.solvve.service.PersonService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(PersonController.class)
public class PersonControllerTest extends BaseControllerTest{

    @MockBean
    private PersonService personService;

    @Test
    public void testGetPerson() throws Exception {
        PersonReadDTO person = createPersonReadDTO();

        Mockito.when(personService.getPerson(person.getId())).thenReturn(person);

        String resultJson = mvc.perform(get("/api/v1/persons/{id}", person.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(person);

        Mockito.verify(personService).getPerson(person.getId());
    }

    @Test
    public void testCreatePerson() throws Exception {
        PersonCreateDTO create = new PersonCreateDTO();
        create.setFirstName("Lola");
        create.setSecondName("Ivanova");
        create.setDateOfBirth(LocalDate.parse("2019-12-31"));
        create.setBiography("Small");

        PersonReadDTO read = createPersonReadDTO();

        Mockito.when(personService.createPerson(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/persons")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assertions.assertThat(actualPerson).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchPerson() throws Exception {
        PersonPatchDTO patch = new PersonPatchDTO();
        patch.setFirstName("Lola");
        patch.setSecondName("Ivanova");
        patch.setDateOfBirth(LocalDate.parse("2019-12-31"));
        patch.setBiography("Small");

        PersonReadDTO read = createPersonReadDTO();

        Mockito.when(personService.patchPerson(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @Test
    public void testPutPerson() throws Exception {
        PersonPutDTO put = new PersonPutDTO();
        put.setFirstName("Lola");
        put.setSecondName("Ivanova");
        put.setDateOfBirth(LocalDate.parse("2019-12-31"));
        put.setBiography("Small");

        PersonReadDTO read = createPersonReadDTO();

        Mockito.when(personService.updatePerson(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/persons/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        PersonReadDTO actualPerson = objectMapper.readValue(resultJson, PersonReadDTO.class);
        Assert.assertEquals(read, actualPerson);
    }

    @Test
    public void testDeletePerson() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/persons/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(personService).deletePerson(id);
    }

    private PersonReadDTO createPersonReadDTO() {
        PersonReadDTO read = generateObject(PersonReadDTO.class);
        return read;
    }

}
