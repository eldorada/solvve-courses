package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.CountryType;
import com.solvve.dto.CountryReadDTO;
import com.solvve.service.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(CountryController.class)
public class CountryControllerTest extends BaseControllerTest{

    @MockBean
    private CountryService countryService;

    @Test
    public void testAddCountryToFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID countryId = UUID.randomUUID();

        CountryReadDTO read = new CountryReadDTO();
        read.setId(countryId);
        read.setType(CountryType.CHINA);
        List<CountryReadDTO> expectedCountries = List.of(read);
        Mockito.when(countryService.addCountryToFilm(filmId, countryId)).thenReturn(expectedCountries);

        String resultJson = mvc.perform(post("/api/v1/films/{filmId}/countries/{id}", filmId, countryId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CountryReadDTO> actualCountries = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedCountries, actualCountries);
    }

    @Test
    public void testRemoveCountryFromFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID countryId = UUID.randomUUID();

        CountryReadDTO read = new CountryReadDTO();
        read.setId(countryId);
        read.setType(CountryType.CHINA);
        List<CountryReadDTO> expectedCountries = List.of(read);
        Mockito.when(countryService.removeCountryFromFilm(filmId, countryId)).thenReturn(expectedCountries);

        String resultJson = mvc.perform(delete("/api/v1/films/{filmId}/countries/{id}", filmId, countryId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CountryReadDTO> actualCountries = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedCountries, actualCountries);
    }
}
