package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Film;
import com.solvve.domain.ReviewStatus;
import com.solvve.dto.*;
import com.solvve.service.ReviewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ReviewController.class)
public class ReviewControllerTest extends BaseControllerTest{

    @MockBean
    private ReviewService reviewService;

    @Test
    public void testGetReview() throws Exception {
        ReviewReadDTO review = createReviewReadDTO();

        Mockito.when(reviewService.getReview(review.getId())).thenReturn(review);

        String resultJson = mvc.perform(get("/api/v1/reviews/{id}", review.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assertions.assertThat(actualReview).isEqualToComparingFieldByField(review);

        Mockito.verify(reviewService).getReview(review.getId());
    }

    @Test
    public void testGetFilmsReviews() throws Exception {

        ReviewReadDTO read = new ReviewReadDTO();
        read.setFilmId(UUID.randomUUID());
        List<ReviewReadDTO> expectedResult = List.of(read);

        Mockito.when(reviewService.getFilmsReviews(read.getFilmId())).thenReturn(expectedResult);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/reviews", read.getFilmId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ReviewReadDTO> actualResult = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetFilmsReviewsById() throws Exception {

        ReviewReadDTO read = createReviewReadDTO();
        read.setFilmId(UUID.randomUUID());

        Mockito.when(reviewService.getFilmsReviews(read.getFilmId(), read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/reviews/{id}",
                read.getFilmId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assertions.assertThat(actualReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreateFilmsReviews() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ReviewCreateDTO create = new ReviewCreateDTO();

        create.setText("It's Bad!");
        create.setDislike(123);
        create.setLike(13);
        create.setStatus(ReviewStatus.ACCEPTED);
        create.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));

        ReviewReadDTO read = createReviewReadDTO();

        Mockito.when(reviewService.createFilmsReviews(film.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films/{filmsId}/reviews", film.getId().toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assertions.assertThat(actualReview).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchReview() throws Exception {
        ReviewPatchDTO patch = new ReviewPatchDTO();
        patch.setText("It's Bad!");
        patch.setDislike(123);
        patch.setLike(13);
        patch.setStatus(ReviewStatus.ACCEPTED);
        patch.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        patch.setFilmId(UUID.randomUUID());

        ReviewReadDTO read = createReviewReadDTO();

        Mockito.when(reviewService.patchReview(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(read, actualReview);
    }

    @Test
    public void testPatchFilmsReviews() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ReviewPatchDTO patch = new ReviewPatchDTO();
        patch.setText("It's Bad!");
        patch.setDislike(123);
        patch.setLike(13);
        patch.setStatus(ReviewStatus.ACCEPTED);
        patch.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        patch.setFilmId(film.getId());

        ReviewReadDTO read = createReviewReadDTO();

        Mockito.when(reviewService.patchFilmsReviews(film.getId(), read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{filmsId}/reviews/{id}",
                film.getId(), read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(read, actualReview);
    }

    @Test
    public void testPutFilmsReviews() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ReviewPutDTO put = new ReviewPutDTO();
        put.setText("It's Bad!");
        put.setDislike(123);
        put.setLike(13);
        put.setStatus(ReviewStatus.ACCEPTED);
        put.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        put.setFilmId(film.getId());

        ReviewReadDTO read = createReviewReadDTO();

        Mockito.when(reviewService.updateFilmsReviews(film.getId(), read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{filmsId}/reviews/{id}",
                film.getId(), read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(read, actualReview);
    }

    @Test
    public void testPutReview() throws Exception {
        ReviewPutDTO put = new ReviewPutDTO();
        put.setText("It's Bad!");
        put.setDislike(123);
        put.setLike(13);
        put.setStatus(ReviewStatus.ACCEPTED);
        put.setModifyDate(Instant.parse("2019-12-31T23:21:08Z"));
        put.setFilmId(UUID.randomUUID());

        ReviewReadDTO read = createReviewReadDTO();

        Mockito.when(reviewService.updateReview(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/reviews/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ReviewReadDTO actualReview = objectMapper.readValue(resultJson, ReviewReadDTO.class);
        Assert.assertEquals(read, actualReview);
    }

    @Test
    public void testDeleteReview() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/reviews/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(reviewService).deleteReview(id);
    }

    @Test
    public void testDeleteReviews() throws Exception {
        UUID id = UUID.randomUUID();
        UUID filmId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{filmsId}/reviews/{id}", filmId, id))
                .andExpect(status().isOk());

        Mockito.verify(reviewService).deleteFilmsReviews(filmId, id);
    }

    private ReviewReadDTO createReviewReadDTO() {
        ReviewReadDTO read = generateObject(ReviewReadDTO.class);
        return read;
    }

}
