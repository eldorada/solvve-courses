package com.solvve.controller;

import com.solvve.domain.ApplicationUser;
import com.solvve.dto.ApplicationUserCreateDTO;
import com.solvve.dto.ApplicationUserPatchDTO;
import com.solvve.dto.ApplicationUserPutDTO;
import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ApplicationUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ApplicationUserController.class)
public class ApplicationUserControllerTest extends BaseControllerTest {

    @MockBean
    private ApplicationUserService applicationUserService;

    @Test
    public void testGetUser() throws Exception {
        ApplicationUserReadDTO applicationUser = createApplicationUserRead();

        Mockito.when(applicationUserService.getApplicationUser(applicationUser.getId())).thenReturn(applicationUser);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", applicationUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ApplicationUserReadDTO actualUser = objectMapper.readValue(resultJson, ApplicationUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(applicationUser);

        Mockito.verify(applicationUserService).getApplicationUser(applicationUser.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(ApplicationUser.class, wrongId);
        Mockito.when(applicationUserService.getApplicationUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        ApplicationUserCreateDTO create = new ApplicationUserCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);

        ApplicationUserReadDTO read = createApplicationUserRead();

        Mockito.when(applicationUserService.createApplicationUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ApplicationUserReadDTO actualUser = objectMapper.readValue(resultJson, ApplicationUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchApplicationUser() throws Exception {
        ApplicationUserPatchDTO patchDTO = new ApplicationUserPatchDTO();
        patchDTO.setName("Vasya");
        patchDTO.setLogin("Vasya1993");
        patchDTO.setRating(7.1);

        ApplicationUserReadDTO read = createApplicationUserRead();

        Mockito.when(applicationUserService.patchApplicationUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ApplicationUserReadDTO actualUser = objectMapper.readValue(resultJson, ApplicationUserReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateApplicationUser() throws Exception {
        ApplicationUserPutDTO putDTO = new ApplicationUserPutDTO();
        putDTO.setName("Vasya");
        putDTO.setLogin("Vasya1993");
        putDTO.setRating(7.1);

        ApplicationUserReadDTO read = createApplicationUserRead();

        Mockito.when(applicationUserService.updateApplicationUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ApplicationUserReadDTO actualUser = objectMapper.readValue(resultJson, ApplicationUserReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteApplicationUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(applicationUserService).deleteApplicationUser(id);
    }

    private ApplicationUserReadDTO createApplicationUserRead() {
        ApplicationUserReadDTO read = generateObject(ApplicationUserReadDTO.class);
        return read;
    }

}
