package com.solvve.controller.integration;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.ApplicationUserCreateDTO;
import com.solvve.dto.ApplicationUserPatchDTO;
import com.solvve.dto.ApplicationUserReadDTO;
import com.solvve.dto.UserRoleReadDTO;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.UserRoleRepository;
import com.solvve.service.UserRoleService;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class WorkingScenarioIntegrationTest extends BaseTest {
    
    private static final String ADMIN_EMAIL = "admin@gmail.com";
    private static final String ADMIN_PASSWORD = "admin123";
    private static final String MANAGER_EMAIL = "manager@gmail.com";
    private static final String MANAGER_PASSWORD = "manager123";
    private static final String MODERATOR_EMAIL = "moderator@gmail.com";
    private static final String MODERATOR_PASSWORD = "moderator123";
    private static final String U1_EMAIL = "user1@gmail.com";
    private static final String U1_PASSWORD = "user23";
    private static final String U2_EMAIL = "user2@gmail.com";
    private static final String U2_PASSWORD = "user13";
    private static final String U3_EMAIL = "user3@gmail.com";
    private static final String U3_PASSWORD = "user12";
    private static final String URL = "http://localhost:8080/api/v1/";

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void finalTest() {

        //FINAL_0
        ApplicationUser admin = new ApplicationUser();
        admin.setEmail(ADMIN_EMAIL);
        admin.setEncodedPassword(passwordEncoder.encode(ADMIN_PASSWORD));
        applicationUserRepository.save(admin);
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.ADMIN);
        userRoleService.addUserRoleToApplicationUser(admin.getId(), userRoleId);

        // FINAL_1
        ApplicationUserCreateDTO m1 = createUserDTO("Vasil", "Vasya",
                MODERATOR_EMAIL, MODERATOR_PASSWORD);

        ApplicationUserReadDTO m1Read = doRequest("users",
                HttpMethod.POST,
                m1,
                null, null,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(m1).isEqualToIgnoringGivenFields(m1Read, "id");

        // FINAL_2
        UUID moderatorId = getUserRoleIdByType(UserRoleType.MODERATOR);

        List<UserRoleReadDTO> m1RoleRead = doRequestForListResult("users/" + m1Read.getId()
                        + "/userRoles/" + moderatorId,
                HttpMethod.POST,
                null,
                null, null,
                new ParameterizedTypeReference<List<UserRoleReadDTO>>() {});

        Assertions.assertThat(m1RoleRead)
                .extracting(UserRoleReadDTO::getId).contains(moderatorId);

        // FINAL_3
        ApplicationUserCreateDTO c1 = createUserDTO("Petro", "Petunya",
                MANAGER_EMAIL, MANAGER_PASSWORD);

        ApplicationUserReadDTO c1Read = doRequest("users",
                HttpMethod.POST,
                c1,
                null, null,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(c1).isEqualToIgnoringGivenFields(c1Read, "id");

        // FINAL_4
        UUID managerId = getUserRoleIdByType(UserRoleType.MANAGER);

        List<UserRoleReadDTO> c1RoleRead = doRequestForListResult("users/" + c1Read.getId()
                        + "/userRoles/" + managerId,
                HttpMethod.POST,
                null,
                null, null,
                new ParameterizedTypeReference<List<UserRoleReadDTO>>() {});

        Assertions.assertThat(c1RoleRead)
                .extracting(UserRoleReadDTO::getId).contains(managerId);

        // FINAL_5
        ApplicationUserCreateDTO u1 = createUserDTO("Maksim", "Maks",
                U1_EMAIL, U1_PASSWORD);

        ApplicationUserReadDTO u1Read = doRequest("users",
                HttpMethod.POST,
                u1,
                null, null,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(u1).isEqualToIgnoringGivenFields(u1Read, "id");

        ApplicationUserCreateDTO u2 = createUserDTO("Lev", "Lion",
                U2_EMAIL, U2_PASSWORD);

        ApplicationUserReadDTO u2Read = doRequest("users",
                HttpMethod.POST,
                u2,
                null, null,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(u2).isEqualToIgnoringGivenFields(u2Read, "id");

        ApplicationUserCreateDTO u3 = createUserDTO("Margarita", "Margo",
                U3_EMAIL, U3_PASSWORD);

        ApplicationUserReadDTO u3Read = doRequest("users",
                HttpMethod.POST,
                u3,
                null, null,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(u3).isEqualToIgnoringGivenFields(u3Read, "id");

        // FINAL_6
        ApplicationUserPatchDTO u2Patch = new ApplicationUserPatchDTO();
        u2Patch.setName("Leo");

        ApplicationUserReadDTO u2ReadAfterRename = doRequest("users/" + u2Read.getId(),
                HttpMethod.PUT,
                u2Patch,
                U2_EMAIL, U2_PASSWORD,
                ApplicationUserReadDTO.class);
        Assertions.assertThat(u2Patch.getName()).isEqualTo(u2ReadAfterRename.getName());
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", userName, password).getBytes()));
    }

    private <E> E doRequest(String path, HttpMethod httpMethod, Object srcObject,
                           String userName, String password, Class<E> entityClass) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<?> httpEntity = HttpEntity.EMPTY;
        HttpHeaders headers = new HttpHeaders();
        if ((userName == null) || (password == null)) {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(ADMIN_EMAIL, ADMIN_PASSWORD));
            httpEntity = new HttpEntity<>(srcObject, headers);
        } else {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(userName, password));
            httpEntity = new HttpEntity<>(srcObject, headers);
        }

        ResponseEntity<E> responseEntity = restTemplate.exchange(URL + path,
                httpMethod, httpEntity,
                entityClass);
        return entityClass.cast(responseEntity.getBody());
    }

    public <E> List<E> doRequestForListResult(String path, HttpMethod httpMethod, Object srcObject,
                           String userName, String password, ParameterizedTypeReference<List<E>> responseType) {
        RestTemplate restTemplate = new RestTemplate();

        HttpEntity<?> httpEntity = HttpEntity.EMPTY;
        HttpHeaders headers = new HttpHeaders();
        if ((userName == null) || (password == null)) {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(ADMIN_EMAIL, ADMIN_PASSWORD));
            httpEntity = new HttpEntity<>(srcObject, headers);
        } else {
            headers.add("Authorization", getBasicAuthorizationHeaderValue(userName, password));
            httpEntity = new HttpEntity<>(srcObject, headers);
        }

        ResponseEntity<List<E>> responseEntity = restTemplate.exchange(URL + path,
                httpMethod, httpEntity,
                responseType);

        return responseEntity.getBody();

    }

    private UUID getUserRoleIdByType(UserRoleType userRoleType) {

        List<UserRoleReadDTO> userRoleReadDTO = doRequestForListResult("userRoles",
                HttpMethod.GET,
                null,
                null, null, new ParameterizedTypeReference<List<UserRoleReadDTO>>() {});

        UUID roleId = null;

        for (UserRoleReadDTO ur : userRoleReadDTO) {
            if (ur.getType().equals(userRoleType)) {
                roleId = ur.getId();
            }
        }

        return roleId;
    }

    private ApplicationUserCreateDTO createUserDTO(String name, String login, String email, String password) {

        ApplicationUserCreateDTO user = new ApplicationUserCreateDTO();
        user.setName(name);
        user.setLogin(login);
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));

        return user;

    }
}
