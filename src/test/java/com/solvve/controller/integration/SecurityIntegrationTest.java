package com.solvve.controller.integration;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import com.solvve.domain.UserRoleType;
import com.solvve.dto.PageResult;
import com.solvve.dto.ViewReadDTO;
import com.solvve.exception.AccessDeniedException;
import com.solvve.repository.ApplicationUserRepository;
import com.solvve.repository.UserRoleRepository;
import com.solvve.service.UserRoleService;
import org.assertj.core.api.Assertions;
import org.bouncycastle.util.encoders.Base64;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.UUID;

@ActiveProfiles({"test", "integration-test"})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class SecurityIntegrationTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRoleService userRoleService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    public void testHealthNoSecurity() {
        RestTemplate restTemplate =new RestTemplate();
        ResponseEntity<Void> response = restTemplate.getForEntity("http://localhost:8080/health", Void.class);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }

    @Test
    public void testGetViewNoSecurity() {
        RestTemplate restTemplate = new RestTemplate();
        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/views", HttpMethod.GET, HttpEntity.EMPTY,
                new ParameterizedTypeReference<PageResult<ViewReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetViews() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setEmail(email);
        applicationUser.setEncodedPassword(passwordEncoder.encode(password));
        applicationUserRepository.save(applicationUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<ViewReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/views", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<ViewReadDTO>>() {
                }
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        //System.out.println(response.getHeaders());
    }

    @Test
    public void testGetWrongPassword() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setEmail(email);
        applicationUser.setEncodedPassword(passwordEncoder.encode(password));
        applicationUserRepository.save(applicationUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, "wrong password"));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/views", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<ViewReadDTO>>() {
                }
        )).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testGetWrongUser() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setEmail(email);
        applicationUser.setEncodedPassword(passwordEncoder.encode(password));
        applicationUserRepository.save(applicationUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue("wrong password", password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/views", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<PageResult<ViewReadDTO>>() {
                }
        )).isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testViewNoSession() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setEmail(email);
        applicationUser.setEncodedPassword(passwordEncoder.encode(password));
        applicationUserRepository.save(applicationUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<PageResult<ViewReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/views", HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                }
        );
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

        Assert.assertNull(response.getHeaders().get("Set-Cookie"));

    }

    @Test
    public void testGetManagerViewsNoRoles() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = new ApplicationUser();
        applicationUser.setEmail(email);
        applicationUser.setEncodedPassword(passwordEncoder.encode(password));
        applicationUserRepository.save(applicationUser);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/managers/" + applicationUser.getId() + "/views",
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<ViewReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetManagerViewsAdmin() {
        String email = "test@gmail.com";
        String password = "pass123";

        ApplicationUser applicationUser = createUser(email, password);
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.ADMIN);
        userRoleService.addUserRoleToApplicationUser(applicationUser.getId(), userRoleId);

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(email, password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        ResponseEntity<List<ViewReadDTO>> response = restTemplate.exchange(
                "http://localhost:8080/api/v1/managers/" + applicationUser.getId() + "/views",
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<>() {
                });

        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());

    }

    @Test
    public void testGetManagerViewsWrongManager() {
        String password = "pass123";

        ApplicationUser manager1 = createUser("test1@gmail.com", "password123");
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);
        userRoleService.addUserRoleToApplicationUser(manager1.getId(), userRoleId);

        ApplicationUser manager2 = createUser("test2@gmail.com", password);
        userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.MANAGER);
        userRoleService.addUserRoleToApplicationUser(manager2.getId(), userRoleId);


        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(manager2.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/managers/" + manager1.getId() + "/views",
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<List<ViewReadDTO>>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    @Test
    public void testGetLoggedUserViewsWrongLoggedUser() {
        String password = "pass123";

        ApplicationUser loggedUser1 = createUser("test1@gmail.com", "password123");
        UUID userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.LOGGED_USER);
        userRoleService.addUserRoleToApplicationUser(loggedUser1.getId(), userRoleId);

        ApplicationUser loggedUser2 = createUser("test2@gmail.com", password);
        userRoleId = userRoleRepository.findUserRoleIdByType(UserRoleType.LOGGED_USER);
        userRoleService.addUserRoleToApplicationUser(loggedUser2.getId(), userRoleId);


        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers  = new HttpHeaders();
        headers.add("Authorization", getBasicAuthorizationHeaderValue(loggedUser2.getEmail(), password));
        HttpEntity<?> httpEntity = new HttpEntity<>(headers);

        Assertions.assertThatThrownBy(() -> restTemplate.exchange(
                "http://localhost:8080/api/v1/logged-users/" + loggedUser1.getId(),
                HttpMethod.GET, httpEntity,
                new ParameterizedTypeReference<ViewReadDTO>() {
                }))
                .isInstanceOf(HttpClientErrorException.class).extracting("statusCode")
                .isEqualTo(HttpStatus.FORBIDDEN);
    }

    private String getBasicAuthorizationHeaderValue(String userName, String password) {
        return "Basic " + new String(Base64.encode(String.format("%s:%s", userName, password).getBytes()));
    }

    private ApplicationUser createUser(String email, String password) {

        ApplicationUser user = generateFlatEntityWithoutId(ApplicationUser.class);
        user.setEmail(email);
        user.setEncodedPassword(passwordEncoder.encode(password));

        return applicationUserRepository.save(user);

    }

}
