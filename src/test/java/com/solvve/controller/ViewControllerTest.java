package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.dto.*;
import com.solvve.exception.ControllerValidationException;
import com.solvve.exception.hander.ErrorInfo;
import com.solvve.service.ViewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ViewController.class)
public class ViewControllerTest extends BaseControllerTest{

    @MockBean
    private ViewService viewService;

    @Test
    public void testGetView() throws Exception {
        ViewReadExtendedDTO view = createViewReadExtended();

        Mockito.when(viewService.getView(view.getId())).thenReturn(view);

        String resultJson = mvc.perform(get("/api/v1/views/{id}", view.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ViewReadExtendedDTO actualView = objectMapper.readValue(resultJson, ViewReadExtendedDTO.class);
        Assertions.assertThat(actualView).isEqualToComparingFieldByField(view);

        Mockito.verify(viewService).getView(view.getId());
    }

    @Test
    public void testGetViews() throws Exception {

        ViewFilter viewFilter = new ViewFilter();
        viewFilter.setLoggedUserId(UUID.randomUUID());
        viewFilter.setFilmId(UUID.randomUUID());
        viewFilter.setStatus(ViewStatus.ONLINE);
        viewFilter.setStartAtTo(Instant.parse("2019-12-31T23:30:08Z"));
        viewFilter.setStartAtFrom(Instant.parse("2019-12-31T23:10:08Z"));

        ViewReadDTO read = createViewRead(viewFilter);

        PageResult<ViewReadDTO> resultPage = new PageResult<>();
        resultPage.setData(List.of(read));
        Mockito.when(viewService.getViews(viewFilter, PageRequest.of(0, defaultPageSize))).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/views")
                .param("loggedUserId", viewFilter.getLoggedUserId().toString())
                .param("filmId", viewFilter.getFilmId().toString())
                .param("status", "ONLINE")
                .param("startAtFrom", viewFilter.getStartAtFrom().toString())
                .param("startAtTo", viewFilter.getStartAtTo().toString())
        )
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ViewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testCreateViewValidationFailed() throws Exception {
        ViewCreateDTO viewCreate = new ViewCreateDTO();
        String resultJson = mvc.perform(post("/api/v1/views")
                .content(objectMapper.writeValueAsString(viewCreate))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        objectMapper.readValue(resultJson, ErrorInfo.class);
        Mockito.verify(viewService, Mockito.never()).createView(ArgumentMatchers.any());
    }
    
    @Test
    public void testCreateViewWrongDates() throws Exception {
        ViewCreateDTO viewCreate = new ViewCreateDTO();
        viewCreate.setLoggedUserId(UUID.randomUUID());
        viewCreate.setFilmId(UUID.randomUUID());
        viewCreate.setStartAt(LocalDateTime.of(2020, 1, 20, 0, 0)
                .toInstant(ZoneOffset.UTC));
        viewCreate.setFinishAt(viewCreate.getStartAt().minus(30, ChronoUnit.MINUTES));
        
        String resultJson = mvc.perform(post("/api/v1/views")
                .content(objectMapper.writeValueAsString(viewCreate))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest()).andReturn().getResponse().getContentAsString();

        ErrorInfo errorInfo = objectMapper.readValue(resultJson, ErrorInfo.class);

        Assert.assertTrue(errorInfo.getMessage().contains("startAt"));
        Assert.assertTrue(errorInfo.getMessage().contains("finishAt"));
        Assert.assertEquals(ControllerValidationException.class, errorInfo.getExceptionClass());

        Mockito.verify(viewService, Mockito.never()).createView(ArgumentMatchers.any());

    }

    @Test
    public void testGetViewsWithPagingAndSorting() throws Exception {
        ViewFilter viewFilter = new ViewFilter();
        ViewReadDTO read =  createViewRead(viewFilter);

        int page = 1;
        int size = 25;

        PageResult<ViewReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements((long) 100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(Sort.Direction.DESC, "StartAt"));
        Mockito.when(viewService.getViews(viewFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/views")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size))
                .param("sort", "StartAt,desc"))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ViewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    @Test
    public void testGetViewsWithBigPage() throws Exception {
        ViewFilter viewFilter = new ViewFilter();
        ViewReadDTO read =  createViewRead(viewFilter);

        int page = 1;
        int size = 99999;

        PageResult<ViewReadDTO> resultPage = new PageResult<>();
        resultPage.setPage(page);
        resultPage.setPageSize(size);
        resultPage.setTotalElements((long) 100);
        resultPage.setTotalPages(4);
        resultPage.setData(List.of(read));

        PageRequest pageRequest = PageRequest.of(page, maxPageSize);
        Mockito.when(viewService.getViews(viewFilter, pageRequest)).thenReturn(resultPage);

        String resultJson = mvc.perform(get("/api/v1/views")
                .param("page", Integer.toString(page))
                .param("size", Integer.toString(size)))
                .andExpect(status().isOk()).andReturn().getResponse().getContentAsString();

        PageResult<ViewReadDTO> actualPage = objectMapper.readValue(resultJson, new TypeReference<>() {
        });
        Assert.assertEquals(resultPage, actualPage);
    }

    private ViewReadDTO createViewRead(ViewFilter viewFilter) {
        ViewReadDTO read = new ViewReadDTO();
        read.setLoggedUserId(viewFilter.getLoggedUserId());
        read.setFilmId(viewFilter.getFilmId());
        read.setStatus(ViewStatus.ONLINE);
        read.setId(UUID.randomUUID());
        read.setStartAt(Instant.parse("2019-12-31T23:10:08Z"));
        read.setFinishAt(Instant.parse("2019-12-31T23:10:08Z"));
        return read;
    }

    private ViewReadExtendedDTO createViewReadExtended() {
        ViewReadExtendedDTO read = generateObject(ViewReadExtendedDTO.class);
        return read;
    }

}
