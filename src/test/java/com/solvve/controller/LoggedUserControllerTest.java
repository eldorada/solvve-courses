package com.solvve.controller;

import com.solvve.domain.LoggedUser;
import com.solvve.dto.LoggedUserCreateDTO;
import com.solvve.dto.LoggedUserPatchDTO;
import com.solvve.dto.LoggedUserPutDTO;
import com.solvve.dto.LoggedUserReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.LoggedUserService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LoggedUserController.class)
public class LoggedUserControllerTest extends BaseControllerTest{

    @MockBean
    private LoggedUserService loggedUserService;

    @Test
    public void testGetUser() throws Exception {
        LoggedUserReadDTO loggedUser = createLoggedUserRead();

        Mockito.when(loggedUserService.getLoggedUser(loggedUser.getId())).thenReturn(loggedUser);

        String resultJson = mvc.perform(get("/api/v1/logged-users/{id}", loggedUser.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LoggedUserReadDTO actualUser = objectMapper.readValue(resultJson, LoggedUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(loggedUser);

        Mockito.verify(loggedUserService).getLoggedUser(loggedUser.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(LoggedUser.class, wrongId);
        Mockito.when(loggedUserService.getLoggedUser(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/logged-users/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/logged-users/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        LoggedUserCreateDTO create = new LoggedUserCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);

        LoggedUserReadDTO read = createLoggedUserRead();

        Mockito.when(loggedUserService.createLoggedUser(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/logged-users")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LoggedUserReadDTO actualUser = objectMapper.readValue(resultJson, LoggedUserReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchLoggedUser() throws Exception {
        LoggedUserPatchDTO patchDTO = new LoggedUserPatchDTO();
        patchDTO.setName("Vasya");
        patchDTO.setLogin("Vasya1993");
        patchDTO.setRating(7.1);

        LoggedUserReadDTO read = createLoggedUserRead();

        Mockito.when(loggedUserService.patchLoggedUser(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/logged-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LoggedUserReadDTO actualUser = objectMapper.readValue(resultJson, LoggedUserReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateLoggedUser() throws Exception {
        LoggedUserPutDTO putDTO = new LoggedUserPutDTO();
        putDTO.setName("Vasya");
        putDTO.setLogin("Vasya1993");
        putDTO.setRating(7.1);

        LoggedUserReadDTO read = createLoggedUserRead();

        Mockito.when(loggedUserService.updateLoggedUser(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/logged-users/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LoggedUserReadDTO actualUser = objectMapper.readValue(resultJson, LoggedUserReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteLoggedUser() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/logged-users/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(loggedUserService).deleteLoggedUser(id);
    }

    private LoggedUserReadDTO createLoggedUserRead() {
        LoggedUserReadDTO read = generateObject(LoggedUserReadDTO.class);
        return read;
    }

}
