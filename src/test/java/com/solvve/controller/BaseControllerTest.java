package com.solvve.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.solvve.security.UserDetailsServiceImpl;
import org.bitbucket.brunneng.br.RandomObjectGenerator;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
public abstract class BaseControllerTest {

    @Value("${spring.data.web.pageable.default-page-size}")
    protected int defaultPageSize;

    @Value("${spring.data.web.pageable.max-page-size}")
    protected int maxPageSize;

    @Autowired
    protected MockMvc mvc;

    @Autowired
    protected ObjectMapper objectMapper;

    @MockBean
    private UserDetailsServiceImpl userDetailsService;

    private RandomObjectGenerator generator = new RandomObjectGenerator();

    protected <T> T generateObject(Class<T> entityClass) {
        T entity = generator.generateRandomObject(entityClass);
        return entity;
    }

}
