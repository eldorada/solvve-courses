package com.solvve.controller;

import com.solvve.domain.LikedObjectType;
import com.solvve.dto.*;
import com.solvve.service.LikeService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(LikeController.class)
public class LikeControllerTest extends BaseControllerTest{

    @MockBean
    private LikeService likeService;

    @Test
    public void testGetLike() throws Exception {
        LikeReadDTO like = createLikeReadDTO();

        Mockito.when(likeService.getLike(like.getId())).thenReturn(like);

        String resultJson = mvc.perform(get("/api/v1/likes/{id}", like.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReadDTO actualLike = objectMapper.readValue(resultJson, LikeReadDTO.class);
        Assertions.assertThat(actualLike).isEqualToComparingFieldByField(like);

        Mockito.verify(likeService).getLike(like.getId());
    }

    @Test
    public void testCreateLike() throws Exception {
        LikeCreateDTO create = new LikeCreateDTO();
        create.setLike(false);
        create.setType(LikedObjectType.FILM);
        create.setLikedObjectId(new FilmReadDTO().getId());

        LikeReadDTO read = createLikeReadDTO();

        Mockito.when(likeService.createLike(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/likes")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReadDTO actualLike = objectMapper.readValue(resultJson, LikeReadDTO.class);
        Assertions.assertThat(actualLike).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchLike() throws Exception {
        LikePatchDTO patch = new LikePatchDTO();
        patch.setLike(false);
        patch.setType(LikedObjectType.FILM);
        patch.setLikedObjectId(new FilmReadDTO().getId());
        LikeReadDTO read = createLikeReadDTO();

        Mockito.when(likeService.patchLike(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/likes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReadDTO actualLike = objectMapper.readValue(resultJson, LikeReadDTO.class);
        Assert.assertEquals(read, actualLike);
    }

    @Test
    public void testPutLike() throws Exception {
        LikePutDTO put = new LikePutDTO();
        put.setLike(false);
        put.setType(LikedObjectType.FILM);
        put.setLikedObjectId(new FilmReadDTO().getId());

        LikeReadDTO read = createLikeReadDTO();

        Mockito.when(likeService.updateLike(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/likes/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        LikeReadDTO actualLike = objectMapper.readValue(resultJson, LikeReadDTO.class);
        Assert.assertEquals(read, actualLike);
    }

    @Test
    public void testDeleteLike() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/likes/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(likeService).deleteLike(id);
    }

    private LikeReadDTO createLikeReadDTO() {
        LikeReadDTO read = generateObject(LikeReadDTO.class);
        return read;
    }

}
