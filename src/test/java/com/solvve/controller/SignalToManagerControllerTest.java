package com.solvve.controller;

import com.solvve.domain.SignalToManagerStatus;
import com.solvve.domain.SignalToManagerType;
import com.solvve.dto.SignalToManagerCreateDTO;
import com.solvve.dto.SignalToManagerReadDTO;
import com.solvve.exception.EntityWrongStatusException;
import com.solvve.service.SignalToManagerService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(SignalToManagerController.class)
public class SignalToManagerControllerTest extends BaseControllerTest {

    @MockBean
    private SignalToManagerService signalToManagerService;

    @Test
    public void testGetSignalToManager() throws Exception {
        SignalToManagerReadDTO signalToManager = createSignalToManagerReadDTO();

        Mockito.when(signalToManagerService.getSignalToManager(signalToManager.getFixObjectId(),
                signalToManager.getId())).thenReturn(signalToManager);

        String resultJson = mvc.perform(get("/api/v1/objects/{fixObjectId}/signalsToManager/{id}",
                signalToManager.getFixObjectId(), signalToManager.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToManagerReadDTO actualSignalToManager = objectMapper.readValue(resultJson, SignalToManagerReadDTO.class);
        Assertions.assertThat(actualSignalToManager).isEqualToComparingFieldByField(signalToManager);

        Mockito.verify(signalToManagerService).getSignalToManager(signalToManager.getFixObjectId(),
                signalToManager.getId());
    }

    @Test
    public void testCreateSignalToManager() throws Exception {
        SignalToManagerCreateDTO create = new SignalToManagerCreateDTO();
        create.setLoggedUserId(UUID.randomUUID());
        create.setManagerId(UUID.randomUUID());
        create.setCorrection(true);
        create.setCorrectionText("Vasy");
        create.setRemarkText("Vas");

        create.setFixObjectId(UUID.randomUUID());
        create.setType(SignalToManagerType.REVIEW);
        create.setRemarkTextStart(1);
        create.setRemarkTextEnd(5);

        SignalToManagerReadDTO read = createSignalToManagerReadDTO();

        Mockito.when(signalToManagerService.createSignalToManager(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/signalsToManager")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        SignalToManagerReadDTO actualSignalToManager = objectMapper.readValue(resultJson, SignalToManagerReadDTO.class);
        Assertions.assertThat(actualSignalToManager).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testFixSignalToManager() throws Exception {
        UUID id = UUID.randomUUID();
        UUID fixObjectId = UUID.randomUUID();

        mvc.perform(put("/api/v1/objects/{fixObjectId}/signalsToManager/{id}",
                fixObjectId, id)).andExpect(status().isOk());

        Mockito.verify(signalToManagerService).fixSignalToManager(fixObjectId, id);

    }

    @Ignore
    @Test
    public void testFixSignalToManagerWithWrongStatus() throws Exception {

        SignalToManagerReadDTO signalToManager = createSignalToManagerReadDTO();

        EntityWrongStatusException exception = new EntityWrongStatusException("Test");
        
        Mockito.doThrow(exception).when(signalToManagerService)
                .fixSignalToManager(signalToManager.getFixObjectId(), signalToManager.getId());
        
        String resultJson = mvc.perform(put("/api/v1/objects/{fixObjectId}/signalsToManager/{id}",
                signalToManager.getFixObjectId(), signalToManager.getId()))
                .andExpect(status().isUnprocessableEntity())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testDeleteSignalToManager() throws Exception {
        UUID id = UUID.randomUUID();
        UUID fixObjectId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/objects/{fixObjectId}/signalsToManager/{id}",
                fixObjectId, id)).andExpect(status().isOk());
        Mockito.verify(signalToManagerService).deleteSignalToManager(fixObjectId, id);
    }

    private SignalToManagerReadDTO createSignalToManagerReadDTO() {
        SignalToManagerReadDTO read = generateObject(SignalToManagerReadDTO.class);
        return read;
    }

}
