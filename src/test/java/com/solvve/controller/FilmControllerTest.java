package com.solvve.controller;

import com.solvve.dto.FilmCreateDTO;
import com.solvve.dto.FilmPatchDTO;
import com.solvve.dto.FilmPutDTO;
import com.solvve.dto.FilmReadDTO;
import com.solvve.service.FilmService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.time.Instant;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(FilmController.class)
public class FilmControllerTest extends BaseControllerTest{

    @MockBean
    private FilmService filmService;

    @Test
    public void testGetFilm() throws Exception {
        FilmReadDTO film = createFilmReadDTO();

        Mockito.when(filmService.getFilm(film.getId())).thenReturn(film);

        String resultJson = mvc.perform(get("/api/v1/films/{id}", film.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(film);

        Mockito.verify(filmService).getFilm(film.getId());
    }

    @Test
    public void testCreateFilm() throws Exception {
        FilmCreateDTO create = new FilmCreateDTO();
        create.setTitle("Lola");
        create.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        create.setStatus(true);

        FilmReadDTO read = createFilmReadDTO();

        Mockito.when(filmService.createFilm(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assertions.assertThat(actualFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilm() throws Exception {
        FilmPatchDTO patch = new FilmPatchDTO();
        patch.setTitle("Lola");
        patch.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        patch.setStatus(true);

        FilmReadDTO read = createFilmReadDTO();

        Mockito.when(filmService.patchFilm(read.getId(), patch)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assert.assertEquals(read, actualFilm);
    }

    @Test
    public void testPutFilm() throws Exception {
        FilmPutDTO put = new FilmPutDTO();
        put.setTitle("Lola");
        put.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        put.setStatus(true);

        FilmReadDTO read = createFilmReadDTO();

        Mockito.when(filmService.updateFilm(read.getId(), put)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        FilmReadDTO actualFilm = objectMapper.readValue(resultJson, FilmReadDTO.class);
        Assert.assertEquals(read, actualFilm);
    }

    @Test
    public void testDeleteFilm() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(filmService).deleteFilm(id);
    }

    private FilmReadDTO createFilmReadDTO() {
        FilmReadDTO read = generateObject(FilmReadDTO.class);
        return read;
    }

}
