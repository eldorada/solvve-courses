package com.solvve.controller;

import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import com.solvve.dto.ActorCreateDTO;
import com.solvve.dto.ActorPatchDTO;
import com.solvve.dto.ActorPutDTO;
import com.solvve.dto.ActorReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.ActorService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ActorController.class)
public class ActorControllerTest extends BaseControllerTest {

    @MockBean
    private ActorService actorService;

    @Test
    public void testGetUser() throws Exception {
        ActorReadDTO actor = createActorRead();

        Mockito.when(actorService.getActor(actor.getId())).thenReturn(actor);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", actor.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualUser = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(actor);

        Mockito.verify(actorService).getActor(actor.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(Actor.class, wrongId);
        Mockito.when(actorService.getActor(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/actors/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/actors/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        ActorCreateDTO create = new ActorCreateDTO();
        create.setAverageMark(3.2);

        ActorReadDTO read = createActorRead();

        Mockito.when(actorService.createPersonsActor(person.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/actors/persons/{id}", person.getId().toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualUser = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchActor() throws Exception {
        ActorPatchDTO patchDTO = new ActorPatchDTO();
        patchDTO.setAverageMark(3.1);

        ActorReadDTO read = createActorRead();

        Mockito.when(actorService.patchActor(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/actors/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualUser = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateActor() throws Exception {
        ActorPutDTO putDTO = new ActorPutDTO();
        putDTO.setAverageMark(2.5);

        ActorReadDTO read = createActorRead();

        Mockito.when(actorService.updateActor(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/actors/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ActorReadDTO actualUser = objectMapper.readValue(resultJson, ActorReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteActor() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/actors/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(actorService).deleteActor(id);
    }

    private ActorReadDTO createActorRead() {
        ActorReadDTO read = generateObject(ActorReadDTO.class);
        return read;
    }

}
