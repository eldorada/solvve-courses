package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Film;
import com.solvve.domain.ParticipantFilmType;
import com.solvve.domain.Person;
import com.solvve.dto.*;
import com.solvve.service.ParticipantFilmService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(ParticipantFilmController.class)
public class ParticipantFilmControllerTest extends BaseControllerTest{

    @MockBean
    private ParticipantFilmService participantFilmService;

    @Test
    public void testGetFilmsParticipantFilms() throws Exception {

        ParticipantFilmReadDTO read = new ParticipantFilmReadDTO();
        read.setFilmId(UUID.randomUUID());
        List<ParticipantFilmReadDTO> expectedResult = List.of(read);

        Mockito.when(participantFilmService.getFilmsParticipantFilms(read.getFilmId())).thenReturn(expectedResult);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/participantFilms", read.getFilmId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<ParticipantFilmReadDTO> actualResult = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetFilmsParticipantFilmsById() throws Exception {

        ParticipantFilmReadDTO read = createParticipantFilmReadDTO();
        read.setFilmId(UUID.randomUUID());

        Mockito.when(participantFilmService.getFilmsParticipantFilms(read.getFilmId(), read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/participantFilms/{id}",
                read.getFilmId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ParticipantFilmReadDTO actualParticipantFilm = objectMapper.readValue(resultJson, ParticipantFilmReadDTO.class);
        Assertions.assertThat(actualParticipantFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreateFilmsParticipantFilms() throws Exception {
        Person person = new Person();
        person.setId(UUID.randomUUID());
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ParticipantFilmCreateDTO create = new ParticipantFilmCreateDTO();

        create.setDescription("frau");
        create.setType(ParticipantFilmType.ACTOR);

        ParticipantFilmReadDTO read = createParticipantFilmReadDTO();

        Mockito.when(participantFilmService.createFilmsParticipantFilms(film.getId(),
                create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films/{filmsId}/participantFilms",
                film.getId().toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON)
                .param("personId", person.getId().toString()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ParticipantFilmReadDTO actualParticipantFilm = objectMapper.readValue(resultJson, ParticipantFilmReadDTO.class);
        Assertions.assertThat(actualParticipantFilm).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilmsParticipantFilms() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ParticipantFilmPatchDTO patch = new ParticipantFilmPatchDTO();
        patch.setDescription("frau");
        patch.setType(ParticipantFilmType.ACTOR);
        patch.setFilmId(film.getId());

        ParticipantFilmReadDTO read = createParticipantFilmReadDTO();

        Mockito.when(participantFilmService.patchFilmsParticipantFilms(film.getId(), read.getId(), patch))
                .thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{filmsId}/participantFilms/{id}",
                film.getId(), read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ParticipantFilmReadDTO actualParticipantFilm = objectMapper.readValue(resultJson, ParticipantFilmReadDTO.class);
        Assert.assertEquals(read, actualParticipantFilm);
    }

    @Test
    public void testPutFilmsParticipantFilms() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        ParticipantFilmPutDTO put = new ParticipantFilmPutDTO();
        put.setDescription("frau");
        put.setType(ParticipantFilmType.ACTOR);
        put.setFilmId(film.getId());

        ParticipantFilmReadDTO read = createParticipantFilmReadDTO();

        Mockito.when(participantFilmService.updateFilmsParticipantFilms(film.getId(), read.getId(), put))
                .thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{filmsId}/participantFilms/{id}",
                film.getId(), read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        ParticipantFilmReadDTO actualParticipantFilm = objectMapper.readValue(resultJson, ParticipantFilmReadDTO.class);
        Assert.assertEquals(read, actualParticipantFilm);
    }

    @Test
    public void testDeleteParticipantFilms() throws Exception {
        UUID id = UUID.randomUUID();
        UUID filmId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{filmsId}/participantFilms/{id}", filmId, id))
                .andExpect(status().isOk());

        Mockito.verify(participantFilmService).deleteFilmsParticipantFilms(filmId, id);
    }

    private ParticipantFilmReadDTO createParticipantFilmReadDTO() {
        ParticipantFilmReadDTO read = generateObject(ParticipantFilmReadDTO.class);
        return read;
    }

}
