package com.solvve.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

@WebMvcTest(controllers = HealthController.class)
public class HealthControllerTest extends BaseControllerTest{

    @Test
    public void testHealth() throws Exception {
        mvc.perform(get("/health")).andExpect(status().isOk());
    }

}
