package com.solvve.controller;

import com.solvve.domain.Admin;
import com.solvve.dto.AdminCreateDTO;
import com.solvve.dto.AdminPatchDTO;
import com.solvve.dto.AdminPutDTO;
import com.solvve.dto.AdminReadDTO;
import com.solvve.exception.EntityNotFoundException;
import com.solvve.service.AdminService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(AdminController.class)
public class AdminControllerTest extends BaseControllerTest{

    @MockBean
    private AdminService adminService;

    @Test
    public void testGetUser() throws Exception {
        AdminReadDTO admin = createAdminRead();

        Mockito.when(adminService.getAdmin(admin.getId())).thenReturn(admin);

        String resultJson = mvc.perform(get("/api/v1/admins/{id}", admin.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualUser = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(admin);

        Mockito.verify(adminService).getAdmin(admin.getId());
    }

    @Test
    public void testGetUserWrongId() throws Exception {
        UUID wrongId = UUID.randomUUID();
        EntityNotFoundException exception = new EntityNotFoundException(Admin.class, wrongId);
        Mockito.when(adminService.getAdmin(wrongId)).thenThrow(exception);

        String resultJson = mvc.perform(get("/api/v1/admins/{id}", wrongId))
                .andExpect(status().isNotFound())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains(exception.getMessage()));
    }

    @Test
    public void testGetUserValidationId() throws Exception {
        String resultJson = mvc.perform(get("/api/v1/admins/123"))
                .andExpect(status().isBadRequest())
                .andReturn().getResponse().getContentAsString();

        Assert.assertTrue(resultJson.contains("status"));
        Assert.assertTrue(resultJson.contains("exceptionClass"));
        Assert.assertTrue(resultJson.contains("message"));

    }

    @Test
    public void testCreateUser() throws Exception {
        AdminCreateDTO create = new AdminCreateDTO();
        create.setName("Vasya");
        create.setLogin("Vasya1993");
        create.setRating(1.0);

        AdminReadDTO read = createAdminRead();

        Mockito.when(adminService.createAdmin(create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/admins")
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualUser = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assertions.assertThat(actualUser).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchAdmin() throws Exception {
        AdminPatchDTO patchDTO = new AdminPatchDTO();
        patchDTO.setName("Vasya");
        patchDTO.setLogin("Vasya1993");
        patchDTO.setRating(7.1);

        AdminReadDTO read = createAdminRead();

        Mockito.when(adminService.patchAdmin(read.getId(), patchDTO)).thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/admins/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(patchDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualUser = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testUpdateAdmin() throws Exception {
        AdminPutDTO putDTO = new AdminPutDTO();
        putDTO.setName("Vasya");
        putDTO.setLogin("Vasya1993");
        putDTO.setRating(7.1);

        AdminReadDTO read = createAdminRead();

        Mockito.when(adminService.updateAdmin(read.getId(), putDTO)).thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/admins/{id}", read.getId().toString())
                .content(objectMapper.writeValueAsString(putDTO))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        AdminReadDTO actualUser = objectMapper.readValue(resultJson, AdminReadDTO.class);
        Assert.assertEquals(read, actualUser);
    }

    @Test
    public void testDeleteAdmin() throws Exception {
        UUID id = UUID.randomUUID();

        mvc.perform(delete("/api/v1/admins/{id}", id.toString())).andExpect(status().isOk());

        Mockito.verify(adminService).deleteAdmin(id);
    }

    private AdminReadDTO createAdminRead() {
        AdminReadDTO read = generateObject(AdminReadDTO.class);
        return read;
    }

}
