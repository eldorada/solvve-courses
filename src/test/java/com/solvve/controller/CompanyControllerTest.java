package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.CompanyType;
import com.solvve.dto.CompanyReadDTO;
import com.solvve.service.CompanyService;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(CompanyController.class)
public class CompanyControllerTest extends BaseControllerTest{

    @MockBean
    private CompanyService companyService;

    @Test
    public void testAddCompanyToFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        CompanyReadDTO read = new CompanyReadDTO();
        read.setId(companyId);
        read.setType(CompanyType.DISNEY);
        List<CompanyReadDTO> expectedCompanies = List.of(read);
        Mockito.when(companyService.addCompanyToFilm(filmId, companyId)).thenReturn(expectedCompanies);

        String resultJson = mvc.perform(post("/api/v1/films/{filmId}/companies/{id}", filmId, companyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CompanyReadDTO> actualCompanies = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedCompanies, actualCompanies);
    }

    @Test
    public void testRemoveCompanyFromFilm() throws Exception {
        UUID filmId = UUID.randomUUID();
        UUID companyId = UUID.randomUUID();

        CompanyReadDTO read = new CompanyReadDTO();
        read.setId(companyId);
        read.setType(CompanyType.BRAZZERS);
        List<CompanyReadDTO> expectedCompanies = List.of(read);
        Mockito.when(companyService.removeCompanyFromFilm(filmId, companyId)).thenReturn(expectedCompanies);

        String resultJson = mvc.perform(delete("/api/v1/films/{filmId}/companies/{id}", filmId, companyId))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<CompanyReadDTO> actualCompanies = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedCompanies, actualCompanies);
    }
}
