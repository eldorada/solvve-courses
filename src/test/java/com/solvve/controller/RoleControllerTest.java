package com.solvve.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.solvve.domain.Film;
import com.solvve.dto.*;
import com.solvve.service.RoleService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;

import java.util.List;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser
@WebMvcTest(RoleController.class)
public class RoleControllerTest extends BaseControllerTest{

    @MockBean
    private RoleService roleService;

    @Test
    public void testGetFilmsRoles() throws Exception {

        RoleReadDTO read = new RoleReadDTO();
        read.setFilmId(UUID.randomUUID());
        List<RoleReadDTO> expectedResult = List.of(read);

        Mockito.when(roleService.getFilmsRoles(read.getFilmId())).thenReturn(expectedResult);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/roles", read.getFilmId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        List<RoleReadDTO> actualResult = objectMapper.readValue(resultJson, new TypeReference<>() {

        });
        Assert.assertEquals(expectedResult, actualResult);
    }

    @Test
    public void testGetFilmsRolesById() throws Exception {

        RoleReadDTO read = createRoleReadDTO();
        read.setFilmId(UUID.randomUUID());

        Mockito.when(roleService.getFilmsRoles(read.getFilmId(), read.getId())).thenReturn(read);

        String resultJson = mvc.perform(get("/api/v1/films/{filmsId}/roles/{id}",
                read.getFilmId(), read.getId()))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testCreateFilmsRoles() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        RoleCreateDTO create = new RoleCreateDTO();

        create.setDescription("frau");
        create.setNameRole("driver");

        RoleReadDTO read = createRoleReadDTO();

        Mockito.when(roleService.createFilmsRoles(film.getId(), create)).thenReturn(read);

        String resultJson = mvc.perform(post("/api/v1/films/{filmsId}/roles",
                film.getId().toString())
                .content(objectMapper.writeValueAsString(create))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assertions.assertThat(actualRole).isEqualToComparingFieldByField(read);
    }

    @Test
    public void testPatchFilmsRoles() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        RolePatchDTO patch = new RolePatchDTO();
        patch.setDescription("frau");
        patch.setNameRole("driver");
        patch.setFilmId(film.getId());

        RoleReadDTO read = createRoleReadDTO();

        Mockito.when(roleService.patchFilmsRoles(film.getId(), read.getId(), patch))
                .thenReturn(read);

        String resultJson = mvc.perform(patch("/api/v1/films/{filmsId}/roles/{id}",
                film.getId(), read.getId())
                .content(objectMapper.writeValueAsString(patch))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @Test
    public void testPutFilmsRoles() throws Exception {
        Film film = new Film();
        film.setId(UUID.randomUUID());

        RolePutDTO put = new RolePutDTO();
        put.setDescription("frau");
        put.setNameRole("driver");
        put.setFilmId(film.getId());

        RoleReadDTO read = createRoleReadDTO();

        Mockito.when(roleService.updateFilmsRoles(film.getId(), read.getId(), put))
                .thenReturn(read);

        String resultJson = mvc.perform(put("/api/v1/films/{filmsId}/roles/{id}",
                film.getId(), read.getId().toString())
                .content(objectMapper.writeValueAsString(put))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();

        RoleReadDTO actualRole = objectMapper.readValue(resultJson, RoleReadDTO.class);
        Assert.assertEquals(read, actualRole);
    }

    @Test
    public void testDeleteRoles() throws Exception {
        UUID id = UUID.randomUUID();
        UUID filmId = UUID.randomUUID();

        mvc.perform(delete("/api/v1/films/{filmsId}/roles/{id}", filmId, id))
                .andExpect(status().isOk());

        Mockito.verify(roleService).deleteFilmsRoles(filmId, id);
    }

    private RoleReadDTO createRoleReadDTO() {
        RoleReadDTO read = generateObject(RoleReadDTO.class);
        return read;
    }

}
