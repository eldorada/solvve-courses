package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Film;
import com.solvve.domain.Person;
import com.solvve.domain.Role;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class RoleRepositoryTest extends BaseTest {

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void testGetFilmRoles() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f1 = createFilm();
        Film f2 = createFilm();
        Role r1 = createRole(f1, a);
        Role r2 = createRole(f1, a);
        createRole(f2, a);

        List<Role> res = roleRepository.findByFilm(f1);
        Assertions.assertThat(res).extracting(Role::getId).isEqualTo(Arrays.asList(r1.getId(), r2.getId()));
    }

    @Test
    public void testGetFilmRolesById() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f1 = createFilm();
        Film f2 = createFilm();
        Role r1 = createRole(f1, a);
        createRole(f1, a);
        createRole(f2, a);

        Role res = roleRepository.findByFilmAndId(f1, r1.getId());
        Assertions.assertThat(res).extracting(Role::getId).isEqualTo(r1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f = createFilm();
        Role r = createRole(f, a);

        Instant createdAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = roleRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Person p =createPerson();
        Actor a = createActor(p);
        Film f = createFilm();
        Role r = createRole(f, a);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = roleRepository.findById(r.getId()).get();
        r.setDescription("21");
        r = roleRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);

        return filmRepository.save(film);
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);
    }
    private Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actorRepository.save(actor);
    }

    private Role createRole(Film film, Actor actor) {
        Role role = generateFlatEntityWithoutId(Role.class);
        role.setActor(actor);
        role.setFilm(film);
        return roleRepository.save(role);
    }

}
