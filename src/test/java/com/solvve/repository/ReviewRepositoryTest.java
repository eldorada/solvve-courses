package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Review;
import com.solvve.domain.ReviewStatus;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class ReviewRepositoryTest extends BaseTest {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void testGetFilmReviews() {
        Film f1 = createFilm();
        Film f2 = createFilm();
        Review r1 = createReview(f1);
        Review r2 = createReview(f1);
        createReview(f2);

        List<Review> res = reviewRepository.findByFilm(f1);
        Assertions.assertThat(res).extracting(Review::getId).isEqualTo(Arrays.asList(r1.getId(), r2.getId()));
    }

    @Test
    public void testGetFilmReviewsById() {
        Film f1 = createFilm();
        Film f2 = createFilm();
        Review r1 = createReview(f1);
        createReview(f1);
        createReview(f2);

        Review res = reviewRepository.findByFilmAndId(f1, r1.getId());
        Assertions.assertThat(res).extracting(Review::getId).isEqualTo(r1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        Film f = createFilm();
        Review r = createReview(f);

        Instant createdAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = reviewRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Film f = createFilm();
        Review r = createReview(f);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = reviewRepository.findById(r.getId()).get();
        r.setLike(21);
        r = reviewRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    private Review createReview(Film film) {
        Review review = new Review();
        review.setText("It's Nice!");
        review.setDislike(1);
        review.setLike(134);
        review.setStatus(ReviewStatus.ON_CHECK);
        review.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        review.setFilm(film);
        return reviewRepository.save(review);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

}
