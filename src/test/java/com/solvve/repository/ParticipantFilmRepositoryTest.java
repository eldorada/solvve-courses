package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.ParticipantFilm;
import com.solvve.domain.Person;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

public class ParticipantFilmRepositoryTest extends BaseTest {

    @Autowired
    private ParticipantFilmRepository participantFilmRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testGetFilmParticipantFilms() {
        Person p = createPerson();

        Film f1 = createFilm();
        Film f2 = createFilm();

        ParticipantFilm r1 = createParticipantFilm(f1, p);
        ParticipantFilm r2 = createParticipantFilm(f1, p);
        createParticipantFilm(f2, p);

        List<ParticipantFilm> res = participantFilmRepository.findByFilm(f1);
        Assertions.assertThat(res).extracting(ParticipantFilm::getId).isEqualTo(Arrays.asList(r1.getId(), r2.getId()));
    }

    @Test
    public void testGetFilmParticipantFilmsById() {
        Person p = createPerson();

        Film f1 = createFilm();
        Film f2 = createFilm();
        ParticipantFilm r1 = createParticipantFilm(f1, p);
        createParticipantFilm(f1, p);
        createParticipantFilm(f2, p);

        ParticipantFilm res = participantFilmRepository.findByFilmAndId(f1, r1.getId());
        Assertions.assertThat(res).extracting(ParticipantFilm::getId).isEqualTo(r1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        Person p = createPerson();
        Film f = createFilm();
        ParticipantFilm r = createParticipantFilm(f, p);

        Instant createdAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        r = participantFilmRepository.findById(r.getId()).get();

        Instant createdAtAfterReload = r.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Film f = createFilm();
        Person p = createPerson();
        ParticipantFilm r = createParticipantFilm(f, p);

        Instant updatedAtBeforeReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        r = participantFilmRepository.findById(r.getId()).get();
        r.setDescription("21");
        r = participantFilmRepository.save(r);

        Instant updatedAtAfterReload = r.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    private Film createFilm() {
        Film film = generateFlatEntityWithoutId(Film.class);

        return filmRepository.save(film);
    }
    
    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);

        return personRepository.save(person);
    }

    private ParticipantFilm createParticipantFilm(Film film, Person person) {
        ParticipantFilm participantFilm = generateFlatEntityWithoutId(ParticipantFilm.class);

        participantFilm.setFilm(film);
        participantFilm.setPerson(person);

        return participantFilmRepository.save(participantFilm);
        
    }

}
