package com.solvve.repository;

import com.solvve.base.BaseTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

@TestPropertySource(properties = "spring.liquibase.change-log=classpath:db/changelog/db.changelog-master.xml")
public class LiquibaseLoadDataTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private SignalToManagerRepository signalToManagerRepository;

    @Autowired
    private LikeRepository likeRepository;

    @Test
    public void testDataLoaded() {
        Assert.assertTrue(applicationUserRepository.count() > 0);
        Assert.assertTrue(filmRepository.count() > 0);
        Assert.assertTrue(reviewRepository.count() > 0);
        Assert.assertTrue(viewRepository.count() > 0);
        Assert.assertTrue(signalToManagerRepository.count() > 0);
        Assert.assertTrue(likeRepository.count() > 0);

    }

}
