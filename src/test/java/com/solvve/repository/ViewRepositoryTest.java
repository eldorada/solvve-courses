package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.LoggedUser;
import com.solvve.domain.Film;
import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.dto.ViewFilter;
import com.solvve.service.ViewService;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.TransactionSystemException;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.List;

public class ViewRepositoryTest extends BaseTest {

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private ViewService viewService;

    @Test
    public void testGetFilmViews() {
        LoggedUser u = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();
        View v1 = createView(u, f1, ViewStatus.ONLINE);
        createView(u, f1, ViewStatus.OFFLINE);
        View v2 = createView(u, f1, ViewStatus.ONLINE);
        createView(u, f2, ViewStatus.ONLINE);

        List<View> res = viewRepository.findByFilmIdAndStatusOrderByStartAtAsc(f1.getId(), ViewStatus.ONLINE);
        Assertions.assertThat(res).extracting(View::getId).isEqualTo(Arrays.asList(v1.getId(), v2.getId()));
    }

    @Test
    public void testGetFilmViewsInInterval() {
        LoggedUser u = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();
        ZoneOffset utc = ZoneOffset.UTC;
        View v1 = createView(u, f1, ViewStatus.ONLINE,
                LocalDateTime.of(2019, 12, 4, 15, 0, 0).toInstant(utc));
        createView(u, f1, ViewStatus.OFFLINE,
                LocalDateTime.of(2019, 12, 4, 15, 0, 0).toInstant(utc));
        View v2 = createView(u, f1, ViewStatus.ONLINE,
                LocalDateTime.of(2019, 12, 4, 15, 30, 0).toInstant(utc));
        createView(u, f1, ViewStatus.ONLINE,
                LocalDateTime.of(2019, 12, 4, 17, 30, 0).toInstant(utc));
        createView(u, f2, ViewStatus.ONLINE,
                LocalDateTime.of(2019, 12, 4, 15, 0, 0).toInstant(utc));

        List<View> res = viewRepository.findViewForFilmInGivenInterval(f1.getId(), ViewStatus.ONLINE,
                LocalDateTime.of(2019, 12, 4, 15, 0, 0).toInstant(utc),
                LocalDateTime.of(2019, 12, 4, 17, 30, 0).toInstant(utc));
        Assertions.assertThat(res).extracting(View::getId).isEqualTo(Arrays.asList(v1.getId(), v2.getId()));
    }

    public void testGetViewsWithEmptyFilter() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1);
        View v2 = createView(u1, f2);
        View v3 = createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId(), v3.getId());
    }

    @Test
    public void testGetViewsByLoggedUser() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1);
        View v2 = createView(u1, f2);
        createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        filter.setLoggedUserId(u1.getId());
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByFilm() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1);
        View v2 = createView(u1, f2);
        View v1 = createView(u2, f2);

        ViewFilter filter = new ViewFilter();
        filter.setFilmId(f2.getId());
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByStatus() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        View v1 = createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        View v2 = createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setStatus(ViewStatus.ONLINE);
        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByStartAtInterval() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        View v2 = createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setStartAtFrom(createInstant(9));
        filter.setStartAtTo(createInstant(13));

        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId(), v2.getId());
    }

    @Test
    public void testGetViewsByAllFilters() {
        LoggedUser u1 = createUser();
        LoggedUser u2 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        View v1 = createView(u1, f1, ViewStatus.OFFLINE, createInstant(12));
        createView(u1, f2, ViewStatus.ONLINE, createInstant(13));
        createView(u2, f2, ViewStatus.ONLINE, createInstant(9));

        ViewFilter filter = new ViewFilter();
        filter.setLoggedUserId(u1.getId());
        filter.setFilmId(f1.getId());
        filter.setStatus(ViewStatus.OFFLINE);
        filter.setStartAtFrom(createInstant(9));
        filter.setStartAtTo(createInstant(13));

        Assertions.assertThat(viewService.getViews(filter, Pageable.unpaged()).getData()).extracting("id")
                .containsExactlyInAnyOrder(v1.getId());
    }

    @Test
    public void testCreatedAtIsSet() {
        LoggedUser u = createUser();
        Film f = createFilm();
        View v = createView(u, f);

        Instant createdAtBeforeReload = v.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        v = viewRepository.findById(v.getId()).get();

        Instant createdAtAfterReload = v.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);

    }

    @Test
    public void testUpdatedAtIsSet() {
        LoggedUser u = createUser();
        Film f1 = createFilm();
        View v = createView(u, f1);

        Instant updatedAtBeforeReload = v.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        v = viewRepository.findById(v.getId()).get();
        v.setStatus(ViewStatus.OFFLINE);
        v = viewRepository.save(v);

        Instant updatedAtAfterReload = v.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);

    }

    @Test
    public void testCalcAverageMark() {
        LoggedUser u1 = createUser();
        Film f1 = createFilm();
        Film f2 = createFilm();

        createView(u1, f1, 4);
        createView(u1, f1, 5);
        createView(u1, f1, (Integer) null);
        createView(u1, f2, 2);
        Assert.assertEquals(4.5, viewRepository.calcAverageMarkOfFilm(f1.getId()),
                Double.MIN_NORMAL);

    }

    @Test(expected = TransactionSystemException.class)
    public void testSaveViewValidation() {
        View view = new View();
        viewRepository.save(view);
    }

    private View createView(LoggedUser loggedUser, Film film) {
        View view = new View();
        view.setStatus(ViewStatus.ONLINE);
        view.setStartAt(Instant.parse("2019-12-31T23:20:08Z"));
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        view.setLoggedUserMark(4);

        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);

    }

    private View createView(LoggedUser loggedUser, Film film,
                            Integer loggedUserMark) {
        View view = new View();
        view.setStatus(ViewStatus.ONLINE);
        view.setStartAt(Instant.parse("2019-12-31T23:20:08Z"));
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        view.setLoggedUserMark(loggedUserMark);

        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);

    }

    private View createView(LoggedUser loggedUser, Film film, ViewStatus status) {
        View view = new View();
        view.setStatus(status);
        view.setStartAt(Instant.parse("2019-12-31T23:20:08Z"));
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        view.setLoggedUserMark(4);

        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private View createView(LoggedUser loggedUser, Film film, ViewStatus status, Instant startAt) {
        View view = new View();
        view.setStatus(status);
        view.setStartAt(startAt);
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        view.setLoggedUserMark(4);
        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }

    private LoggedUser createUser() {
        LoggedUser user = new LoggedUser();
        user.setName("Vas");
        user.setLogin("Vasya1993");
        user.setRating(1.7);
        return loggedUserRepository.save(user);
    }

    private Instant createInstant(int hour) {
        return LocalDateTime.of(2019, 12, 23, hour, 0).toInstant(ZoneOffset.UTC);
    }

}
