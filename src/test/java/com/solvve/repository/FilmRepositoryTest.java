package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.LoggedUser;
import com.solvve.domain.View;
import com.solvve.domain.ViewStatus;
import com.solvve.dto.FilmInLeaderBoardReadDTO;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionTemplate;

import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

public class FilmRepositoryTest extends BaseTest {

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    private LoggedUserRepository loggedUserRepository;

    @Autowired
    private ViewRepository viewRepository;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Test
    public void testCreatedAtIsSet() {
        Film film = createFilm();

        Instant createdAtBeforeReload = film.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        film = filmRepository.findById(film.getId()).get();

        Instant createdAtAfterReload = film.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Film film = createFilm();
        Instant updatedAtBeforeReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        film = filmRepository.findById(film.getId()).get();
        film.setTitle("Lola F");
        film = filmRepository.save(film);

        Instant updatedAtAfterReload = film.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    @Test
    public void testGetIdsOfFilms() {
        Set<UUID> expectedIdsOfFilms = new HashSet<>();
        expectedIdsOfFilms.add(createFilm().getId());
        expectedIdsOfFilms.add(createFilm().getId());

        transactionTemplate.executeWithoutResult(status -> {
            Assert.assertEquals(expectedIdsOfFilms, filmRepository.getIdsOfFilms().collect(Collectors.toSet()));
        });

    }
    
    @Test
    public void testGetFilmsLeaderBoard() {
        LoggedUser u =createUser();

        int filmCount = 5;

        Set<UUID> filmIds = new HashSet<>();

        for (int i = 0; i < filmCount; ++i) {
            Film f = createFilm();
            filmIds.add(f.getId());

            createView(u, f, true);
            createView(u, f, true);
            createView(u, f, false);

        }

        List<FilmInLeaderBoardReadDTO> filmsLeaderBoard = filmRepository.getFilmsLeaderBoard();
        Assertions.assertThat(filmsLeaderBoard).isSortedAccordingTo(Comparator
                .comparing(FilmInLeaderBoardReadDTO::getAverageMark).reversed());

        Assert.assertEquals(filmIds, filmsLeaderBoard.stream().map(FilmInLeaderBoardReadDTO::getId)
                .collect(Collectors.toSet()));

        for (FilmInLeaderBoardReadDTO f : filmsLeaderBoard) {
            Assert.assertNotNull(f.getTitle());
            Assert.assertNotNull(f.getAverageMark());
            Assert.assertEquals(2, f.getViewCount().longValue() );
        }
    }

    private View createView(LoggedUser loggedUser, Film film, Boolean withMark) {
        View view = new View();
        view.setStatus(ViewStatus.ONLINE);
        view.setStartAt(Instant.parse("2019-12-31T23:20:08Z"));
        view.setFinishAt(Instant.parse("2019-12-31T23:21:08Z"));
        if (withMark) {
            view.setLoggedUserMark(4);
        }

        view.setLoggedUser(loggedUser);
        view.setFilm(film);
        return viewRepository.save(view);

    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        film.setAverageMark(10.0);
        return filmRepository.save(film);
    }

    private LoggedUser createUser() {
        LoggedUser user = new LoggedUser();
        return loggedUserRepository.save(user);
    }
}
