package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Review;
import com.solvve.domain.ReviewStatus;
import com.solvve.exception.EntityNotFoundException;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

public class RepositoryHelperTest extends BaseTest {

    @Autowired
    private RepositoryHelper repositoryHelper;

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private FilmRepository filmRepository;

    @Test
    public void testGetReferenceIfExist() {

        Film f = createFilm();
        Review r = createReview(f);

        Assertions.assertThat(repositoryHelper.getReferenceIfExist(Film.class, f.getId())).extracting("id")
                .isEqualTo(r.getFilm().getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void testGetReferenceIfExistWrongId() {
        Film notExist = repositoryHelper.getReferenceIfExist(Film.class, UUID.randomUUID());
    }

    @Test
    public void testGetFilmsRequired() {
        Film film = createFilm();

        Assertions.assertThat(repositoryHelper.getReferenceIfExist(Film.class, film.getId())).extracting("id")
                .isEqualTo(film.getId());
    }

    private Review createReview(Film film) {
        Review review = new Review();
        review.setText("It's Nice!");
        review.setDislike(1);
        review.setLike(134);
        review.setStatus(ReviewStatus.ON_CHECK);
        review.setModifyDate(Instant.parse("2019-12-31T23:20:08Z"));
        review.setFilm(film);
        return reviewRepository.save(review);
    }

    private Film createFilm() {
        Film film = new Film();
        film.setTitle("Lola");
        film.setReleaseDate(Instant.parse("2019-12-31T23:20:08Z"));
        film.setStatus(true);
        return filmRepository.save(film);
    }
}
