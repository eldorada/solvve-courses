package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Person;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

public class PersonRepositoryTest extends BaseTest {

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testCreatedAtIsSet() {
        Person person = createPerson();

        Instant createdAtBeforeReload = person.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        person = personRepository.findById(person.getId()).get();

        Instant createdAtAfterReload = person.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Person person = createPerson();
        Instant updatedAtBeforeReload = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        person = personRepository.findById(person.getId()).get();
        person.setBiography("Lola F");
        person = personRepository.save(person);

        Instant updatedAtAfterReload = person.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);
    }
    
}
