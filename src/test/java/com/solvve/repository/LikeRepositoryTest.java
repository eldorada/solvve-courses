package com.solvve.repository;

import com.solvve.base.BaseTest;
import com.solvve.domain.Film;
import com.solvve.domain.Like;
import com.solvve.domain.LikedObjectType;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

public class LikeRepositoryTest extends BaseTest {

    @Autowired
    private LikeRepository likeRepository;

    @Test
    public void testCreatedAtIsSet() {
        Like like = new Like();
        like.setLike(true);
        like.setType(LikedObjectType.FILM);
        like.setLikedObjectId(new Film().getId());
        likeRepository.save(like);

        Instant createdAtBeforeReload = like.getUpdatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        like = likeRepository.findById(like.getId()).get();

        Instant createdAtAfterReload = like.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        Like like = new Like();
        like.setLike(true);
        like.setType(LikedObjectType.FILM);
        like.setLikedObjectId(new Film().getId());
        likeRepository.save(like);

        Instant updatedAtBeforeReload = like.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        like = likeRepository.findById(like.getId()).get();
        like.setLike(false);
        like = likeRepository.save(like);

        Instant updatedAtAfterReload = like.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

}
