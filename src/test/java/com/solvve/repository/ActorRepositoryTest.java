package com.solvve.repository;

import static org.junit.Assert.*;

import com.solvve.base.BaseTest;
import com.solvve.domain.Actor;
import com.solvve.domain.Person;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

public class ActorRepositoryTest extends BaseTest {

    @Autowired
    private ActorRepository actorRepository;

    @Autowired
    private PersonRepository personRepository;

    @Test
    public void testCreatedAtIsSet() {

        Person p = createPerson();
        Actor a = createActor(p);
        a.setAverageMark(3.5);
        a = actorRepository.save(a);

        Instant createdAtBeforeReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        a = actorRepository.findById(a.getId()).get();

        Instant createdAtAfterReload = a.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {

        Person p = createPerson();
        Actor a = createActor(p);
        a.setAverageMark(3.5);
        a = actorRepository.save(a);

        Instant updatedAtBeforeReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        a = actorRepository.findById(a.getId()).get();
        a.setAverageMark(3.6);
        a = actorRepository.save(a);

        Instant updatedAtAfterReload = a.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

    private Person createPerson() {
        Person person = generateFlatEntityWithoutId(Person.class);
        return personRepository.save(person);
    }

    private Actor createActor(Person person) {
        Actor actor = generateFlatEntityWithoutId(Actor.class);
        actor.setPerson(person);
        return actorRepository.save(actor);
    }

}
