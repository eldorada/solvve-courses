package com.solvve.repository;

import static org.junit.Assert.*;

import com.solvve.base.BaseTest;
import com.solvve.domain.ApplicationUser;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;

public class ApplicationUserRepositoryTest extends BaseTest {

    @Autowired
    private ApplicationUserRepository applicationUserRepository;

    @Test
    public void testSave() {

        ApplicationUser u = new ApplicationUser();
        u = applicationUserRepository.save(u);
        assertNotNull(u.getId());
        assertTrue(applicationUserRepository.findById(u.getId()).isPresent());
    }

    @Test
    public void testCreatedAtIsSet() {
        ApplicationUser u = new ApplicationUser();
        u.setName("Vasya");
        u.setLogin("Vasya1934");
        u.setRating(3.5);
        u = applicationUserRepository.save(u);

        Instant createdAtBeforeReload = u.getCreatedAt();
        Assert.assertNotNull(createdAtBeforeReload);
        u = applicationUserRepository.findById(u.getId()).get();

        Instant createdAtAfterReload = u.getCreatedAt();
        Assert.assertNotNull(createdAtAfterReload);
        Assert.assertEquals(createdAtBeforeReload, createdAtAfterReload);
    }

    @Test
    public void testUpdatedAtIsSet() {
        ApplicationUser u = new ApplicationUser();
        u.setName("Vasya");
        u.setLogin("Vasya1934");
        u.setRating(3.5);
        u = applicationUserRepository.save(u);

        Instant updatedAtBeforeReload = u.getUpdatedAt();
        Assert.assertNotNull(updatedAtBeforeReload);
        u = applicationUserRepository.findById(u.getId()).get();
        u.setRating(3.6);
        u = applicationUserRepository.save(u);

        Instant updatedAtAfterReload = u.getUpdatedAt();
        Assert.assertNotNull(updatedAtAfterReload);
        Assert.assertNotEquals(updatedAtBeforeReload, updatedAtAfterReload);
    }

}
